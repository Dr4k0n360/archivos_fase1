#ifndef VALIDACIONESCOMANDOS_H_INCLUDED
#define VALIDACIONESCOMANDOS_H_INCLUDED


#include "ejemplo.h"
#include "discos_particiones.h"
#include "validaciones_comandosBloques.h"
#include "bloques_archivos.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <dirent.h>
/*
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>*/


using namespace std;
//STRUCTS DE LOS COMANDOS
struct cmdEXEC{
    string path;
};

struct cmdMKDISK{
    int _size;
    string _fit;
    string _unit;
    string _path;
};

struct cmdRMDISK{
    string _path;
};

struct cmdFDISK{
    int _size;
    string _unit;
    string _path;
    string _type;
    string _fit;
    string _delete;
    string _name;
    int _add;
};

struct cmdMOUNT{
    string _path;
    string _name;
};

struct cmdUNMOUNT{
    string id;
};

struct cmdREP{
    string _name;
    string _path;
    string _id;
    string _ruta;
};

void parametrosEXEC(vector<string> valoresComandos);
struct cmdEXEC Valor_path(vector<string> comando, struct cmdEXEC cmd);
void COMANDOS_PROGRAMA(vector<string> _Comando);
void EJECUCION_MKDISK(struct cmdMKDISK disk);
struct cmdMKDISK R_CMD_MKDISK(vector<string> comandos,struct cmdMKDISK retornar);
struct cmdRMDISK R_CMD_RMDISK(vector<string> comandos, struct cmdRMDISK retornar);
void EJECUCION_RMDISK(struct cmdRMDISK disk);
struct cmdFDISK R_CMD_FDISK(vector<string> comandos, struct cmdFDISK retornar);
void EJECUCION_FDISK(struct cmdFDISK fdisk);
void EJECUCION_CAT(struct cmdCAT cat);
struct cmdLOSS R_CMD_LOSS(vector<string> comandos,struct cmdLOSS retornar);
void EJECUCION_LOSS(struct cmdLOSS loss);
void EJECUCION_RECOVERY(struct cmdLOSS recovery);
void EJECION_INODE_REP(struct cmdREP rep);
void EJECUCION_BLOCK_REP(struct cmdREP rep);
void EJECUCION_BM_INODE_REP(struct cmdREP rep);
void EJECUCION_BM_BLOCK_REP(struct cmdREP rep);
void EJECUCION_SB_REP(struct cmdREP rep);
void EJECUCION_FILE_REP(struct cmdREP rep);
void EJECUCION_JOURNALING_REP(struct cmdREP rep);
void EJECUCION_TREE_REP(struct cmdREP rep);
void EJECUCION_LS_REP(struct cmdREP rep);
void EJECUCION_CHGRP(struct cmdCHGRP chgrp);


////////
void APLICAR_AJUSTES_PARTITION_PYE(struct MBRD_DISK disco, struct cmdFDISK fdisk);
void APLICAR_AJUSTES_PARTITION_L(struct MBRD_DISK disco, struct cmdFDISK fdisk);
void EJECUCION_FDISK_DELETE(struct cmdFDISK disk);
struct cmdMOUNT R_CMD_MOUNT(vector <string> comandos,struct cmdMOUNT retornar);
void EJECUCION_MOUNT(struct cmdMOUNT mount);
struct cmdUNMOUNT R_CMD_UNMOUNT(vector <string> comandos, struct cmdUNMOUNT retornar);
struct cmdREP R_CMD_REP(vector <string> comandos, struct cmdREP retornar);
void EJECUCION_MBR_REP(struct cmdREP rep);
void EJECUCION_DISK_REP(struct cmdREP rep);
struct cmdMKFS R_CMD_MKFS(vector<string> comando, struct cmdMKFS retornar);
void EJECUCION_MKFS(struct cmdMKFS mkfs);
struct cmdLOGIN R_CMD_LOGIN(vector<string> comandos,struct cmdLOGIN retornar);
void EJECUCION_LOGIN(struct cmdLOGIN login);
struct cmdMKGRP R_CMD_MKGRP(vector<string> comandos, struct cmdMKGRP retornar);
void EJECUCION_MKGRP(struct cmdMKGRP mkgrp);
struct cmdMKUSR R_CMD_MKUSR(vector<string> comandos, struct cmdMKUSR retornar);
void EJECUCION_MKUSR(struct cmdMKUSR mkusr);
struct cmdMKFILE R_CMD_MKFILE(vector<string> comandos,struct cmdMKFILE retornar);
void EJECUCION_MKFILE(struct cmdMKFILE mkfile);
struct cmdMKDIR R_CMD_MKDIR(vector<string> comandos,struct cmdMKDIR retornar);
void EJECUCION_MKDIR(struct cmdMKDIR _mkdir);
struct cmdCAT R_CMD_CAT(vector<string> comandos,struct cmdCAT retornar);
struct cmdEDIT R_CMD_EDIT(vector<string> comandos,struct cmdEDIT retornar);
void EJECUCION_EDIT(struct cmdEDIT edit);
struct cmdRMGRP R_CMD_RMGRP(vector<string> comandos, struct cmdRMGRP retornar);
void EJECUCION_RMGRP(struct cmdRMGRP rmgrp);
struct cmdRMUSR R_CMD_USR(vector<string> comandos,struct cmdRMUSR retornar);
void EJECUCION_RMUSR(struct cmdRMUSR usr);
struct cmdCHGRP R_CMD_CHGRP(vector<string> comandos,struct cmdCHGRP retornar);
struct cmdSYNCRONICE R_CMD_SYNCRONICE(vector<string> comandos,struct cmdSYNCRONICE retornar);
void EJECUCION_SYNCRONICE(struct cmdSYNCRONICE syncronice);

//metodo que nos devuelve un struct con los parametros del cmoando exec
void parametrosEXEC(vector<string> valoresComandos){

   struct cmdEXEC datoscomando;

   for(int i=1; i<valoresComandos.size(); i++){//con este for ejecutamos los comandos de exec
       char valor[valoresComandos[i].size()+1];
       strcpy(valor,valoresComandos[i].c_str());//se convierte todo el comando en char "-path=texto"
       vector<string> comando_individual=split(valor,"=");
       datoscomando=Valor_path(comando_individual, datoscomando);
    }

    ifstream archivoEntrada;//objeto del archivo
    string datosArchivo;//string para leer el archivo
    string datosConcatenados;
    string datosConcatenados2;
    if(datoscomando.path!=""){
        //aqui va el codigo para cada linea de comando que valla ejecutando

        archivoEntrada.open(datoscomando.path,ios::in);

        if(archivoEntrada.fail()){
            //imprimirln(datoscomando.path);
            imprimirln("===ERROR: EL ARCHIVO NO EXISTE EN LA RUTA ESPECIFICADA===");
        }else{

           imprimirln("+++ LEYENDO ARCHIVO... +++");
            while(!archivoEntrada.eof()){
                getline(archivoEntrada,datosArchivo);
                datosConcatenados+=datosArchivo;
            }

            //datosConcatenados2=DATOSARCHIVO(datosConcatenados);
            char datosArchivos2[datosConcatenados.size()+1];
            strcpy(datosArchivos2,datosConcatenados.c_str());
            vector<string> comandosArchivos=split(datosArchivos2,"\r\n");

            for(int i=0; i<comandosArchivos.size(); i++){//con este for vamos reccoriendo cada linea de comando del archivo e imprmiendo
                imprimirln(comandosArchivos[i]);//IMPRIME LA LINEA DEL COMANDO QUE SE ESTA EJECUTANDO

                //convirtiendo a vector
                char comandoIndividual[comandosArchivos[i].size()+1];
                strcpy(comandoIndividual,comandosArchivos[i].c_str());
                vector<string> _Comando=split(comandoIndividual,"-");

                //MANDANDO LA LINEA DEL COMANDO A EJECUTARSE POR EL PROGRAMA
                COMANDOS_PROGRAMA(_Comando);

            }



        /*    //////ESTO LO AGREGAMOS---SE ELIMINA
            string linea;
            while(getline(archivoEntrada,linea)){

                 char comandoIndividual[linea.size()+1];
                strcpy(comandoIndividual,linea.c_str());
                vector<string> _Comando=split(comandoIndividual,"-");

                //MANDANDO LA LINEA DEL COMANDO A EJECUTARSE POR EL PROGRAMA
                COMANDOS_PROGRAMA(_Comando);

            }


            /////////*/


        }

        archivoEntrada.close();
    }else{
        imprimirln("===ERROR: NO ESTA USANDO EL COMANDO -PATH Y/O NO ESTA INDICANDO UNA RUTA===");
    }
}


void COMANDOS_PROGRAMA(vector<string> _Comando){//los valores que tiene _Comando siempre son los separados por "-"

/*    for(int i=0; i<_Comando.size(); i++){
        imprimirln(_Comando[i]);
    }*/


    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="mkdisk"){//aqui adentro se va a ejecutar todos los comandos de MKDISK
        struct cmdMKDISK MKDISK;
        MKDISK._size=0; //inicializa la variable SIZE DE MKDISK EN 0
        for(int i=1; i<_Comando.size(); i++){//CON ESTE FOR OBTENEMOS TODOS LOS DATOS DE LOS COMANDOS MKDISK
            char temporal[_Comando[i].size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");//este vector obtiene los que estan separados por "=" siempre son 2
            MKDISK=R_CMD_MKDISK(valores,MKDISK);
        }


        if((MKDISK._size!=NULL) && (MKDISK._path!="")){//ESTE IF SIRVE PARA VALIDAR LOS PARAMETROS QUE SON OBLIGATORIOS
            //ESTOS IF ES PARA DARLE VALOR A LOS PARAMETROS OPCIONALES DE MKDISK
            if(MKDISK._fit==""){
                MKDISK._fit="ff";
            }
            if(MKDISK._unit==""){
                MKDISK._unit="m";
            }
            //ESTO VA A EJECUTAR TODA LA LINEA DE LOS COMANDOS MKDISK
            EJECUCION_MKDISK(MKDISK);
            //imprimirln("entro a mkdisk");
        }else{
            imprimirln("===ERROR: LOS COMANDOS PATH Y SIZE SON OBLIGATORIOS===");
        }
        //FINDE LA EJECUCION DE LOS COMANDOS DE MKDISK
    }else

    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="rmdisk"){
        struct cmdRMDISK RMDISK;
        for(int i=1; i<_Comando.size(); i++){
            char temporal[_Comando[i].size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");
            RMDISK=R_CMD_RMDISK(valores,RMDISK);
        }

        if(RMDISK._path!=""){

            if(VALIDAR_EXTENSION_PATH(RMDISK._path)){
                //imprimirln("entro a rmdisk");
                EJECUCION_RMDISK(RMDISK);//     verificar el comando mdisk para salir
            }

        }else{
            imprimirln("===ERROR: NO ESTA USANDO EL COMANDO PATH OBLIGATORIO PARA RMDISK===");
        }
        //FIN DEL COMANDO RMDISK
    }else
    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="fdisk"){
        struct cmdFDISK FDISK;
        FDISK._size=0;
        FDISK._add=0;
        for(int i=1; i<_Comando.size(); i++){
            char temporal[_Comando[i].size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");

            if((convertirMinusculla(obtener_texto_sin_espacios(valores[0]))=="add")){//ESTE IF SIRVE PARA VALIDAR NUMEROS NEGATIVOS PARA ADD
                //imprimirln("validando 11");
                if(i!=_Comando.size()-1){
                   // imprimirln("validando12");
                   // imprimirNum(valores.size());
                    if(valores.size()<2){//ESTO ES PARA VALIDAR QUE VALORES NO TENGA ESPACIO NULO
                   //     imprimirln("entro en uno");
                        if(validarNumero(obtener_texto_sin_espacios(_Comando[i+1]))==true){//VERIFICA SI LO QUE LE SIGUE ES UN NUMERO
                          //  imprimirln("entro en dos");
                            string valores2="-"+_Comando[i+1];
                            valores.push_back(valores2);
                            i++;
                        }else{
                            imprimirln("===ERROR: NO AGREGO UN VALOR AL COMANDO ADD===");
                        }

                    }else{//ENTRA AQUI SI TIENE ESPACIOS NO NULOS

                        if(validarNumero(obtener_texto_sin_espacios(_Comando[i+1]))==true){//VERIFICA SI LO SIGUIENTE QUE VIENE ES NUMERO
                            string valores2="-"+_Comando[i+1];
                            valores.pop_back();         //ELIMINA EL ULTIMO ELEMENTO DEL VECTOR PARA AGREGAR EL NUMERO
                            valores.push_back(valores2);
                            i++;
                        }else

                        if(obtener_texto_sin_espacios(valores[1])==""){//VERIFICA QUE LO QUE VIENE SEA NULO PARA IMPRIMIR EL MENSAJE ERROR
                            imprimirln("===ERROR: NO AGREGO UN VALOR AL COMANDO ADD===");
                        }

                    }

                }
            }

            FDISK=R_CMD_FDISK(valores,FDISK);
        }




        if( (FDISK._size!=NULL) && (FDISK._path!="")  && (FDISK._name!="")){

            if(FDISK._unit==""){
                FDISK._unit="k";
            }

            if(FDISK._type==""){
                FDISK._type="p";
            }

            if(FDISK._fit==""){
                FDISK._fit="wf";
            }


            if(FDISK._delete!=""){
                EJECUCION_FDISK_DELETE(FDISK);
            }else

            if(FDISK._add!=NULL){
                imprimirln("entro a delete");
            }else{
                EJECUCION_FDISK(FDISK);
            }


        }else{
            imprimirln("===ERRRO: LOS COMANDOS SIZE, PATH Y NAME SON OBLIGATORIOS PARA FDISK===");
        }

/*        imprimirln(FDISK._delete);
        imprimirNumln(FDISK._add);
        imprimirln(FDISK._fit);
        imprimirln(FDISK._name);
        imprimirln(FDISK._path);
        imprimirNumln(FDISK._size);
        imprimirln(FDISK._type);
        imprimirln(FDISK._unit);       */ //imprimirln("entro a fdisk");
        //FIN DEL COMANDO FDISK
    }else
    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="mount"){
        struct cmdMOUNT MOUNT;
        for(int i=1; i<_Comando.size(); i++){
            char temporal[_Comando[i].size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");
            MOUNT=R_CMD_MOUNT(valores,MOUNT);
        }

        if((MOUNT._name!="") && (MOUNT._path!="")){
            EJECUCION_MOUNT(MOUNT);
        }else{
            imprimirln("=== ERROR: LOS COMANDOS PATH Y NAME SON OBLIGATORIOS PARA MOUNT");
        }

    }else
    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="unmount"){
        struct cmdUNMOUNT UNMOUNT;
        for(int i=1; i<_Comando.size(); i++){
            char temporal[_Comando.size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");
            UNMOUNT=R_CMD_UNMOUNT(valores,UNMOUNT);
        }

        if(UNMOUNT.id!=""){

            if(EXISTE_LLAVE_REGISTRO(UNMOUNT.id)){//            SE COLOCO AQUI LA EJECUCION_UNMOUNT
                MONTAR_PARTICION=ELIMINAR_MONTAJE(UNMOUNT.id);
                imprimirln("++++ MONTAJE ELIMINADO EXITOSAMENTE ++++");
                MOSTRAR_INF_MONTAJES();
            }else{
                imprimirln("==== ERROR: NO EXISTE PARTICION CON ESE ID =====");
            }

        }else{
            imprimirln("==== ERROR: EL COMANDO ID ES OBLIGATORIO PARA UNMOUNT ====");
        }


    }else
    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="rep"){
        struct cmdREP REP;
        for(int i=1; i< _Comando.size(); i++){
            char temporal[_Comando[i].size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");
            REP=R_CMD_REP(valores,REP);
        }

        if((REP._id!="") && (REP._name!="") &&(REP._path!="")){
                            //PARA PRUEBAS SE COMENTA
               /* if(REP._name=="mbr"){
                //imprimirln("entra a mbr");
                    EJECUCION_MBR_REP(REP);
                }else
                if(REP._name=="disk"){
                    //imprimirln("entra a disk");
                    EJECUCION_DISK_REP(REP);
                }*/
                if(REP._name=="inode"){
                    EJECION_INODE_REP(REP);
                }else if(REP._name=="journaling"){
                    EJECUCION_JOURNALING_REP(REP);
                }else if(REP._name=="block"){
                    EJECUCION_BLOCK_REP(REP);
                }else if(REP._name=="bm_inode"){
                    EJECUCION_BM_INODE_REP(REP);
                }else if(REP._name=="bm_block"){
                    EJECUCION_BM_BLOCK_REP(REP);
                }else if(REP._name=="tree"){
                    EJECUCION_TREE_REP(REP);
                }else if(REP._name=="sb"){
                    EJECUCION_SB_REP(REP);
                }else if(REP._name=="file"){
                    EJECUCION_FILE_REP(REP);
                }else if(REP._name=="ls"){
                    EJECUCION_LS_REP(REP);
                }
            //imprimirln("VERIFICADO REP");
        }else{
            imprimirln("=== ERROR: LOS COMANDOS NAME, PATH, Y ID, SON OBLIGATORIOS PARA REP===");
        }

    }else
    if(convertirMinusculla(obtener_texto_sin_espacios(_Comando[0]))=="mkfs"){
        cmdMKFS MKFS;
        for(int i=1; i<_Comando.size(); i++){
        char temporal[_Comando[i].size()+1];
        strcpy(temporal,_Comando[i].c_str());
        vector<string> valores=split(temporal,"=");
        MKFS=R_CMD_MKFS(valores,MKFS);
        }

        if(MKFS._id!=""){
            if(MKFS._fs==""){
                MKFS._fs="3fs";
            }

            if(MKFS._type==""){
                MKFS._type="full";
            }
            EJECUCION_MKFS(MKFS);
        }else{
            imprimirln("=== ERROR: EL ID DEBE TENER UN VALOR OBLIGATORIO ===");
        }


    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="login"){
        cmdLOGIN LOGIN;
        for(int i=1; i<_Comando.size(); i++){
            char temporal[_Comando[i].size()+1];
            strcpy(temporal,_Comando[i].c_str());
            vector<string> valores=split(temporal,"=");
            LOGIN=R_CMD_LOGIN(valores,LOGIN);
        }

        if((LOGIN._id!="") && (LOGIN._pwd!="") && (LOGIN._usr!="")){
            EJECUCION_LOGIN(LOGIN);
        }else{
            imprimirln("==== ERROR: LOS PARAMETROS ID, PWD Y USR SON OBLIGATORIOS PARA LOGIN ====");
        }

    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="logout"){
        if(_Comando.size()==1){
            //VALIDO QUE ALLAN UN DATO EN DATOSPIVOTE PARA PODER CERRAR SESION
            if(DATOS_PIVOTE.size()==1){
                DATOS_PIVOTE.clear();

                if(DATOS_PIVOTE.size()!=0){//PARA VERIFICAR QUE SE ALLA ELIMINADO LOS DATOS DE LA SESION ACTIVA
                    DATOS_PIVOTE.clear();
                    imprimirln("+++++ SESION CERRADA DE FORMA EXISTOSA (2) ++++");
                }else{
                    imprimirln("+++++ SESION CERRADA DE FORMA EXISTOSA ++++");
                }

            }else{
                //COMO PUEDE ENTRAR AQUI SI EXISTE MAS DE UN DATO ENTONCES BORRAMOS DATOS PIVOTE Y MANDAMOS QUE NO TIENE SESION ACTIVA
                DATOS_PIVOTE.clear();
                imprimirln("==== ERROR: USTED NO TIENE UNA SESION ACTIVA ====");
            }

        }else{
            imprimirln("=== ERROR: EL COMANDO LOGOUT NO RECIBE PARAMETROS ====");
        }

    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="mkgrp"){
        if(DATOS_PIVOTE.size()==1){
            struct cmdMKGRP MKGRP;
            for(int i=1; i<_Comando.size(); i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                MKGRP=R_CMD_MKGRP(valores,MKGRP);
            }

            if(MKGRP._name!=""){
                EJECUCION_MKGRP(MKGRP);
            }else{
                imprimirln(" ==== ERROR: EL COMANDO NAME ES UN PARAMETRO OBLIGATORIO PARA MKGRP ====");
            }

        }else{
            DATOS_PIVOTE.clear();//SI NO TIENE UNA SESION ACTIVA DATOS_PIVOTE DEBE SER CERO
            imprimirln(" ==== ERROR: USTED NO TIENE UNA SESION INICIADA ====");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="mkusr"){
        if(DATOS_PIVOTE.size()==1){//VALIDA QUE SE HALLA INICIADO SESION PRIMERO PARA RELIZAR ESTE COMANDO
            struct cmdMKUSR MKUSR;
            for(int i=1; i<_Comando.size(); i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                MKUSR=R_CMD_MKUSR(valores,MKUSR);

            }


            if((MKUSR._grp!="") && (MKUSR._usr!="") && (MKUSR._pwd!="")){
                EJECUCION_MKUSR(MKUSR);
            }else{
                imprimirln("===ERROR: LOS COMANDOS GRP, USR, PWD SON OBLIGATORIO PARA MKUSR===");
            }

        }else{
            DATOS_PIVOTE.clear();
            imprimirln("==== ERROR: USTED NO TIENE UNA SESION INICIADA ====");
        }

    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="mkfile"){
        if(DATOS_PIVOTE.size()==1){
            struct cmdMKFILE MKFILE;
            for(int i=1; i<_Comando.size();i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                if(obtener_texto_sin_espacios(convertirMinusculla(valores[0]))=="size"){
                    if(i!=(_Comando.size()-1)){
                       if(validarNumero(obtener_texto_sin_espacios(_Comando[i+1]))){
                            imprimirln("===ERROR: NO SE PERMITEN NUMEROS NEGATIVOS CON SIZE==");
                            i++;
                       }
                    }
                }

                MKFILE=R_CMD_MKFILE(valores,MKFILE);
            }

            if(MKFILE._path!=""){
                EJECUCION_MKFILE(MKFILE);
            }else{
                imprimirln("===ERROR: EL PARAMETRO PATH ES OBLIGATORIO PARA MKFILE===");
            }

        }else{
            DATOS_PIVOTE.clear();
            imprimirln("==== ERROR: USTED NO TIENE UNA SESION INICIADA ====");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="mkdir"){
        if(DATOS_PIVOTE.size()==1){
            struct cmdMKDIR MKDIR;
            for(int i=1; i<_Comando.size(); i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                MKDIR=R_CMD_MKDIR(valores,MKDIR);
            }

           if(MKDIR._path!=""){
            EJECUCION_MKDIR(MKDIR);
           }else{
            imprimirln("=== ERROR: EL PARAMETRO PATH ES OBLIGATORIO PARA MKDIR ===");
           }
        }else{
            DATOS_PIVOTE.clear();
            imprimirln("==== ERROR: USTED NO TIENE UNA SESION INICIADA ====");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="cat"){
        if(DATOS_PIVOTE.size()==1){
            struct cmdCAT CAT;
            for(int i=1; i<_Comando.size(); i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                CAT=R_CMD_CAT(valores,CAT);
            }

            if(CAT._file!=""){
                EJECUCION_CAT(CAT);
            }else{
                imprimirln("=== ERROR: EL PARAMETRO FILE ES OBLIGATORIO PARA CAT ===");
            }


        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: USTED NO TIENE UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="pause"){
        if(DATOS_PIVOTE.size()==1){
            if(_Comando.size()==1){
                imprimirln("++++ PRESIOINE ENTER PARA CONTINUAR ++++");
                char* pausa=static_cast<char *>(malloc(3));

                cin.getline(pausa,3);
                free(pausa);
            }else{
                imprimirln("=== ERROR: EL COMANDO PAUSE NO ACEPTA PARAMETROS ===");
            }
        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: USTED NO TIENE UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="loss"){
        if(DATOS_PIVOTE.size()==1){
            struct cmdLOSS LOSS;
            for(int i=1; i<_Comando.size();i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                LOSS=R_CMD_LOSS(valores,LOSS);
            }

            if(LOSS._id!=""){
                EJECUCION_LOSS(LOSS);
            }else{
                imprimirln("=== ERROR: EL PARAMETRO ID ES OBLIGATORIO PARA LOSS ===");
            }

        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: USTED NO TIENE UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="recovery"){
        if(DATOS_PIVOTE.size()==1){
            //SE UTILIZARA LA MISMA ESTRUCTURA QUE LOSS YA QUE AMBOS SOLO PIDEN EL ID
            struct cmdLOSS RECOVERY;
            for(int i=1; i<_Comando.size();i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                RECOVERY=R_CMD_LOSS(valores,RECOVERY);
            }

            if(RECOVERY._id!=""){
                EJECUCION_RECOVERY(RECOVERY);
            }else{
                imprimirln("=== ERROR: EL PARAMETRO ID ES OBLIGATORIO PARA RECOVERY ===");
            }

        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: USTED NO TIENE UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="edit"){
        if(DATOS_PIVOTE.size()==1){
            struct cmdEDIT EDIT;
            for(int i=1; i<_Comando.size(); i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                EDIT=R_CMD_EDIT(valores,EDIT);
            }

            if((EDIT._cont!="") && (EDIT._path!="")){
                EJECUCION_EDIT(EDIT);
            }else{
                imprimirln("=== ERROR: LOS PARAMETROS PATH Y CONT SON OBLIGATORIO PARA EDIT ===");
            }

        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: NO TIENE UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="rmgrp"){
        if(DATOS_PIVOTE.size()==1){
            if((DATOS_PIVOTE[0].u_id==1) && (DATOS_PIVOTE[0].g_id==1)){
                struct cmdRMGRP RMGRP;
                for(int i=1; i<_Comando.size(); i++){
                    char temporal[_Comando[i].size()+1];
                    strcpy(temporal,_Comando[i].c_str());
                    vector<string> valores=split(temporal,"=");
                    RMGRP=R_CMD_RMGRP(valores,RMGRP);
                }

                if(RMGRP._name!=""){
                    EJECUCION_RMGRP(RMGRP);
                }else{
                    imprimirln("=== ERROR: EL PARAMETRO NAME ES OBLIGATORIO PARA MRGRP ===");
                }

            }else{
                imprimirln("=== ERROR: USTED NO ES EL USUARIO ROOT ===");
            }
        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: NO CUENTA CON UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="rmusr"){
        if(DATOS_PIVOTE.size()==1){
            if((DATOS_PIVOTE[0].u_id==1) && (DATOS_PIVOTE[0].g_id==1)){
                struct cmdRMUSR USR;
                for(int i=1; i<_Comando.size(); i++){
                    char temporal[_Comando[i].size()+1];
                    strcpy(temporal,_Comando[i].c_str());
                    vector<string> valores=split(temporal,"=");
                    USR=R_CMD_USR(valores,USR);
                }

                if(USR._usr!=""){
                    EJECUCION_RMUSR(USR);
                }else{
                    imprimirln("=== ERROR: EL PARAMETRO USR ES OBLIGATORIO PARA RMUSR ===");
                }

            }else{
                imprimirln("=== ERROR: USTED NO ES EL USUARIO ROOT ===");
            }
        }else{
            DATOS_PIVOTE.clear();
            imprimirln("=== ERROR: NO TIENE UNA SESION INICIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="chgrp"){
        if(DATOS_PIVOTE.size()==1){
            if((DATOS_PIVOTE[0].u_id==1) && (DATOS_PIVOTE[0].g_id==1)){
                struct cmdCHGRP CHGRP;
                for(int i=1; i<_Comando.size(); i++){
                    char temporal[_Comando[i].size()+1];
                    strcpy(temporal,_Comando[i].c_str());
                    vector<string> valores=split(temporal,"=");
                    CHGRP=R_CMD_CHGRP(valores,CHGRP);
                }

                if((CHGRP._grp!="") && (CHGRP._usr!="")){
                    EJECUCION_CHGRP(CHGRP);
                }else{
                    imprimirln("=== ERROR: LOS PARAMETROS USR Y GRP SON OBLIGATORIOS PARA CHGRP ===");
                }

            }else{
                imprimirln("=== ERROR: USTED NO ES EL USUARIO ROOT ===");
            }
        }else{
            imprimirln("=== ERROR: NO TIENE UNA SESION INCIADA ===");
        }
    }else
    if(obtener_texto_sin_espacios(convertirMinusculla(_Comando[0]))=="syncronice"){
            struct cmdSYNCRONICE SYNCRONICE;
            for(int i=1; i<_Comando.size(); i++){
                char temporal[_Comando[i].size()+1];
                strcpy(temporal,_Comando[i].c_str());
                vector<string> valores=split(temporal,"=");
                SYNCRONICE=R_CMD_SYNCRONICE(valores,SYNCRONICE);
            }

            if(SYNCRONICE._id!=""){
                EJECUCION_SYNCRONICE(SYNCRONICE);
            }else{
                imprimirln("=== ERROR: EL PARAMETRO ID ES OBLIGATORIO PARA SYNCRONICE ===");
            }
    }
}

//METODO PARA EL COMANDO MKDISK
void EJECUCION_MKDISK(struct cmdMKDISK disk){

  /*  imprimirln("entro al mejtodo ejcutar");
    imprimirln(disk._fit);
    imprimirln(disk._path);
    imprimirNumln(disk._size);
    imprimirln(disk._unit);*/
    if(VALIDAR_EXTENSION_PATH(disk._path)==true){

        int tamano=0;           //NOS VA A SERVIR PARA INDICAR LA CANTIDAD DEL DISCO EN BYTES
        //INSTRUCCIONES PARA CREAR EL DIRECTORIO O CARPETAS
        char valoresDirectorio[disk._path.size()+1];
        strcpy(valoresDirectorio,disk._path.c_str());
        vector<string> directorios=split(valoresDirectorio,"/");
        imprimirln("+++CREAND EL ARCHIVO ESPECIFICADO++++");
        crearCarpetas(directorios);// ESTE METODO CREA LAS CARPETAS SE COMENTA SOLO PARA PRUEBAS

        if(disk._unit=="k"){
            tamano=disk._size*1024;
           CREAR_ARCHIVO_DISCO(disk._path,disk._size);     //CREA EL ARCHIVOS QUE VA A SIMULAR SER DISCO DURO
           CREAR_ARCHIVO_DISCO(OBTENER_PATH_ARCHIVORAID(disk._path),disk._size);//CREAR EL RAID DEL DISCO
        }else{
            tamano=disk._size*1024*1024;
           CREAR_ARCHIVO_DISCO(disk._path,(disk._size*1024));     //CREA EL ARCHIVOS QUE VA A SIMULAR SER DISCO DURO
            CREAR_ARCHIVO_DISCO(OBTENER_PATH_ARCHIVORAID(disk._path),(disk._size*1024));
        }
        //mprimirln(disk._path);
       srand(time(NULL));
       time_t tmFechaHora=time(NULL);
       struct MBRD_DISK disco;     //LA ESTRUCTURA QUE SE GUARDARA EN EL ARCHIVO_DISCO
       //char hola=disk._fit[0];
       //cout<<hola<<endl;
       //DANDOLE VALORES A LA ESTRUCTURA DISCO
       disco.mbr_fecha_creacion=tmFechaHora;
       disco.mbr_fit=disk._fit[0];
       disco.mbr_tamano=tamano;
       disco.mbr_disk_signature=(int) rand();
       // imprimirln(ctime(&tmFechaHora));
 /*     imprimirln(ctime(&disco.mbr_fecha_creacion));
       cout<<disco.mbr_fit<<endl;
       imprimirNumln(disco.mbr_tamano);
       imprimirNumln(disco.mbr_disk_signature);     */

       GUARDAR_MBR_EN_DISCO(disk._path,disco);        //METODOS QUE GUARDAN EL MBR EN EL DISCO Y EN EL RAID
       GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(disk._path),disco);

  /*     struct MBRD_DISK nuevoEjemplo;
       nuevoEjemplo=OBTENER_MBR_DEL_DISCO(disk._path);
       imprimirln(ctime(&nuevoEjemplo.mbr_fecha_creacion));
       cout<<nuevoEjemplo.mbr_fit<<endl;
       imprimirNumln(nuevoEjemplo.mbr_tamano);
       imprimirNumln(nuevoEjemplo.mbr_disk_signature);*/

    }
}
//METODO PARA EL COMANDO RMDISK
void EJECUCION_RMDISK(struct cmdRMDISK disk){
    //imprimirln(disk._path);
    //imprimirNumln(remove("/home/dr4k0n/Escritorio/ejemplo.txt"));
    //imprimirln("entro------");
    char* palabra=static_cast<char *>(malloc(50));//CREAMOS UN CHAR Y LE ASIGNAMOS MEMORIA CON MALLOC
    imprimirln("******    ESTA SEGURO QUE DESEA ELIMINAR EL DISCO (s/n)     ******");

    do{
        cin.getline(palabra,50);
       if(strcmp(palabra,"s")==0){
           //imprimirln("entro a si");
            if(remove(disk._path.c_str())==0){
                imprimirln("++++ARCHIVO ELIMINADO EXITOSAMENTE++++");
            }else{
                imprimirln("====ERROR: EL ARCHIVOS NO FUE ENCONTRADO PARA ELIMINARSE====");
            }

       }else if(strcmp(palabra,"n")==0){
           imprimirln("+++++ ELIMINACION CANCELADA+++++");
       }else{
           imprimirln("=====ERROR: NO SE RECONOCE EL VALOR INTRODUCIDO, INTENTELO DE NUEVO====");
       }

    }while((strcmp(palabra,"s")!=0) && (strcmp(palabra,"n")!=0));

    free(palabra);//LIMPIAMOS LA MEMORIA RESERVADA POR MALLOC

}

void EJECUCION_FDISK(struct cmdFDISK fdisk){

    if(EXISTE_DISCO_O_ARCHIVO(fdisk._path)){//PRIMERO VALIDA QUE EXISTA EL ARCHIVO
         struct MBRD_DISK DISCO;
         if(fdisk._delete==""){//SI NO SE UTILIZO EL COMANDO DELETE SE REALIZAR ESTE ACCION
           DISCO=OBTENER_MBR_DEL_DISCO(fdisk._path);//OBTENEMOS EL MBR DEL DISCO
            //cout<<DISCO.mbr_partition[0].part_status<<endl;
            if(DISCO_EN_BLANCO_PARTICIONES_PYE(DISCO)==true){//SI ESTO ES TRUE EL DISCO ESTA EN BLANCO NINGUNA PARTICION A SIDO USADA
                //imprimirln("debe entrar aca...");
                /*imprimirNumln(sizeof(MBRD_DISK));
                imprimirNumln(sizeof(MBRD_DISK)+OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit));
                imprimirNumln(sizeof(MBRD_DISK)+1);*/

                //DISCO.mbr_partition[0].part_status=(char)"1";
                if(fdisk._type!="l"){//SI LA PARTICION A CREAR NO ES LOGICA REALIZA LA SIGUIENE ACCION

                    if(OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)<DISCO.mbr_tamano){

                            string status="1";
                           // strcpy((char *)DISCO.mbr_partition[0].part_status,"1");

                            DISCO.mbr_partition[0].part_status=status[0];           //ESCRIBIENDO LOS DATOS DE LA PRIMERA PARTICION
                            DISCO.mbr_partition[0].part_type=fdisk._type[0];
                            DISCO.mbr_partition[0].part_fit=fdisk._fit[0];
                            DISCO.mbr_partition[0].part_start=sizeof(MBRD_DISK)+1;
                            DISCO.mbr_partition[0].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                           /* for(int i=0; (i<16) && (i<fdisk._name.size()); i++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                DISCO.mbr_partition[0].part_name[i]=fdisk._name[i];
                            }*/
                            /////
                            for(int p=0; p<16; p++){

                                 if(p<fdisk._name.size()){
                                          DISCO.mbr_partition[0].part_name[p]=fdisk._name[p];
                                  }else{
                                         DISCO.mbr_partition[0].part_name[p]=00;
                                }

                             }

                            ////
                            //imprimirln("permitido el acceso");
                          GUARDAR_MBR_EN_DISCO(fdisk._path,DISCO);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                          GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),DISCO);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                          imprimirln("+++++ LA PRIMERA PARTICION A SIDO CREADA EXITOSAMENTE ++++");

                    }else{
                        imprimirln("=== ERROR: EL TAMANIO ASIGNADO DE LA PARTICION A CREAR ES MAYOR AL DEL DISCO===");
                    }



                }else{//SI ES LOGICA SE PROHIBE YA QUE NO HAY PARTICIONES EXTENDIDAS
                    imprimirln("=== ERROR: NO EXISTEN PARTICIONES EXTENDIDAS PARA CREAR PARTICIONES LOGICAS===");
                }


 /*               struct MBRD_DISK DISCO2;
                OBTENER_MBR_DEL_DISCO(fdisk._path);

               for(int j=0; j<4; j++){

                    if(DISCO2.mbr_partition[j].part_status!=0){

                        cout<<DISCO2.mbr_partition[j].part_status<<endl;
                        cout<<DISCO2.mbr_partition[j].part_type<<endl;
                        cout<<DISCO2.mbr_partition[j].part_fit<<endl;
                        imprimirNumln(DISCO2.mbr_partition[j].part_start);
                        imprimirNumln(DISCO2.mbr_partition[j].part_size);
                        cout<<DISCO2.mbr_partition[j].part_name<<endl;

                    }

                }                       */


            //imprimirln("entra a disco en blanco");

            }else{//SI YA EXISTE MAS DE ALGUNA PARTICION Y HAY QUE APLICAR LOS AJUSTES
                //imprimirln("se van a crear otras particiones");

                if(fdisk._type=="e"){//SE REALIZARA ESTO CUANDO SE DESEE HACER PARTICIONES EXTENDIDAS
                    //imprimirln("entro a e");
                    if(EXISTEN_PARTCIONES_EXTENDIDAS(DISCO)==false){
                        //imprimirln("se puede crear particion extendida");
                        if(EXISTE_PARTICION_EN_EXTENDIDA(DISCO,fdisk._name)==true){
                            APLICAR_AJUSTES_PARTITION_PYE(DISCO,fdisk);
                        }else{
                            imprimirln("=== ERROR: YA EXISTE UNA PARTICION CON ESTE NOMBRE EN PARTICION EXTENDIDA ===");
                        }


                    }else{
                        imprimirln("=== ERROR: SOLO SE PERMITE UNA PARTICION EXTENDIDA ===");
                    }
                }
                else if(fdisk._type=="l"){//CUANDO SE DESEEN HACER PARTICIONES LOGICAS

                    if(EXISTEN_PARTCIONES_EXTENDIDAS(DISCO)){
                        //imprimirln("se puede crear particion logica");

                        //imprimirNumln(CANTIDAD_PARTICIONES_LOGICAS(DISCO));
                        if(CANTIDAD_PARTICIONES_LOGICAS(DISCO)!=15){

                            if(EXISTE_PARTICION_EN_EXTENDIDA(DISCO,fdisk._name)==true){
                                //imprimirln("no existe particion logica con ese nombre");

                                if(EXISTE_PARTICION_EN_DISCO(DISCO,fdisk._name)==true){

                                    APLICAR_AJUSTES_PARTITION_L(DISCO,fdisk);

                                }else{
                                    imprimirln("===ERROR: YA EXISTE UNA PARTICION CON ESE NOMBRE EN PRIMARIA O EXTENDIDA===");
                                }
                            }else{
                                imprimirln("=== ERROR: YA EXISTE UNA PARTICION LOGICA CON ESE NOMBRE===");
                            }


                        }else{
                            imprimirln("=== ERROR: SE ALCANZO EL LIMITE DE PARTICIONES LOGICAS ===");
                        }


                    }else{
                        imprimirln("=== ERROR: NO EXISTE NINGUNA PARTICION EXTENDIDA PARA PODER CREAR PARTICION LOGICA ===");
                    }


                }else{//SE REALIZARAN PARTICIONES PRIMARIAS
                  //  imprimirln("se va a crear particion primaria");
                   // imprimirNumln(CANTIDAD_PARTICIONES_PRIMARIAS(DISCO));
                    if(CANTIDAD_PARTICIONES_PRIMARIAS(DISCO)!=3){

                      /*  if(DISCO.mbr_fit==98){//EL DISCO ES QUIEN ESPECIFICA CON QUE AJUSTE SE UTILIZARAN LAS PARTICIONES
                            imprimirln("mejor ajuste");
                        }else
                        if(DISCO.mbr_fit==102){
                            imprimirln("primero ajuste");
                            //PRIMER_AJUSTE_D_P(DISCO,fdisk);
                        }else
                        if(DISCO.mbr_fit==119){
                            imprimirln("peor ajuste");
                        }*/

                        if(EXISTE_PARTICION_EN_EXTENDIDA(DISCO,fdisk._name)==true){
                            APLICAR_AJUSTES_PARTITION_PYE(DISCO,fdisk);
                        }else{
                            imprimirln("===ERROR: YA EXISTE UNA PARTICION LOGICA CON ESE NOMBRE ===");
                        }



                    }else{

                        imprimirln("++++ ALCANZO EL LIMITE DE PARTICIONES PRIMARIAS PERMITIDAS===");

                    }
                    //MOSTRAR_INFORMACION_DISCO(DISCO);

                   // imprimirNumln(CANTIDAD_PARTICIONES_PRIMARIAS(DISCO));
                }


            }


            //imprimirln("entro a ejecucion fdisk");

        }else{// SI SE UTILIZO EL COMANDO DELETE SE REALIZARA ESTA ACCION

        }

    }

}

void EJECUCION_MOUNT(struct cmdMOUNT mount){
    if(EXISTE_DISCO_O_ARCHIVO(mount._path)){//VALIDA QUE EXISTA EL ARCHIVO
        struct MBRD_DISK disco;
        disco=OBTENER_MBR_DEL_DISCO(mount._path);

        if((EXISTE_PARTICION_EN_DISCO(disco,mount._name)==false) || (EXISTE_PARTICION_EN_EXTENDIDA(disco,mount._name)==false)){
            bool ESTA_EN_PARTICION_PYE=EXISTE_PARTICION_EN_DISCO(disco,mount._name);

           // if(ESTA_EN_PARTICION_PYE==false){//SI ESTO ES VERDADERO SIGNIFICA QUE LA PARTICION ES UNA PARTICION PRIMARIA O EXTENDIDA
                //int INDICE_PARTICION_PYE=OBTENER_INDICE_PARTICION_EXTENDIDAYPRIMARA(disco,mount._name);

             //   if(strcmp(disco.mbr_partition[INDICE_PARTICION_PYE].part_name,mount._name.c_str())==0){
              //      imprimirln("SI ENTRO PARA PARTICIONES");

                        if(EXISTE_LLAVE_PATH(mount._path)){//ESTO SE REALIZA SI YA EXISTE LA LLAVE CON EL PATH
                        /////////////////////////
                       // int contador=0;
                           // bool MONTARPARTICION=false;

                         /*   while((EXISTE_LLAVE(VALORES[contador])) && (contador<26) ){
                                contador++;
                            }*/
                            string temporalLlave=RETORNAR_LLAVE_PATH(mount._path);

                            string temporal_concatena;
                            bool salir=false;
                            for(int i=1; (i<10) && (salir==false); i++){
                                temporal_concatena="";
                                if(i==1){
                                temporal_concatena="vd"+temporalLlave+"1";
                                }else if(i==2){
                                temporal_concatena="vd"+temporalLlave+"2";
                                }else if(i==3){
                                temporal_concatena="vd"+temporalLlave+"3";
                                }else if(i==4){
                                temporal_concatena="vd"+temporalLlave+"4";
                                }else if(i==5){
                                temporal_concatena="vd"+temporalLlave+"5";
                                }else if(i==6){
                                temporal_concatena="vd"+temporalLlave+"6";
                                }else if(i==7){
                                temporal_concatena="vd"+temporalLlave+"7";
                                }else if(i==8){
                                temporal_concatena="vd"+temporalLlave+"8";
                                }

                                if(i<9){

                                        if(EXISTE_LLAVE_REGISTRO(temporal_concatena)==false){

                                            if(EXISTE_PARTICION_MONTADA(mount._name)==false){

                                                ////
                                                 salir=true;
                                                struct REGISTRO nuevoRegistro;
                                                nuevoRegistro._llave=temporal_concatena;
                                                nuevoRegistro._path=mount._path;
                                                nuevoRegistro._nombreParticion=mount._name;
                                                MONTAR_PARTICION.push_back(nuevoRegistro);
                                                imprimirln("++++ PARTICION MONTADA EXITOSAMENTE(2) ++++");

                                                ////

                                            }else{
                                                salir=true;
                                                imprimirln("++++ ESTA PARTICION YA SE ENCUENTRA MONTADA ++++");
                                            }
                                        }/*else{
                                            salir=true;
                                            imprimirln("++++ ESTA PARTICION YA SE ENCUENTRA MONTADA ++++");
                                        }*/

                                }else{
                                    imprimirln("++++ YA SE MONTARON TODAS LAS PARTICIONES ++++");
                                }
                            }
                        /////////////////////////
                        }else{

                            int contador=0;

                            while((EXISTE_LLAVE(VALORES[contador])) && (contador<25) ){
                                contador++;
                            }

                            if(contador<25){

                            string temporal="vd"+VALORES[contador]+"1";
                            struct CLAVE nuevaClave;
                            struct REGISTRO nuevoRegistro;
                            nuevaClave._llave=VALORES[contador];
                            nuevaClave._path=mount._path;
                            nuevoRegistro._llave=temporal;
                            nuevoRegistro._nombreParticion=mount._name;
                            nuevoRegistro._path=mount._path;
                            VALORES_CLAVE.push_back(nuevaClave);
                            MONTAR_PARTICION.push_back(nuevoRegistro);
                             imprimirln("++++ PARTICION MONTADA EXITOSAMENTE(1) ++++");
                            }else{

                                imprimirln("++++ SE ALCANZO EL NUMERO MAXIMO DE MONTAJES A REALIZAR ++++");
                            }



                        }


           //     }
           /*     else{
                    imprimirln("=== ERROR: AL OBTENER LA PARTICION (MOUNT)===");
                }*/


         //   }
        /*    else{//SI ENTRA AQUI SIGNIFICA QUE LA PARTICION ES UNA PARTICION LOGICA
                int INDICE_PARTICION_LOGICA=OBTENER_INDICE_PARTICION_LOGICA(disco,mount._name);

                if(strcmp(disco.mbr_partitionEXT[INDICE_PARTICION_LOGICA].part_name,mount._name.c_str())==0){
                    imprimirln("SE VERIFICO QUE SI ES LOGICA");
                }else{
                    imprimirln("=== ERROR: AL OBTENER LA PARTIICON LOGICA (MOUNT)");
                }
            }*/


        }else{
            imprimirln("=== ERROR: LA PARTICION INDICADA NO EXISTE EN EL DISCO ESPECIFICADO ===");
        }

       // MOSTRAR_INFORMACION_DISCO(disco);
        MOSTRAR_INF_MONTAJES();
    }
}

void EJECUCION_MKFS(struct cmdMKFS mkfs){
    //OBTENEMOS LOS DATOS MONTADOS DE LA PARTICION
    struct REGISTRO DATOS_PART_MONTADA;
    DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(mkfs._id);
    //OBTENEMOS LOS DATOS DEL MBR DEL DISCO QUE TIENE LA PARTICION
    struct MBRD_DISK DISCO;
    DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);
    //VALIDAMOS QUE LA PARTICION SEA UNA PARTICION PRIMARIA O EXTENDIDA
    if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){
        //OBTENEMOS LOS DATOS DE LA PARTICION
        struct PARTITION_MBR_DISK DATOS_PARTICION;
        DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

        if(DATOS_PARTICION.part_type==112){//VALIDAMOS QUE SEA UNA PARTICION PRIMARIA LA CUAL SE DESEA FORMATEAR
        ////******
            //OBTENEMOS LA CANTIDAD DE INODOS QUE SE DEBEN CREAR
            int VALOR_N;
            string NOMBRE_DISCO=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path);

            /*imprimirln("mostrando N");
            imprimirNumln(VALOR_N);
            cout<<DATOS_PART_MONTADA._llave<<endl;
            cout<<DATOS_PART_MONTADA._nombreParticion<<endl;
            cout<<DATOS_PART_MONTADA._path<<endl;
            cout<<DATOS_PARTICION.part_name<<endl;
            cout<<DATOS_PARTICION.part_size<<endl;*/
           // if(mkfs._fs=="2fs"){
                VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                //SE FORMATEAN LOS BITMAPS DE INODOS Y BLOQUES DEL DISCO Y SU RAID
                FORMATEO_FAST_FS(DATOS_PART_MONTADA._path,DATOS_PARTICION.part_start+sizeof(Super_Bloque),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N,VALOR_N);
              //  FORMATEO_FAST_FS(OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path),DATOS_PARTICION.part_start+sizeof(Super_Bloque),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N,VALOR_N);

                if(mkfs._type=="full"){//VERFICA SI SE DESEA HACER UN FORMATE FULL
                    FORMATE_FULL_FS(DATOS_PART_MONTADA._path,DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos)),VALOR_N);
                //    FORMATE_FULL_FS(OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos)),VALOR_N);
                    imprimirln("++++ FORMATEO FULL REALIZADO CORRECTAMENTE ++++");
                }else{
                    imprimirln("++++ FORMATEO FAST REALIZADO CORRECTAMENTE ++++");
                }
                //PARA EL TIME DEL INODO Y EL SUPER BLOQUE
                CREAR_ARCHIVO_AUXILIAR_BLOQUES(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion,DATOS_PART_MONTADA._nombreParticion,(VALOR_N*3));
                FORMATEO_FAST_FS(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin",0,VALOR_N,VALOR_N);
                srand(time(NULL));
                //imprimirln("entra a 2fs");
                //CREAMOS Y LLENAMOS EL SUPER BLOQUE
                struct Super_Bloque SUPER_BLOQUE;
                SUPER_BLOQUE.s_filesystem_type=3;
                SUPER_BLOQUE.s_inodes_count=VALOR_N;
                SUPER_BLOQUE.s_blocks_count=(3*VALOR_N);
                SUPER_BLOQUE.s_free_inodes_count=VALOR_N-2;
                SUPER_BLOQUE.s_free_blocks_count=(3*VALOR_N)-2;
                SUPER_BLOQUE.s_mtime=time(NULL);
                SUPER_BLOQUE.s_umtime=time(NULL);
                SUPER_BLOQUE.s_mnt_count=1;
                SUPER_BLOQUE.s_magic=0xEF53;
                SUPER_BLOQUE.s_inode_size=sizeof(Tabla_Inodos);
                SUPER_BLOQUE.s_block_size=sizeof(B_Carpetas);
                SUPER_BLOQUE.s_first_ino=2;
                SUPER_BLOQUE.s_first_blo=2;
                SUPER_BLOQUE.s_bm_inode_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                SUPER_BLOQUE.s_bm_block_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N;
                SUPER_BLOQUE.s_inode_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N);
                SUPER_BLOQUE.s_block_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos));
                //CREA EL SUPER BLOQUE EN EL DISCO Y EN EL RAID
                GUARDAR_SUPERBLOQUE_DISCO(DATOS_PART_MONTADA._path,DATOS_PARTICION.part_start,SUPER_BLOQUE);

                string RUTA_AUXILIAR=NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
                FILE *archivoOriginal;
                FILE *archivoEstra;

                archivoOriginal=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                archivoEstra=fopen(RUTA_AUXILIAR.c_str(),"rb+");

                ////------------------------------ACTUALIZANDO LOS BITMAP DE INODOS, ARCHIVOS Y BLOQUES CARPETAS
                 //****actualizando inodo
                 int posicion_bitmap_inodo=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                 char actualizar=49;
                 fseek(archivoOriginal,posicion_bitmap_inodo,SEEK_SET);
                 fwrite(&actualizar,1,1,archivoOriginal);

                 fseek(archivoOriginal,(posicion_bitmap_inodo+1),SEEK_SET);
                 fwrite(&actualizar,1,1,archivoOriginal);
                 //****actualizando bloque carpeta
                 fseek(archivoEstra,0,SEEK_SET);
                 fwrite(&actualizar,1,1,archivoEstra);
                 //****actualizando bloque archivo
                 fseek(archivoEstra,(2*VALOR_N),SEEK_SET);
                 fwrite(&actualizar,1,1,archivoEstra);
                /////-----------------------------------------DATOS DE LOS INODOS
                    struct Tabla_Inodos INODORAIZ;
                    struct Tabla_Inodos INODOUSERS;
                    INODORAIZ.i_uid=1;
                    INODORAIZ.i_gid=1;
                    INODORAIZ.i_type=0;
                    INODORAIZ.i_size=0;
                    INODORAIZ.i_perm=777;
                    INODORAIZ.i_atime=time(NULL);
                    INODORAIZ.i_ctime=time(NULL);
                    INODORAIZ.i_mtime=time(NULL);
                    for(int i=0; i<16; i++){
                        INODORAIZ.i_block[i]=-1;
                    }
                    INODORAIZ.i_block[0]=0;
                    fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)),SEEK_SET);
                    fwrite(&INODORAIZ,sizeof(Tabla_Inodos),1,archivoOriginal);

                    INODOUSERS.i_uid=1;
                    INODOUSERS.i_gid=1;
                    INODOUSERS.i_type=1;
                    INODOUSERS.i_size=27;
                    INODOUSERS.i_perm=664;
                    INODOUSERS.i_atime=time(NULL);
                    INODOUSERS.i_ctime=time(NULL);
                    INODOUSERS.i_mtime=time(NULL);
                    for(int i=0; i<16; i++){
                        INODOUSERS.i_block[i]=-1;
                    }
                    INODOUSERS.i_block[0]=0;
                    fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(1*sizeof(Tabla_Inodos))),SEEK_SET);
                    fwrite(&INODOUSERS,sizeof(Tabla_Inodos),1,archivoOriginal);

                /////-------------------------------------------DATOS PARA EL BLOQUE
                struct B_Carpetas CARPETA;
                for(int i=0; i<4; i++){
                    CARPETA.b_content[i].b_inodo=-1;
                }
                CARPETA.b_content[0].b_inodo=0;
                strcpy(CARPETA.b_content[0].b_name,".");
                CARPETA.b_content[1].b_inodo=0;
                strcpy(CARPETA.b_content[1].b_name,"..");
                CARPETA.b_content[2].b_inodo=1;
                strcpy(CARPETA.b_content[2].b_name,"users.txt");
                fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(VALOR_N*4)+(VALOR_N*sizeof(Tabla_Inodos))),SEEK_SET);
                fwrite(&CARPETA,sizeof(B_Carpetas),1,archivoOriginal);
                ////--------------------------------------------DATOS PARA EL BLOQUE ARCHIVOS
                struct B_Archivos ARCHIVO;
                strcpy(ARCHIVO.b_contentA,"1,G,root\n1,U,root,root,123\n");
                fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos))+(2*VALOR_N*sizeof(B_Archivos))),SEEK_SET);
                fwrite(&ARCHIVO,sizeof(B_Archivos),1,archivoOriginal);

                fflush(archivoOriginal);
                fclose(archivoOriginal);
                fflush(archivoEstra);
                fclose(archivoEstra);

             //   imprimirln("entrara archivo crear journaling");
                CREAR_ARCHIVO_JOURNALING(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion,DATOS_PART_MONTADA._nombreParticion);
                //-----ACTUALIZANDO EL ARCHIVO JOURNALING CON LA CREACION DE LA CARPETA "/" Y EL ARCHIVO USERS.TXT CON SUS DATOS
                struct JOURNALING DATOSJOUR1;
                struct JOURNALING DATOSJOUR2;
                strcpy(DATOSJOUR1.RUTA,"/");
                strcpy(DATOSJOUR1.CONTENIDO,"");
                //DATOSJOUR1.CP=00;
                string texto="/users.txt";
                strcpy(DATOSJOUR1.CP,"");
                DATOSJOUR1.FECHA_CREACION=time(NULL);
                strcpy(DATOSJOUR2.RUTA,texto.c_str());
                strcpy(DATOSJOUR2.CONTENIDO,"p_root.sh");
                //DATOSJOUR2.CP=00;
                strcpy(DATOSJOUR2.CP,"");
                DATOSJOUR2.FECHA_CREACION=time(NULL);
                //GUARDA EN EL ARCHIVO JOURNALING LA CREACION DE "/" Y EL ARCHIVO USERS.TXT CON SUS DATOS
                GUARDAR_STRUCT_JOURNALING(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/jour_"+DATOS_PART_MONTADA._nombreParticion+".bin",DATOSJOUR1);
                GUARDAR_STRUCT_JOURNALING(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/jour_"+DATOS_PART_MONTADA._nombreParticion+".bin",DATOSJOUR2);
                 //------------------------------OBTENER EL STRUCT DEL INODO INICIAL PERTENECIENTE A LA CARPETA RAIZ "/"----------------------------------

        ////******
        }else{
            imprimirln("=== ERROR: INTENTA FORMATEAR UNA PARTICION EXTENDIDA ===");
        }

    }else{//AQUI INGRESA SI DESEAMOS FORMATEAR UNA PARTICION LOGICA

        struct PARTITION_EBR_DISK DATOS_PARTICION;
        DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);

        if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
        ////¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡CODIGO COPIADO DE PRIMARIA
            int VALOR_N;
            string NOMBRE_DISCO=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path);
           // if(mkfs._fs=="2fs"){
                VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                //SE FORMATEAN LOS BITMAPS DE INODOS Y BLOQUES DEL DISCO Y SU RAID
                FORMATEO_FAST_FS(DATOS_PART_MONTADA._path,DATOS_PARTICION.part_start+sizeof(Super_Bloque),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N,VALOR_N);
              //  FORMATEO_FAST_FS(OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path),DATOS_PARTICION.part_start+sizeof(Super_Bloque),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N,VALOR_N);

                if(mkfs._type=="full"){//VERFICA SI SE DESEA HACER UN FORMATE FULL
                    FORMATE_FULL_FS(DATOS_PART_MONTADA._path,DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos)),VALOR_N);
                //    FORMATE_FULL_FS(OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N),DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos)),VALOR_N);
                    imprimirln("++++ FORMATEO FULL REALIZADO CORRECTAMENTE ++++");
                }else{
                    imprimirln("++++ FORMATEO FAST REALIZADO CORRECTAMENTE ++++");
                }
                //PARA EL TIME DEL INODO Y EL SUPER BLOQUE
                CREAR_ARCHIVO_AUXILIAR_BLOQUES(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion,DATOS_PART_MONTADA._nombreParticion,(VALOR_N*3));
                FORMATEO_FAST_FS(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin",0,VALOR_N,VALOR_N);
                srand(time(NULL));
                //imprimirln("entra a 2fs");
                //CREAMOS Y LLENAMOS EL SUPER BLOQUE
                struct Super_Bloque SUPER_BLOQUE;
                SUPER_BLOQUE.s_filesystem_type=3;
                SUPER_BLOQUE.s_inodes_count=VALOR_N;
                SUPER_BLOQUE.s_blocks_count=(3*VALOR_N);
                SUPER_BLOQUE.s_free_inodes_count=VALOR_N-2;
                SUPER_BLOQUE.s_free_blocks_count=(3*VALOR_N)-2;
                SUPER_BLOQUE.s_mtime=time(NULL);
                SUPER_BLOQUE.s_umtime=time(NULL);
                SUPER_BLOQUE.s_mnt_count=1;
                SUPER_BLOQUE.s_magic=0xEF53;
                SUPER_BLOQUE.s_inode_size=sizeof(Tabla_Inodos);
                SUPER_BLOQUE.s_block_size=sizeof(B_Carpetas);
                SUPER_BLOQUE.s_first_ino=2;
                SUPER_BLOQUE.s_first_blo=2;
                SUPER_BLOQUE.s_bm_inode_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                SUPER_BLOQUE.s_bm_block_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N;
                SUPER_BLOQUE.s_inode_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N);
                SUPER_BLOQUE.s_block_start=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos));
                //CREA EL SUPER BLOQUE EN EL DISCO Y EN EL RAID
                GUARDAR_SUPERBLOQUE_DISCO(DATOS_PART_MONTADA._path,DATOS_PARTICION.part_start,SUPER_BLOQUE);

                string RUTA_AUXILIAR=NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
                FILE *archivoOriginal;
                FILE *archivoEstra;

                archivoOriginal=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                archivoEstra=fopen(RUTA_AUXILIAR.c_str(),"rb+");

                ////------------------------------ACTUALIZANDO LOS BITMAP DE INODOS, ARCHIVOS Y BLOQUES CARPETAS
                 //****actualizando inodo
                 int posicion_bitmap_inodo=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                 char actualizar=49;
                 fseek(archivoOriginal,posicion_bitmap_inodo,SEEK_SET);
                 fwrite(&actualizar,1,1,archivoOriginal);

                 fseek(archivoOriginal,(posicion_bitmap_inodo+1),SEEK_SET);
                 fwrite(&actualizar,1,1,archivoOriginal);
                 //****actualizando bloque carpeta
                 fseek(archivoEstra,0,SEEK_SET);
                 fwrite(&actualizar,1,1,archivoEstra);
                 //****actualizando bloque archivo
                 fseek(archivoEstra,(2*VALOR_N),SEEK_SET);
                 fwrite(&actualizar,1,1,archivoEstra);
                /////-----------------------------------------DATOS DE LOS INODOS
                    struct Tabla_Inodos INODORAIZ;
                    struct Tabla_Inodos INODOUSERS;
                    INODORAIZ.i_uid=1;
                    INODORAIZ.i_gid=1;
                    INODORAIZ.i_type=0;
                    INODORAIZ.i_size=0;
                    INODORAIZ.i_perm=777;
                    INODORAIZ.i_atime=time(NULL);
                    INODORAIZ.i_ctime=time(NULL);
                    INODORAIZ.i_mtime=time(NULL);
                    for(int i=0; i<16; i++){
                        INODORAIZ.i_block[i]=-1;
                    }
                    INODORAIZ.i_block[0]=0;
                    fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)),SEEK_SET);
                    fwrite(&INODORAIZ,sizeof(Tabla_Inodos),1,archivoOriginal);

                    INODOUSERS.i_uid=1;
                    INODOUSERS.i_gid=1;
                    INODOUSERS.i_type=1;
                    INODOUSERS.i_size=27;
                    INODOUSERS.i_perm=664;
                    INODOUSERS.i_atime=time(NULL);
                    INODOUSERS.i_ctime=time(NULL);
                    INODOUSERS.i_mtime=time(NULL);
                    for(int i=0; i<16; i++){
                        INODOUSERS.i_block[i]=-1;
                    }
                    INODOUSERS.i_block[0]=0;
                    fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(1*sizeof(Tabla_Inodos))),SEEK_SET);
                    fwrite(&INODOUSERS,sizeof(Tabla_Inodos),1,archivoOriginal);

                /////-------------------------------------------DATOS PARA EL BLOQUE
                struct B_Carpetas CARPETA;
                for(int i=0; i<4; i++){
                    CARPETA.b_content[i].b_inodo=-1;
                }
                CARPETA.b_content[0].b_inodo=0;
                strcpy(CARPETA.b_content[0].b_name,".");
                CARPETA.b_content[1].b_inodo=0;
                strcpy(CARPETA.b_content[1].b_name,"..");
                CARPETA.b_content[2].b_inodo=1;
                strcpy(CARPETA.b_content[2].b_name,"users.txt");
                fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(VALOR_N*4)+(VALOR_N*sizeof(Tabla_Inodos))),SEEK_SET);
                fwrite(&CARPETA,sizeof(B_Carpetas),1,archivoOriginal);
                ////--------------------------------------------DATOS PARA EL BLOQUE ARCHIVOS
                struct B_Archivos ARCHIVO;
                strcpy(ARCHIVO.b_contentA,"1,G,root\n1,U,root,root,123\n");
                fseek(archivoOriginal,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos))+(2*VALOR_N*sizeof(B_Archivos))),SEEK_SET);
                fwrite(&ARCHIVO,sizeof(B_Archivos),1,archivoOriginal);

                fflush(archivoOriginal);
                fclose(archivoOriginal);
                fflush(archivoEstra);
                fclose(archivoEstra);

            //    imprimirln("entrara archivo crear journaling");
                CREAR_ARCHIVO_JOURNALING(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion,DATOS_PART_MONTADA._nombreParticion);
                //-----ACTUALIZANDO EL ARCHIVO JOURNALING CON LA CREACION DE LA CARPETA "/" Y EL ARCHIVO USERS.TXT CON SUS DATOS
                struct JOURNALING DATOSJOUR1;
                struct JOURNALING DATOSJOUR2;
                strcpy(DATOSJOUR1.RUTA,"/");
                strcpy(DATOSJOUR1.CONTENIDO,"");
                //DATOSJOUR1.CP=00;
                string texto="/users.txt";
                strcpy(DATOSJOUR1.CP,"");
                DATOSJOUR1.FECHA_CREACION=time(NULL);
                strcpy(DATOSJOUR2.RUTA,texto.c_str());
                strcpy(DATOSJOUR2.CONTENIDO,"p_root.sh");
                //DATOSJOUR2.CP=00;
                strcpy(DATOSJOUR2.CP,"");
                DATOSJOUR2.FECHA_CREACION=time(NULL);
                //GUARDA EN EL ARCHIVO JOURNALING LA CREACION DE "/" Y EL ARCHIVO USERS.TXT CON SUS DATOS
                GUARDAR_STRUCT_JOURNALING(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/jour_"+DATOS_PART_MONTADA._nombreParticion+".bin",DATOSJOUR1);
                GUARDAR_STRUCT_JOURNALING(NOMBRE_DISCO+"_"+DATOS_PART_MONTADA._nombreParticion+"/jour_"+DATOS_PART_MONTADA._nombreParticion+".bin",DATOSJOUR2);

           /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }else{
            imprimirln("==== ERROR: NO SE PUDO FORMATEAR LA PARTICION CON EL ID ESPECIFICADO ====");
        }

    }

}

void EJECUCION_LOGIN(struct cmdLOGIN login){
    //imprimirln("entro a metodo login");
    //VALIDO QUE NO ALLA UNA SESION ACTIVA
    if(DATOS_PIVOTE.size()!=1){
        //SIGNIFICA QUE SI EXISTE EL ID DE LA PARTICION A UTILIZAR MONTADA
        if(EXISTE_LLAVE_REGISTRO(login._id)){
            //OBTENGO LOS DATOS DE LA PARTICION MONTADA (LLAVE, PATH_DISCO, NOMBREPARTICION)
            struct REGISTRO DATOS_PARTICION_MONTADA;
            DATOS_PARTICION_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(login._id);
            //OBTENEMOS EL DISCO PARA ASI OBTENER LA PARTICION DEL DISCO
            struct MBRD_DISK DISCO;
            DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PARTICION_MONTADA._path);

            if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PARTICION_MONTADA._nombreParticion)==false){
                //SI ENTRA AQUI SIGNIFICA QUE LA PARTICION ES UNA PARTICION PRIMARIA O EXTENDIDA
                struct PARTITION_MBR_DISK PARTICION;
                PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PARTICION_MONTADA._nombreParticion);
                string NOMBRE_DISCO=OBTENER_NOMBRE_DISCO(DATOS_PARTICION_MONTADA._path);
                string PATH_JOURNA=NOMBRE_DISCO+"_"+DATOS_PARTICION_MONTADA._nombreParticion+"/"+"jour_"+DATOS_PARTICION_MONTADA._nombreParticion+".bin";
                string PATH_AREXTRA=NOMBRE_DISCO+"_"+DATOS_PARTICION_MONTADA._nombreParticion+"/"+DATOS_PARTICION_MONTADA._nombreParticion+".bin";
                //VALIDAMOS QUE SEA UNA PARTICION PRIMARIA
                if(PARTICION.part_type==112){
                //  OBTENEMOS LOS DATOS PARA LOS DATOS_PUBLICOS
                    struct DATOS_PUBLICOS datos;
                    datos.inicio_particion=PARTICION.part_start;
                    datos.valor_N=OBTENER_CANTIDAD_N_EXT2(PARTICION.part_size);
                    datos.path_archivoExtra=PATH_AREXTRA;
                    datos.path_journaling=PATH_JOURNA;
                    datos.path_disco=DATOS_PARTICION_MONTADA._path;
                    datos.path_raid=OBTENER_PATH_ARCHIVORAID(DATOS_PARTICION_MONTADA._path);
                    datos.g_id=0;
                    datos.u_id=0;
                    //AÑADIMOS ESTOS PRIMEROS DATOS A DATOSPIVOTE Y CON ESTO YA EXISTE UNA SESION ACTIVA
                    DATOS_PIVOTE.push_back(datos);
                   /* if(DATOS_PIVOTE.size()==1){
                        imprimirln(DATOS_PIVOTE[0].path_disco);
                        imprimirln(DATOS_PIVOTE[0].path_raid);
                        imprimirln(DATOS_PIVOTE[0].path_journaling);
                        imprimirln(DATOS_PIVOTE[0].path_archivoExtra);
                        imprimirNumln(DATOS_PIVOTE[0].inicio_particion);
                        imprimirNumln(DATOS_PIVOTE[0].valor_N);
                    }*/
                    string RUTA_ARCHIVO_USR="/users.txt";
                    char BUSCADOR[RUTA_ARCHIVO_USR.size()+1];
                    strcpy(BUSCADOR,RUTA_ARCHIVO_USR.c_str());
                    vector<string> BUSCAR=split(BUSCADOR,"/");
                    //VALIDO QUE EXISTE EL ARCHIVO USERS EN ESTA PARTICION, SI EXISTE SIGNIFICA QUE ESTA PARTICION FUE FORMATEADA
                    if(EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(BUSCAR,0,0)){
                        //RECUPERAMOS EL CONTENIDO DEL ARCHIVOS USERS.TXT
                        string contenidoArchivo=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(BUSCAR,0,0);

                        if(contenidoArchivo!=""){
                            //SE LLAMA AL METODO OBTENER_DIRECCION PARA ASI NO ESTAR REALIZANDO UN CHAR TEMPORAL PARA EL SPLIT
                            vector<string> SEPARADOR_UNO=OBTENER_DIRECCION(contenidoArchivo,"\n");
                            string GRUPO_USUARIO="";
                            if(SEPARADOR_UNO.size()>0){
                                //ESTE PRIMER FOR VA RECUPERAR EL ID DEL USUARIO Y SU GRUPO
                                for(int i=0; i<SEPARADOR_UNO.size(); i++){
                                    //CON ESTE VECTOR SE OBTENDRAN LOS DATOS DEL USUARIO PARA INICIAR SESION
                                    vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
                                    //ENTRARA A DATOS DEL USUARIO PARA RECUPERAR SU GRUPO Y SU ID
                                    if(SEPARADOR_DOS.size()==5){
                                        if((strcmp(SEPARADOR_DOS[3].c_str(),login._usr.c_str())==0) && (strcmp(SEPARADOR_DOS[4].c_str(),login._pwd.c_str())==0) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
                                            //RECUPERO EL ID DEL USUARIO Y EL GRUPO AL QUE PERTENENCE
                                            DATOS_PIVOTE[0].u_id=atoi(SEPARADOR_DOS[0].c_str());
                                            GRUPO_USUARIO=SEPARADOR_DOS[2];
                                            //imprimirln("mostrando su id, y grupo");
                                            //imprimirNumln(DATOS_PIVOTE[0].u_id);
                                            //imprimirln(GRUPO_USUARIO);
                                        }
                                    }
                                }
                                //ESTE SEGUNDO FOR RECUPERARA EL ID DEL GRUPO
                                for(int i=0; i<SEPARADOR_UNO.size(); i++){
                                    vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
                                    if((SEPARADOR_DOS.size()==3) && (DATOS_PIVOTE[0].u_id!=0) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
                                        if(strcmp(SEPARADOR_DOS[2].c_str(),GRUPO_USUARIO.c_str())==0){
                                            DATOS_PIVOTE[0].g_id=atoi(SEPARADOR_DOS[0].c_str());
                                        }
                                    }

                                }

                                if((DATOS_PIVOTE[0].g_id!=0) && (DATOS_PIVOTE[0].u_id!=0)){
                                    imprimirln("++++ SESION INICIADA CORRECTAMENTE ++++");
                               //     imprimirNumln(DATOS_PIVOTE[0].u_id);
                               //     imprimirNumln(DATOS_PIVOTE[0].g_id);
                                }else{
                                    DATOS_PIVOTE.clear();
                                    imprimirln("==== ERROR: VERFIQUE LOS DATOS PARA INICIAR SESION, USUARIO NO RECONOCIDO =====");
                                }

                            }else{
                                DATOS_PIVOTE.clear();
                                imprimirln("=== ERROR: NO SE PUDO VALIDAR AL USUARIO, INTENTELO NUEVAMENTE ===");
                            }

                        }else{
                            DATOS_PIVOTE.clear();
                            imprimirln("=== ERROR: NO SE PUEDO OBTENER EL CONTENIDO DEL ARCHIVO USERS.TXT PARA VALIDAR AL USUARIO INTENTELO NUEVAMENTE ====");
                        }



                    }else{
                        DATOS_PIVOTE.clear();
                        imprimirln("==== ERROR: ESTA PARTICION MONTADA NO A SIDO FORMATEADA ====");
                    }
                }else{
                    imprimirln("==== ERROR: INTENTA REALIZAR LOGIN A UNA PARTICION EXTENDIDA, INTENTE CON LOGICAS ===");
                }

            }else{//ESTE CODIGO ES PARA CUANDO SE QUIERE INICIAR SESION EN UNA PARTICION LOGICA
                struct PARTITION_EBR_DISK PARTICION;
                PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PARTICION_MONTADA._nombreParticion);
                string NOMBRE_DISCO=OBTENER_NOMBRE_DISCO(DATOS_PARTICION_MONTADA._path);
                string PATH_JOURNA=NOMBRE_DISCO+"_"+DATOS_PARTICION_MONTADA._nombreParticion+"/"+"jour_"+DATOS_PARTICION_MONTADA._nombreParticion+".bin";
                string PATH_AREXTRA=NOMBRE_DISCO+"_"+DATOS_PARTICION_MONTADA._nombreParticion+"/"+DATOS_PARTICION_MONTADA._nombreParticion+".bin";
                //PARA VERIFICAR QUE SEA LA PARTICION LOGICA
                if(strcmp(PARTICION.part_name,DATOS_PARTICION_MONTADA._nombreParticion.c_str())==0){
                   //000000000000000000000000000000000CODIGO COPIADO DE PRIMARIA
                    //  OBTENEMOS LOS DATOS PARA LOS DATOS_PUBLICOS
                    struct DATOS_PUBLICOS datos;
                    datos.inicio_particion=PARTICION.part_start;
                    datos.valor_N=OBTENER_CANTIDAD_N_EXT2(PARTICION.part_size);
                    datos.path_archivoExtra=PATH_AREXTRA;
                    datos.path_journaling=PATH_JOURNA;
                    datos.path_disco=DATOS_PARTICION_MONTADA._path;
                    datos.path_raid=OBTENER_PATH_ARCHIVORAID(DATOS_PARTICION_MONTADA._path);
                    datos.g_id=0;
                    datos.u_id=0;
                    //AÑADIMOS ESTOS PRIMEROS DATOS A DATOSPIVOTE Y CON ESTO YA EXISTE UNA SESION ACTIVA
                    DATOS_PIVOTE.push_back(datos);
                   /* if(DATOS_PIVOTE.size()==1){
                        imprimirln(DATOS_PIVOTE[0].path_disco);
                        imprimirln(DATOS_PIVOTE[0].path_raid);
                        imprimirln(DATOS_PIVOTE[0].path_journaling);
                        imprimirln(DATOS_PIVOTE[0].path_archivoExtra);
                        imprimirNumln(DATOS_PIVOTE[0].inicio_particion);
                        imprimirNumln(DATOS_PIVOTE[0].valor_N);
                    }*/
                    string RUTA_ARCHIVO_USR="/users.txt";
                    char BUSCADOR[RUTA_ARCHIVO_USR.size()+1];
                    strcpy(BUSCADOR,RUTA_ARCHIVO_USR.c_str());
                    vector<string> BUSCAR=split(BUSCADOR,"/");
                    //VALIDO QUE EXISTE EL ARCHIVO USERS EN ESTA PARTICION, SI EXISTE SIGNIFICA QUE ESTA PARTICION FUE FORMATEADA
                    if(EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(BUSCAR,0,0)){
                        //RECUPERAMOS EL CONTENIDO DEL ARCHIVOS USERS.TXT
                        string contenidoArchivo=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(BUSCAR,0,0);

                        if(contenidoArchivo!=""){
                            //SE LLAMA AL METODO OBTENER_DIRECCION PARA ASI NO ESTAR REALIZANDO UN CHAR TEMPORAL PARA EL SPLIT
                            vector<string> SEPARADOR_UNO=OBTENER_DIRECCION(contenidoArchivo,"\n");
                            string GRUPO_USUARIO="";
                            if(SEPARADOR_UNO.size()>0){
                                //ESTE PRIMER FOR VA RECUPERAR EL ID DEL USUARIO Y SU GRUPO
                                for(int i=0; i<SEPARADOR_UNO.size(); i++){
                                    //CON ESTE VECTOR SE OBTENDRAN LOS DATOS DEL USUARIO PARA INICIAR SESION
                                    vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
                                    //ENTRARA A DATOS DEL USUARIO PARA RECUPERAR SU GRUPO Y SU ID
                                    if(SEPARADOR_DOS.size()==5){
                                        if((strcmp(SEPARADOR_DOS[3].c_str(),login._usr.c_str())==0) && (strcmp(SEPARADOR_DOS[4].c_str(),login._pwd.c_str())==0) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
                                            //RECUPERO EL ID DEL USUARIO Y EL GRUPO AL QUE PERTENENCE
                                            DATOS_PIVOTE[0].u_id=atoi(SEPARADOR_DOS[0].c_str());
                                            GRUPO_USUARIO=SEPARADOR_DOS[2];
                                            //imprimirln("mostrando su id, y grupo");
                                            //imprimirNumln(DATOS_PIVOTE[0].u_id);
                                            //imprimirln(GRUPO_USUARIO);
                                        }
                                    }
                                }
                                //ESTE SEGUNDO FOR RECUPERARA EL ID DEL GRUPO
                                for(int i=0; i<SEPARADOR_UNO.size(); i++){
                                    vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
                                    if((SEPARADOR_DOS.size()==3) && (DATOS_PIVOTE[0].u_id!=0) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
                                        if(strcmp(SEPARADOR_DOS[2].c_str(),GRUPO_USUARIO.c_str())==0){
                                            DATOS_PIVOTE[0].g_id=atoi(SEPARADOR_DOS[0].c_str());
                                        }
                                    }

                                }

                                if((DATOS_PIVOTE[0].g_id!=0) && (DATOS_PIVOTE[0].u_id!=0)){
                                    imprimirln("++++ SESION INICIADA CORRECTAMENTE (L) ++++");
                                //    imprimirNumln(DATOS_PIVOTE[0].u_id);
                                //    imprimirNumln(DATOS_PIVOTE[0].g_id);
                                }else{
                                    DATOS_PIVOTE.clear();
                                    imprimirln("==== ERROR: VERFIQUE LOS DATOS PARA INICIAR SESION, USUARIO NO RECONOCIDO (L) =====");
                                }

                            }else{
                                DATOS_PIVOTE.clear();
                                imprimirln("=== ERROR: NO SE PUDO VALIDAR AL USUARIO, INTENTELO NUEVAMENTE (L) ===");
                            }
                        }else{
                            DATOS_PIVOTE.clear();
                            imprimirln("=== ERROR: NO SE PUEDO OBTENER EL CONTENIDO DEL ARCHIVO USERS.TXT PARA VALIDAR AL USUARIO INTENTELO NUEVAMENTE (L) ====");
                        }
                    }else{
                        DATOS_PIVOTE.clear();
                        imprimirln("==== ERROR: ESTA PARTICION MONTADA NO A SIDO FORMATEADA (L) ====");
                    }
                   //0000000000000000000000000000000000000000000000000000000000
                }else{
                    imprimirln("==== ERROR: NO SE PUDO INICIAR SESION CON EL ID ESPECIFICADO ====");
                }
            }

        }else{
            imprimirln("==== ERROR: NO SE ENCUENTRA NINGUNA PARTICION MONTADA CON ESE ID ====");
        }
    }else{
        imprimirln("==== ERRRO: USTED TIENE UNA SESION ACTIVA, CIERRE LA SESION PARA INICIAR UNA NUEVA ====");
    }

}

void EJECUCION_MKGRP(struct cmdMKGRP mkgrp){
    //SE VALIDA QUE EL USUARIO QUE DESEA USAR ESTE COMANDO SEA EL USUARIO ROOT
    if((DATOS_PIVOTE[0].u_id==1) && (DATOS_PIVOTE[0].g_id==1)){
        string PATH_ARCHIVOUSR="/users.txt";
        vector<string> DIRECCION=OBTENER_DIRECCION(PATH_ARCHIVOUSR,"/");
        string DATOS_ARCHIVO=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);

        if(!EXISTE_GRUPO_ARCHIVODISCO(DATOS_ARCHIVO,mkgrp._name)){//COMO ESTAMOS CREANDO GRUPO HAY QUE VALIDAR QUE NO EXISTA Y SI NO EXISTE RETORNA FALSE
            DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();//ELIMINA CUALQUIER BLOQUE AUN GUARDADO AQUI POR ERROR
            string DATOS_ACTUALIZADOS=CREAR_GRUPO_ARCHIVODISCO(DATOS_ARCHIVO,mkgrp._name);//CREA LOS DATOS CON EL NUEVO GRUPO
            ////DATOS_ACTUALIZADOS+="bienvenido, otro ejemplo, archivo prueba, esto es una pru, solo verificacion";

            DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(DATOS_ACTUALIZADOS);


         /*   for(int i=0; i<DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size(); i++){
                char temporal[64];
                imprimirln("*********************************");
                //imprimirln(TEXTONUEVO[i]);
                strcpy(temporal,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i].c_str());
                cout<<temporal<<endl;
            }*/

            ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);

            if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
                imprimirln("++++ GRUPO CREADO EXITOSAMENTE +++");
            }

            DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
        }else{
            imprimirln("==== ERROR: ESTE GRUPO YA EXISTE ====");
        }
    }else{
        imprimirln("==== ERROR: USTED NO ES EL USUARIO ROOT PARA EJECUTAR ESTE COMANDO ====");
    }
}

void EJECUCION_MKUSR(struct cmdMKUSR mkusr){
    if((DATOS_PIVOTE[0].u_id==1) && (DATOS_PIVOTE[0].g_id==1)){//SI PERMITE EL ACCESO ES EL USUARIO ROOT
            string PATHARCHIVOUSR="/users.txt";
            vector<string> DIRECCION=OBTENER_DIRECCION(PATHARCHIVOUSR,"/");
            string DATOS_ARCHIVO_USR=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);

            //imprimirln(DATOS_ARCHIVO_USR);
            if(EXISTE_GRUPO_ARCHIVODISCO(DATOS_ARCHIVO_USR,mkusr._grp)){//VALIDAMOS QUE EXSITA EL GRUPO
                if(!EXISTE_USUARIO_ARCHIVODISCO(DATOS_ARCHIVO_USR,mkusr._usr)){
                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                    string DATOS_ACTUALIZADOS=CREAR_USUARIO_ARCHIVODISCO(DATOS_ARCHIVO_USR,mkusr._grp,mkusr._pwd,mkusr._usr);

            //        imprimirln(DATOS_ACTUALIZADOS);

                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(DATOS_ACTUALIZADOS);

                 /*   for(int i=0; i<DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size(); i++){
                        char temporal[64];
                        imprimirln("..........................................................");
                        strcpy(temporal,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i].c_str());
                        cout<<temporal<<endl;
                    }*/

                    ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);

                    if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
                        imprimirln("++++ USUARIO CREADO EXITOSAMENTE ++++");
                    }else{
                        imprimirln("=== ERROR AL ACTUALIZAR LOS DATOS DE USERS.TXT ===");
                    }

                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                }else{
                    imprimirln("=== ERROR: EL USUARIO ESPECIFICADO YA EXISTE ===");
                }
            }else{
                imprimirln("===ERROR: EL GRUPO ESPECIFICADO NO EXISTE===");
            }

    }else{
        imprimirln("=== ERROR: USTED NO ES EL USUARIO ROOT PARA REALIZAR ESTE COMANDO ===");
    }
}


void EJECUCION_MKFILE(struct cmdMKFILE mkfile){
    string CONTENIDO_ARCHHIVO="";
    if(mkfile._cont!=""){//VALIDAMOS EL CONTENIDO QUE TENDRA EL ARCHIVO, DE CONT O SIZE
        if(EXISTE_DISCO_O_ARCHIVO(mkfile._cont)){
            CONTENIDO_ARCHHIVO=RECUPERAR_TEXT_ARCHIVOCOMPU(mkfile._cont);
            DATOS_PIVOTE[0]._p_carpeta=mkfile._cont;
        }
    }else
    if(mkfile._size!=""){
        CONTENIDO_ARCHHIVO=RECUPERAR_TEXT_SIZE(mkfile._size);
        DATOS_PIVOTE[0]._p_carpeta=mkfile._size;
    }
/*
    if(DATOS_PIVOTE[0]._p_carpeta==""){
        imprimirln("VALIDADO COMO NULO");
    }
*/
    if(mkfile._p!=""){
        DATOS_PIVOTE[0]._p_archivo=mkfile._p;
    }
/*
    if(DATOS_PIVOTE[0]._p_carpeta!=""){
        imprimirln("VALIDADO CMO NO NULO");
    }*/

    if(VALIDAR_EXTENSION_PATH(mkfile._path)){//VALIDAMOS EL TIPO DE EXTENSION PARA EL ARCHIVO QUE SE CREARA EN NUESTROS DISCOARCHIVO
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();//LIMPIAR POR SI HAY TEXTO DE UN ARCHIVO ANTERIOR

        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(CONTENIDO_ARCHHIVO);

/*        for(int i=0;i<DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size();i++){
            char temPrueba[64];
            imprimirln("¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿");
            strcpy(temPrueba,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i].c_str());
            cout<<temPrueba<<endl;
        }*/
        vector<string> DIRECCION=OBTENER_DIRECCION(mkfile._path,"/");
  //      imprimirln("CANTIDAD DE DATOS DE DATOSTEMRPOAL BLOQUES");
  //      imprimirNumln(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size());
 //       imprimirln("************************MOSTRANDO DATOS ANTIGUOS***************************************");
  //      string DATOSACTIVOS_VIEJOS=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);
  //      imprimirln(DATOSACTIVOS_VIEJOS);

   //     imprimirln("*******************************VERIFICAR QUE EXISTA O CREAR EL ACHIVO************************************");
        CREAR_ARCHIVO_ARCHIVODISCO(DIRECCION,0,0);

  //      imprimirln("*********************************ESCRIBIENDO EN EL ARCHIVO, NO DEBERIA APARECER MUCHAS COSA *************************");
        ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);
        /////codigo para el journaling

        /////codigo terminado
        if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
            imprimirln("+++++ ARCHIVO CREADO Y MODIFICADO EXITOSAMENTE +++++");
        }else{
            imprimirln("+++++ ARCHIVO CREADO EXITOSAMENTE PERO NO MODIFICADO ++++");
        }

    //    imprimirln("REGRESO DEL METODO RECURSIVO.................");

        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();//LIMPIA PARA QUE NO QUEEDE RESIDUOS DEL TEXTO QUE SE QUIERE CREAR
        DATOS_PIVOTE[0]._p_archivo="";
        DATOS_PIVOTE[0]._p_carpeta="";
    }

}


void EJECUCION_MKDIR(struct cmdMKDIR _mkdir){
    if(_mkdir._p!=""){
        DATOS_PIVOTE[0]._p_archivo=_mkdir._p;
    }

    //VALIDANDO LA EXTENSION POR SEGUNDA VEZ
    if(!VALIDAR_SINEXTENSION_PATH(_mkdir._path)){
        vector<string> DIRECCION=OBTENER_DIRECCION(_mkdir._path,"/");
        DATOS_PIVOTE[0]._p_carpeta="";
    //    imprimir("escribiendo la carpeta");
        CREAR_ARCHIVO_ARCHIVODISCO(DIRECCION,0,0);
     //   imprimirln("regresode crear carpeta");

        DATOS_PIVOTE[0]._p_archivo="";
        DATOS_PIVOTE[0]._p_carpeta="";
    }
}

void EJECUCION_CAT(struct cmdCAT cat){
    //VALIDANDO POR SEGUNDA VEZ EL FILE DE CAT
    if(VALIDAR_EXTENSION_PATH(cat._file)){
        vector<string> DIRECCION=OBTENER_DIRECCION(cat._file,"/");
        string DATOS_ARCHIVO=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);
        imprimirln("******************************   MOSTRANDO DATOS DEL ARCHIVO ****************************");
        imprimirln(DATOS_ARCHIVO);
        imprimirln("******************************  FIN DATOS ARCHIVO ************************************");

      //  imprimirln("mostrando informacion journaling");
      //  MOSTRAR_INFORMACION_JOURNALING(DATOS_PIVOTE[0].path_journaling);

    }
}

void EJECUCION_LOSS(struct cmdLOSS loss){
    if(EXISTE_LLAVE_REGISTRO(loss._id)){
        //DATOS DE LA PARTICION MONTADA
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(loss._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
               //     imprimirln("va a poder formatear partion primaria");
                    int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                    //SE MODIFICO LOS METODOS DE FORMATEO FAST Y FULL A PARA UN ARCHIVO GLOBAL ASI COMO EL DE RECUPERAR DISCO SE
                    //COLOCO A UN FILE GLOBAL, PONIENDO CMO INCLUDE A BLOQUE_ARCHIVOS YA QUE ARCHIVO_GLOBAL NO ESTABA
                    //DECLARADO EN EL ESPACIO DE DISCOS_PARTICIONES
                    //LA ULTIMA VEZ QUE SE PROBO EL PROGRAMA FUE ANTES DE PONER EN ESTE METODO LOS METODOS FAST Y FULL
                    //SOLO TRONO UNA VEZ DE TODAS LAS PRUBAS PERO FUE CUANDO SALIO INGRESE UN NUEVO COMANDOS
                    //ASI QUE PIENSO QUE POR ERROR DI ENTER DOS VECES CREANDO ASI QUE SALIERA ESE ERROR
                    //ESTO COLOCA EN /0 EL BITMAP DE INODOS Y BLOQUES
                    FORMATEO_FAST_FS(DATOS_PART_MONTADA._path,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)),(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N),VALOR_N);
                    FORMATE_FULL_FS(DATOS_PART_MONTADA._path,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)),(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos))),VALOR_N);
                    FORMATEO_FAST_FS(OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin",0,VALOR_N,VALOR_N);
            }else{
                imprimirln("=== ERROR: EN LA PARTICION NO SE PUEDE REALIZAR LOSS, PUEDE QUE SEA EXTENDIDA ===");
            }


        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
            //    imprimirln("=== se puede realizar parcion logica ===");
                int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                FORMATEO_FAST_FS(DATOS_PART_MONTADA._path,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)),(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+VALOR_N),VALOR_N);
                FORMATE_FULL_FS(DATOS_PART_MONTADA._path,(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)),(DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(VALOR_N*sizeof(Tabla_Inodos))),VALOR_N);
                FORMATEO_FAST_FS(OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin",0,VALOR_N,VALOR_N);
            }else{
                imprimirln("=== ERROR: LA PARTICION NO SE PUEDE REALIZAR LOSS ====");
            }

        }

    }else{
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
}

void EJECUCION_RECOVERY(struct cmdLOSS recovery){
    if(EXISTE_LLAVE_REGISTRO(recovery._id)){
        //CODIGO COPIADO DE LOSS
                //DATOS DE LA PARTICION MONTADA
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(recovery._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);
            //DATOS PARA DATOS_PIVOTE VERSION2
            string NOMBRE_ARC_EXTRA=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
               //     imprimirln("va a poder formatear partion primaria");

                    if(strcmp(DATOS_PIVOTE[0].path_archivoExtra.c_str(),NOMBRE_ARC_EXTRA.c_str())==0){
                        //PRIMERO VAMOS A CREAR UNICAMENTE EL INODO Y SU PRIMER APUNTADOR LO VAMOS APUNTAR A UN BLOQUE CARPETA VACIO

                        //LUEGO YA REALIZAMOS TODO LO NECESARIOS PARA RECUPERAR EL SISTEMA ESTO CON LOS DATOS DEL JOURNALING Y LOS METODOS YA CREADOS
                        //PAA LA CREACION DE CARPETAS Y ARCHIVOS SOLO HAY QUE DEFINIR CUAL ES CUAL
                        int CONTADOR=RECUPERAR_CONTADOR_JOURNALING(DATOS_PIVOTE[0].path_journaling);
                        CREAR_INODO_Y_BLOQUECARPETA(0,0);
                        for(int i=1; i<CONTADOR; i++){
                            struct JOURNALING TMP;
                            int posicion_recuperar=sizeof(CONTADOR_JOURNALING)+(i*sizeof(JOURNALING));
                            TMP=RECUPERAR_JOURNALING(DATOS_PIVOTE[0].path_journaling,posicion_recuperar);
                            //VERIFICAMOS SI ES CARPETA 0 ARCHIVO.
                            if(VALIDAR_EXTENSION_SINMENSAJE(TMP.RUTA)){//SI ENTRA AQUI ES UN ARCHIVO
                                //SI ES ARCHIVO DEBEMOS IDENTIFICAR SI EL CONTNEIDO PROVIENE DE UN NUMERO O DE UNA RUTA
                                string CONTENIDO_ARCHIVO="";
                                if(validarNumero(TMP.CONTENIDO)){//SI ENTRA AQUI ES UN SIZE
                                    CONTENIDO_ARCHIVO=RECUPERAR_TEXT_SIZE(TMP.CONTENIDO);
                                    DATOS_PIVOTE[0]._p_carpeta=TMP.CONTENIDO;
                                }else{//EN CASO CONTRARIO ES DE UNA CARPETA
                                    if(strcmp(TMP.CONTENIDO,"p_root.sh")==0){
                                        CONTENIDO_ARCHIVO="1,G,root\n1,U,root,root,123\n";
                                    }else{
                                        CONTENIDO_ARCHIVO=RECUPERAR_TEXT_ARCHIVOCOMPU(TMP.CONTENIDO);
                                    }

                                    DATOS_PIVOTE[0]._p_carpeta=TMP.CONTENIDO;
                                }
                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(CONTENIDO_ARCHIVO);
                                if(strcmp(TMP.CP,"")!=0){
                                    DATOS_PIVOTE[0]._p_archivo=TMP.CP;
                                }
                                vector<string>DIRECCION=OBTENER_DIRECCION(TMP.RUTA,"/");
                       /*         imprimirln("mostrando datos obtenidos hasta ahora");
                                imprimirln(DATOS_PIVOTE[0]._p_carpeta);
                                imprimirln(DATOS_PIVOTE[0]._p_archivo);
                                imprimirln(CONTENIDO_ARCHIVO);
                                imprimirNumln(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size());*/

                                CREAR_ARCHIVO_ARCHIVODISCO(DIRECCION,0,0);

                                ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);


                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                                DATOS_PIVOTE[0]._p_archivo="";
                                DATOS_PIVOTE[0]._p_carpeta="";
                            }else{//EN CASO CONTRARIO ES UNA CARPETA
                        //        imprimirln("-------------------------es una carpeta-------------------------");

                                if(strcmp(TMP.CP,"")!=0){
                                    DATOS_PIVOTE[0]._p_archivo=TMP.CP;
                                }

                                vector<string> DIRECCION=OBTENER_DIRECCION(TMP.RUTA,"/");
                                CREAR_ARCHIVO_ARCHIVODISCO(DIRECCION,0,0);
                                DATOS_PIVOTE[0]._p_archivo="";
                                DATOS_PIVOTE[0]._p_carpeta="";

                            }
                        }


                    }else{
                        imprimirln("=== LA PARTICION DONDE DESEA REALIZAR RECOVERY NO ES LA MISMA EN LA QUE USTED SE ENCUENTRA INICIADA LA SESION ===");
                    }

            }else{
                imprimirln("=== ERROR: EN LA PARTICION NO SE PUEDE REALIZAR RECOVERY, PUEDE QUE SEA EXTENDIDA ===");
            }


        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
            //    imprimirln("=== se puede realizar parcion logica ===");
                string NOMBRE_ARC_EXTRA=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";

                if(strcmp(DATOS_PIVOTE[0].path_archivoExtra.c_str(),NOMBRE_ARC_EXTRA.c_str())==0){
                   ////
                   //PRIMERO VAMOS A CREAR UNICAMENTE EL INODO Y SU PRIMER APUNTADOR LO VAMOS APUNTAR A UN BLOQUE CARPETA VACIO

                        //LUEGO YA REALIZAMOS TODO LO NECESARIOS PARA RECUPERAR EL SISTEMA ESTO CON LOS DATOS DEL JOURNALING Y LOS METODOS YA CREADOS
                        //PAA LA CREACION DE CARPETAS Y ARCHIVOS SOLO HAY QUE DEFINIR CUAL ES CUAL
                        int CONTADOR=RECUPERAR_CONTADOR_JOURNALING(DATOS_PIVOTE[0].path_journaling);
                        CREAR_INODO_Y_BLOQUECARPETA(0,0);
                        for(int i=1; i<CONTADOR; i++){
                            struct JOURNALING TMP;
                            int posicion_recuperar=sizeof(CONTADOR_JOURNALING)+(i*sizeof(JOURNALING));
                            TMP=RECUPERAR_JOURNALING(DATOS_PIVOTE[0].path_journaling,posicion_recuperar);
                            //VERIFICAMOS SI ES CARPETA 0 ARCHIVO.
                            if(VALIDAR_EXTENSION_SINMENSAJE(TMP.RUTA)){//SI ENTRA AQUI ES UN ARCHIVO
                                //SI ES ARCHIVO DEBEMOS IDENTIFICAR SI EL CONTNEIDO PROVIENE DE UN NUMERO O DE UNA RUTA
                                string CONTENIDO_ARCHIVO="";
                                if(validarNumero(TMP.CONTENIDO)){//SI ENTRA AQUI ES UN SIZE
                                    CONTENIDO_ARCHIVO=RECUPERAR_TEXT_SIZE(TMP.CONTENIDO);
                                    DATOS_PIVOTE[0]._p_carpeta=TMP.CONTENIDO;
                                }else{//EN CASO CONTRARIO ES DE UNA CARPETA
                                    if(strcmp(TMP.CONTENIDO,"p_root.sh")==0){
                                        CONTENIDO_ARCHIVO="1,G,root\n1,U,root,root,123\n";
                                    }else{
                                        CONTENIDO_ARCHIVO=RECUPERAR_TEXT_ARCHIVOCOMPU(TMP.CONTENIDO);
                                    }

                                    DATOS_PIVOTE[0]._p_carpeta=TMP.CONTENIDO;
                                }
                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(CONTENIDO_ARCHIVO);
                                if(strcmp(TMP.CP,"")!=0){
                                    DATOS_PIVOTE[0]._p_archivo=TMP.CP;
                                }
                                vector<string>DIRECCION=OBTENER_DIRECCION(TMP.RUTA,"/");
                            /*    imprimirln("mostrando datos obtenidos hasta ahora");
                                imprimirln(DATOS_PIVOTE[0]._p_carpeta);
                                imprimirln(DATOS_PIVOTE[0]._p_archivo);
                                imprimirln(CONTENIDO_ARCHIVO);
                                imprimirNumln(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size());*/

                                CREAR_ARCHIVO_ARCHIVODISCO(DIRECCION,0,0);

                                ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);


                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                                DATOS_PIVOTE[0]._p_archivo="";
                                DATOS_PIVOTE[0]._p_carpeta="";
                            }else{//EN CASO CONTRARIO ES UNA CARPETA
                              //  imprimirln("-------------------------es una carpeta-------------------------");

                                if(strcmp(TMP.CP,"")!=0){
                                    DATOS_PIVOTE[0]._p_archivo=TMP.CP;
                                }

                                vector<string> DIRECCION=OBTENER_DIRECCION(TMP.RUTA,"/");
                                CREAR_ARCHIVO_ARCHIVODISCO(DIRECCION,0,0);
                                DATOS_PIVOTE[0]._p_archivo="";
                                DATOS_PIVOTE[0]._p_carpeta="";

                            }
                        }
                   ////
                }else{
                    imprimirln("=== NO SE PUEDE REALIZAR RECOVERY, NO TIENE SESION INICIADA EN DICHA PARTICION ===");
                }

            }else{
                imprimirln("=== ERROR: LA PARTICION NO SE PUEDE REALIZAR RECOVERY ====");
            }

        }
        //FIN CODIGO COPIADO
    }else{
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
}

void EJECUCION_EDIT(struct cmdEDIT edit){
    if(VALIDAR_EXTENSION_PATH(edit._path)){
        vector<string> DIRECCION=OBTENER_DIRECCION(edit._path,"/");
        if(EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(DIRECCION,0,0)){
            if(DIRECCION[0]!="users.txt"){
                string datos_antiguos=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);
                string nuevosdatos=datos_antiguos+edit._cont;
                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(nuevosdatos);

                ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);

               // imprimirln(nuevosdatos);

                if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
                    imprimirln("++++ ARCHIVO EDITADO EXITOSAMENTE ++++");
                }else{
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL EDITAR ARCHIVO, EDICION SIN COMPLETAR ===");
                }
                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
            }else{
                imprimirln("=== ERROR: PROHIBIDO MODIFICAR EL ARCHIVO USERS DESDE EDIT ===");
            }
        }else{
            imprimirln("=== ERROR: EL ARCHIVO ESPECIFICADO NO EXISTE ===");
        }
    }
}

void EJECUCION_RMGRP(struct cmdRMGRP rmgrp){
    string DIR="/users.txt";
    vector<string> DIRECCION=OBTENER_DIRECCION(DIR,"/");
    string DATOS_ACTUALES=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);

    if(EXISTE_GRUPO_ARCHIVODISCO(DATOS_ACTUALES,rmgrp._name)){
        string DATOS_ACTUALIZADOS=REMOVER_GRUPO(DATOS_ACTUALES,rmgrp._name);
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(DATOS_ACTUALIZADOS);

        ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);

        if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
            imprimirln("++++ GRUPO ELIMINADO EXITOSAMENTE ++++");
        }else{
            imprimirln("=== ERROR: NO SE PUDO COMPLETAR LA ELIMINACION ===");
        }
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
    }else{
        imprimirln("=== ERROR: EL GRUPO NO EXISTE ===");
    }

}

void EJECUCION_RMUSR(struct cmdRMUSR usr){
////
    string DIR="/users.txt";
    vector<string> DIRECCION=OBTENER_DIRECCION(DIR,"/");
    string DATOS_ACTUALES=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);

    if(EXISTE_USUARIO_ARCHIVODISCO(DATOS_ACTUALES,usr._usr)){
        string DATOS_ACTUALIZADOS=REMOVER_USUARIO(DATOS_ACTUALES,usr._usr);
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(DATOS_ACTUALIZADOS);

        ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);

        if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
            imprimirln("++++ USUARIO ELIMINADO EXITOSAMENTE ++++");
        }else{
            imprimirln("=== ERROR: NO SE PUDO COMPLETAR LA ELIMINACION ===");
        }
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
    }else{
        imprimirln("=== ERROR: EL USUARIO NO EXISTE ===");
    }

////
}

void EJECUCION_CHGRP(struct cmdCHGRP chgrp){
  ////
    string DIR="/users.txt";
    vector<string> DIRECCION=OBTENER_DIRECCION(DIR,"/");
    string DATOS_ACTUALES=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);

    if((EXISTE_USUARIO_ARCHIVODISCO(DATOS_ACTUALES,chgrp._usr)) && (EXISTE_GRUPO_ARCHIVODISCO(DATOS_ACTUALES,chgrp._grp))){
        string DATOS_ACTUALIZADOS=CAMBIAR_GRUPO_USUARIO(DATOS_ACTUALES,chgrp._usr,chgrp._grp);
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=TEXTO_BLOQUES_ARCHIVODISCO(DATOS_ACTUALIZADOS);

        ESCRIBIR_TEXT_EN_ARCHIVODISCO(DIRECCION,0,0);

       if(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()==0){
            imprimirln("++++ USUARIO ELIMINADO EXITOSAMENTE ++++");
        }else{
            imprimirln("=== ERROR: NO SE PUDO COMPLETAR LA ELIMINACION ===");
        }
        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.clear();
    }else{
        imprimirln("=== ERROR: EL USUARIO O GRUPO NO EXISTE ===");
    }
  ////
}

void EJECUCION_SYNCRONICE(struct cmdSYNCRONICE syncronice){
        // CONECTAR_ENVIAR_MENSAJE_SERVIDOR(RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS("INSERT INTO P\nAIS VALUES(10,'MEXICANAS')"));
     if(EXISTE_LLAVE_REGISTRO(syncronice._id)){
        //OBTENEMOS LOS DATOS DE PARTICION MONTADA Y DEL DISCO
        struct REGISTRO DATOS_PARTICION_MONTADA;
        DATOS_PARTICION_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(syncronice._id);

        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PARTICION_MONTADA._path);
        //SI ENTRA AQUI SE USARA UNA PARTICION MONTADA DE TIPO PRIMARIA O EXTENDIDA HABRA QUE VALIDAR CUAL DE LAS DOS
        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PARTICION_MONTADA._nombreParticion)==false){
           //OBTENEMOS LOS DATOS DE LA PARTICION
           struct PARTITION_MBR_DISK DATOS_PARTICION;
           DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PARTICION_MONTADA._nombreParticion);

           //VALIDAMOS SI ES PARTICION PRIMARIA
           if(DATOS_PARTICION.part_type==112){
                int Valor_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                struct PARAMETROS_SYNCRONICE_PRIMARIA param;
                param.path_disco=DATOS_PARTICION_MONTADA._path;
                param.inicio_particion=DATOS_PARTICION.part_start;
                param.valor_n=Valor_N;
                param.nombre_particion=DATOS_PARTICION_MONTADA._nombreParticion;
                param.nombre_disco=OBTENER_NOMBRE_DISCO(DATOS_PARTICION_MONTADA._path);


                //ALMACENANDO VALORES EN EL VECTOR DATOS PIVOTE YA QUE UTILIZA DATOS DE ESTE VECTOR EL METODO OBTENER TEXTO USAURIO ARCHIVODISOCO
                struct DATOS_PUBLICOS TMP_DATOS;
                bool existen_datos=false;
                if(DATOS_PIVOTE.size()==1){
                    TMP_DATOS=DATOS_PIVOTE[0];
                    existen_datos=true;
                    DATOS_PIVOTE.clear();
                }else{
                    DATOS_PIVOTE.clear();
                }

                struct DATOS_PUBLICOS DATOS_A_USAR;
                DATOS_A_USAR.g_id=1;
                DATOS_A_USAR.u_id=1;
                DATOS_A_USAR.inicio_particion=DATOS_PARTICION.part_start;
                DATOS_A_USAR.path_disco=DATOS_PARTICION_MONTADA._path;
                DATOS_A_USAR.valor_N=Valor_N;
                DATOS_PIVOTE.push_back(DATOS_A_USAR);

                /*
                imprimirln("IMPRIMIENDO DATOS OBTENIDOS");
                imprimirln(param.path_disco);
                imprimirln(param.nombre_disco);
                imprimirln(param.nombre_particion);
                imprimirNumln(param.inicio_particion);
                imprimirNumln(param.valor_n);*/

                vector<string> DIRECCION=OBTENER_DIRECCION("/users.txt","/");
                string DATOS_ARCH_USUARIOS=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);
                //SE RECUPERAN UNICAMENTE LOS NOMBRES DE LOS USUARIO PARA PODER RECORRELOS COMO VECTOR DE STRINGS
                vector<string> NOMBRES_USUARIOS_PARTICION=OBTENER_USUARIOS_DE_PARTICION(DATOS_ARCH_USUARIOS);

                for(int i=1; i<NOMBRES_USUARIOS_PARTICION.size(); i++){
                    param.nombre_particion=DATOS_PARTICION_MONTADA._nombreParticion+"_"+NOMBRES_USUARIOS_PARTICION[i];
                    imprimirln(param.nombre_particion);
                    //SE GUARDAN EL DISCO/PARTICION_USUARIO CON SUS RESPECTIVOS DATOS Y RESPECTIVOS ENLACES********************
                    string query="{CALL INSERCION_DISCO_PARTICION('"+param.nombre_disco+"','"+param.nombre_particion+"',"+to_string(DISCO.mbr_tamano)+","+to_string(DATOS_PARTICION.part_size)+")}";
                    CONECTAR_ENVIAR_MENSAJE_SERVIDOR(query);
                    INICIO_RECORRIDO_SYN_INODO(param,0,"/",0);
                }

                //SE GUARDAN LOS USUARIOS EN SU TABLA Y LA RELACION USUARIO->ID_DISCO_PARTICION************************************
                for(int i=1; i<NOMBRES_USUARIOS_PARTICION.size(); i++){
                    string nombre_particion=DATOS_PARTICION_MONTADA._nombreParticion+"_"+NOMBRES_USUARIOS_PARTICION[i];
                    string query="{CALL INSERTAR_USUARIO_CONSOLA('"+NOMBRES_USUARIOS_PARTICION[i]+"',3,1,'"+param.nombre_disco+"','"+nombre_particion+"')}";
                    CONECTAR_ENVIAR_MENSAJE_SERVIDOR(query);
                }
                //SE GUARDAN LA RELACION USUARIO->ID_PARTICION_DISCO*********************
               /* for(int i=1; i<NOMBRES_USUARIOS_PARTICION.size(); i++){
                    imprimirln(NOMBRES_USUARIOS_PARTICION[i]);
                }*/

                if(existen_datos==true){
                    DATOS_PIVOTE.clear();
                    DATOS_PIVOTE.push_back(TMP_DATOS);
                }else{
                    DATOS_PIVOTE.clear();
                }


           }else{
                imprimirln("=== ERROR: NO SE PERMITE SYNCRONIZACION DE PARTICIONES EXTENDIDAS ===");
           }
        }else{
            imprimirln("=== ERROR: SOLO SE PERMITE SYNCRONIZACION DE PARTICIONES PRIMARIAS, INTENTA USAR UNA PARTICION LOGIA ===");
        }

     }else{
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ESPECIFICO ===");
     }
}
//METODO CON EL QUE OBTENEMOS LOS VALORES DEL COMANDO MKDISK
struct cmdMKDISK R_CMD_MKDISK(vector<string> comandos,struct cmdMKDISK retornar){//RETORNAR STRUCT CON VALORES COMANDOS


    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="size") && (comandos.size()>1)){//COMANDO OBLIGATORIO

        if(comandos[1]!=" "){
            //imprimir(obtener_texto_sin_espacios(comandos[1]));
            //imprimirln("esto tiene que estar junto");
            string valor=obtener_texto_sin_espacios(comandos[1]);
            if(validarNumero(valor)){// VALIDA SI ES UN NUMERO LO QUE ESTA INGRESANDO
                retornar._size=atoi(valor.c_str());//LA FUNCION ATOI CONVERTE UN NUMERO STRING A ENTERO
               // imprimirNumln(retornar._size);
            }else{
                imprimirln("===ERROR: INGRESE NUMEROS EN EL COMANDO SIZE===");
            }
        }else{
            imprimirln("===ERROR: NO INGRESO UN NUMERO AL COMANDO SIZE===");
        }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){//COMANDO OBLIGATORIO

            if(comandos[1]!=" "){
                retornar._path=eliminarComillas(comandos[1]);
             //   imprimirln(retornar._path);
            }else{
                imprimirln("===ERROR: NO ESPECIFICO UNA RUTA PARA EL COMANDO PATH===");
            }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="fit") && (comandos.size()>1)){//COMANDO OPCIONAL
            if(comandos[1]!=" "){
                //imprimirln("ingreso a fit");
                if(validar_fit_mkdisk(comandos[1])){
                    retornar._fit=convertirMinusculla(obtener_texto_sin_espacios(comandos[1]));
                 //   imprimirln(retornar._fit);
                }else{
                    imprimirln("===ERROR: EL VALOR INGRESADO PARA FIT NO ES CORRECTO");
                }

            }else{
                imprimirln("===ERROR: NO INGRESO ALGUNO DE LOS VALORES PERMITIDOS PARA FIT===");
            }
    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="unit") && (comandos.size()>1)){//COMANDO OPCIONAL
            if(comandos[1]!=" "){
                //imprimirln("ingreso a unit");
                if(validar_unit_mkdisk(comandos[1])){//ESTE VALIDA QUE EL COMANDO UNIT SOLO ACEPTE K Y M PARA MKDISK

                    retornar._unit=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
                //    imprimirln(retornar._unit);
                }else{
                    imprimirln("===ERROR: EL VALOR INGRESADO PARA UNIT NO ES RECONOCIDO===");
                }

            }else{
                imprimirln("===ERROR: NO INGRESO VALORES PARA EL COMANDO UNIT===");
            }
    }else{
        imprimirln("===ERROR: ESTA USANDO UN COMANDO NO RECONOCIDO POR MKDISK===");
    }


    return retornar;
}

//METODO CON EL QUE OBTENEMOS LOS VALORES DEL COMANDO RMDISK

struct cmdRMDISK R_CMD_RMDISK(vector<string> comandos, struct cmdRMDISK retornar){

    if((convertirMinusculla(obtener_texto_sin_espacios(comandos[0]))=="path") && (comandos.size()>1)){

        if(comandos[1]!=""){
            retornar._path=eliminarComillas(comandos[1]);
        }else{
            imprimirln("===ERROR: NO ESPECIFICO UNA RUTA CORRECTA PARA EL COMANDO PATH DE RMDISK===");
        }
    }else{
        imprimirln("===ERROR: ESTA UTILIZANDO UN COMANDO NO RECONOCIDO POR RMDISK===");
    }

    return retornar;
}

struct cmdFDISK R_CMD_FDISK(vector<string> comandos, struct cmdFDISK retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="size") && (comandos.size()>1)){

        if(comandos[1]!=" "){

            string valor=obtener_texto_sin_espacios(comandos[1]);
            if(validarNumero(valor)){
                //imprimirln("entro a size");
                retornar._size=atoi(valor.c_str());
            }else{
                imprimirln("===ERROR: NO ESTA INGRESANDO NUMEROS POSITIVOS AL COMANDO SIZE===");
            }

        }else{
            imprimirln("=== ERROR: NO INGRESO UN NUMERO AL COMANDO SIZE===");
        }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="unit") && (comandos.size()>1)){

        if(comandos[1]!=" "){

            if(VALIDAR_UNIT_FDISK(comandos[1])){
               // imprimirln("entro a unit ");
                retornar._unit=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }else{
                imprimirln("=== ERROR: EL VALOR INTRODUCIDO PARA UNIT NO ES ACEPTADO===");
            }

        }else{
            imprimirln("=== ERROR: NO INGRESO UN VALOR AL COMANDO UNIT===");
        }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            //imprimirln("ingreso a path");
            if(VALIDAR_EXTENSION_PATH(eliminarComillas(comandos[1]))){
                retornar._path=eliminarComillas(comandos[1]);
            }
        }else{
            imprimirln("=== ERROR: EL VALOR INGRESADO A PATH ES VACIO===");
        }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="type") && (comandos.size()>1)){

        if(comandos[1]!=" "){

            if(VALIDAR_TYPE_FDISK(comandos[1])){
                //imprimirln("entro a type");
                retornar._type=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }else{
                imprimirln("=== ERROR: EL VALOR INTRODUCIDO PARA TYPE NO ES RECONOCIDO===");
            }

        }else{
            imprimirln("===ERROR: ESTA INGRESANDO VACIO AL COMANDO TYPE===");
        }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="fit") && (comandos.size()>1)){
        if(comandos[1]!=" "){

            if(validar_fit_mkdisk(comandos[1])){//SE UTILIZA FIT DE MKDISK YA QUE SON LOS MISMOS VALORES LOS QUE ACEPTA
                //imprimirln("ingreso a fit");
                retornar._fit=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }else{
                imprimirln("=== ERROR: NO SE RECONOCIO EL VALOR ASIGNADO A FIT===");
            }

        }else{
            imprimirln("=== ERROR: ESTA INGRESANDO VALORES VACIOS A FIT===");
        }

    }else

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="delete") && (comandos.size()>1)){

        if(comandos[1]!=" "){

            if(VALIDAR_DELETE_FDISK(comandos[1])){
               // imprimirln("entro a delete");
                retornar._delete=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }else{
                imprimirln("===ERROR: EL VALOR INTRODUCIDO A DELETE NO ES RECONOCIDO===");
            }


        }else{
            imprimirln("=== ERROR: ESPACIO VACIO EN DELETE===");
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="name") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            retornar._name=eliminarComillas(comandos[1]);
           // imprimirln(retornar._name);
        }else{

            imprimirln("===ERROR: COLOCO VACIO AL COMANDO NAME===");
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="add") && (comandos.size()>1)){
    //    imprimirln("si entro add");
        if(comandos[1]!=" "){
            //imprimirln(obtener_texto_sin_espacios(comandos[1]));
            if(VALIDAR_NUMEROS_NEGYPOST(obtener_texto_sin_espacios(comandos[1]))){//VALIDA QUE LE VENTA UN NUMERO YA SEA POSITIVO O NEGATIVO
                retornar._add=atoi(obtener_texto_sin_espacios(comandos[1]).c_str());
               // imprimirNumln(retornar._add);
            }else{
                imprimirln("===ERROR: NO ESTA ASIGNANDO UN ENTERO AL COMANDO ADD===");
            }

        }else{
            imprimirln("===ERROR: COLOCO UN VALOR NULO PARA ADD===");
        }

    }else{
        imprimirln("====ERROR: ESTA INGRESANDO UN COMANDO NO RECONOCIDO POR FDISK===");
    }

    return retornar;
}


struct cmdMOUNT R_CMD_MOUNT(vector <string> comandos,struct cmdMOUNT retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){

            if(comandos[1]!=" "){
                //imprimirln("ingreso a path");
                if(VALIDAR_EXTENSION_PATH(eliminarComillas(comandos[1]))){
                    retornar._path=eliminarComillas(comandos[1]);
                }
            }else{
                imprimirln("=== ERROR: EL VALOR INGRESADO A PATH ES VACIO===");
            }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="name") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            retornar._name=eliminarComillas(comandos[1]);
           // imprimirln(retornar._name);
        }else{

            imprimirln("===ERROR: COLOCO VACIO AL COMANDO NAME===");
        }

    }else{
        imprimirln("=== ERROR: ESTA INGRESANDO UN COMANDO NO RECONOCIDO POR MOUNT ===");
    }

    return retornar;
}

struct cmdUNMOUNT R_CMD_UNMOUNT(vector <string> comandos, struct cmdUNMOUNT retornar){
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="id") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            retornar.id=eliminarComillas(comandos[1]);
           // imprimirln(retornar._name);
        }else{

            imprimirln("===ERROR: COLOCO VACIO AL COMANDO ID===");
        }

    }else{
        imprimirln("=== ERROR: ESTA INGRESANDO UN COMANDO NO RECONOCIDO POR UNMOUNT ===");
    }
    return retornar;
}


struct cmdREP R_CMD_REP(vector <string> comandos, struct cmdREP retornar){
////
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){

            if(comandos[1]!=" "){
                //imprimirln("ingreso a path");
                if(VALIDAR_EXTENSION_PATH(eliminarComillas(comandos[1]))){
                    retornar._path=eliminarComillas(comandos[1]);
                }
            }else{
                imprimirln("=== ERROR: EL VALOR INGRESADO A PATH ES VACIO===");
            }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="name") && (comandos.size()>1)){

        if(VALIDAR_NAME_REP(obtener_texto_sin_espacios(convertirMinusculla(comandos[1])))){
                    if(comandos[1]!=" "){
                        retornar._name=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
                       // imprimirln(retornar._name);
                    }else{

                        imprimirln("===ERROR: COLOCO VACIO AL COMANDO NAME===");
                    }
        }else{
            imprimirln("=== EL VALOR ASIGNADO A NAME NO ES RECONOCIDO POR REP ===");
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="id") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            retornar._id=eliminarComillas(comandos[1]);
           // imprimirln(retornar._name);
        }else{

            imprimirln("===ERROR: COLOCO VACIO AL COMANDO ID===");
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="ruta") && (comandos.size()>1)){
        if(comandos[1]!=""){
            retornar._ruta=eliminarComillas(comandos[1]);
        }else{
            imprimirln("=== ERROR: COLOCA VACIO EL COMANDO RUTA ===");
        }
    }else{
        imprimirln("=== ERROR: ESTA INGRESANDO UN COMANDO NO RECONOCIDO POR REP ===");
    }

    return retornar;
/////
}

//

struct cmdEXEC Valor_path(vector<string> comando, struct cmdEXEC cmd){

    if((convertirMinusculla(comando[0])=="path") && (comando.size()>1)){
        cmd.path=eliminarComillas(comando[1]);
    }

    return cmd;
}

struct cmdMKFS R_CMD_MKFS(vector<string> comandos, struct cmdMKFS retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="id") && (comandos.size()>1)){

        if(comandos[1]!=" "){

            if(EXISTE_LLAVE_REGISTRO(obtener_texto_sin_espacios(convertirMinusculla(comandos[1])))==true){
                //imprimirln("entro a id");
                retornar._id=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }else{
                imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESTE ID ===");
            }

        }else{
            imprimirln("=== ERROR: COLOCO VACIO EL ID ===");
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="type") && (comandos.size()>1)){

        if(comandos[1]!=" "){

            if(VALIDACION_TYPE_MKFS(obtener_texto_sin_espacios(convertirMinusculla(comandos[1])))==true){
                //imprimirln("entro a type");
                retornar._type=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }


        }else{
            imprimirln("=== ERROR: COLOCO VACIO EL TYPE ===");
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="fs") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            if(VALIDACION_FS_MKFS(obtener_texto_sin_espacios(convertirMinusculla(comandos[1])))==true){
               // imprimirln("entro a fs");
                retornar._fs=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
            }
        }

    }else{
        imprimirln("=== ERROR: ESTA INGRESANDO UN PARAMETRO NO RECONOCIDO POR MKFS ===");
    }

    return retornar;
}

struct cmdLOGIN R_CMD_LOGIN(vector<string> comandos,struct cmdLOGIN retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="usr") && (comandos.size()>1)){
        //imprimirln("entro a usr");
        if(comandos[1]!=" "){
            retornar._usr=eliminarComillas(comandos[1]);
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="pwd") && (comandos.size()>1)){
       // imprimirln("entro a pwd");
        if(comandos[1]!=" "){
            retornar._pwd=eliminarComillas(comandos[1]);
        }

    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="id") && (comandos.size()>1)){
      //  imprimirln("entro id");
        if(comandos[1]!=" "){
            retornar._id=obtener_texto_sin_espacios(comandos[1]);
        }
    }else{
        imprimirln("==== ERROR: ESTA UTILIZANDO UN COMANDO NO RECONOCIDO POR LOGIN ====");
    }

    return retornar;
}

struct cmdMKGRP R_CMD_MKGRP(vector<string> comandos, struct cmdMKGRP retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="name") && (comandos.size()>1)){

        if(comandos[1]!=" "){
            retornar._name=eliminarComillas(comandos[1]);
        }

    }else{
        imprimirln(" ==== ERROR: ESTA UTILIZANDO UN COMANDO NO RECONOCIDO POR MKGRP ====");
    }

    return retornar;
}

struct cmdMKUSR R_CMD_MKUSR(vector<string> comandos, struct cmdMKUSR retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="usr") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._usr=NOMBRE_EXACTO_10CARACTERES(eliminarComillas(comandos[1]));
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="pwd") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._pwd=NOMBRE_EXACTO_10CARACTERES(eliminarComillas(comandos[1]));
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="grp") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._grp=NOMBRE_EXACTO_10CARACTERES(eliminarComillas(comandos[1]));
        }
    }else{
        imprimirln("=== ERROR: ESTA INGRESANDO UN COMANDO NO RECONOCIDO POR MKUSR ===");
    }

    return retornar;
}

struct cmdMKFILE R_CMD_MKFILE(vector<string> comandos,struct cmdMKFILE retornar){
    //imprimirln(comandos[0]);
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            if(VALIDAR_EXTENSION_PATH(eliminarComillas(comandos[1]))){
                retornar._path=eliminarComillas(comandos[1]);
            }
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="size")){
        if(comandos.size()>1){
            ////
            if(comandos[1]!=" "){
                if(validarNumero(obtener_texto_sin_espacios(comandos[1]))){
                    retornar._size=obtener_texto_sin_espacios(comandos[1]);
                }else{
                    imprimirln("=== ERROR: EL VALOR DE SIZE DEBE SER NUMERICO ===");
                }
            }
            ////
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="cont") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            if(VALIDAR_EXTENSION_PATH(eliminarComillas(comandos[1]))){
                retornar._cont=eliminarComillas(comandos[1]);
            }
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="p")){
        if(comandos.size()==1){
            retornar._p="p";
        }else{
            imprimirln("===ERROR: EL PARAMETRO P NO SE LE DEBE ASIGNAR NINGUN VALOR===");
        }
    }else{
        imprimirln("===ERROR: USTED ESTA INGREANDO UN PARAMETRO NO RECONOCIDO POR MKFILE===");
    }

    return retornar;
}

struct cmdMKDIR R_CMD_MKDIR(vector<string> comandos,struct cmdMKDIR retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            if(!VALIDAR_SINEXTENSION_PATH(eliminarComillas(comandos[1]))){
                retornar._path=eliminarComillas(comandos[1]);
            }
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="p")){
        if(comandos.size()==1){
            retornar._p="p";
        }else{
            imprimirln("=== ERROR: EL PARAMETRO P NO RECIBE NINGUN VALOR ===");
        }
    }else{
        imprimirln("=== ERROR: USTED ESTA UTILIZANDO UN PARAMETRO NO RECONOCIDO POR MKDIR ===");
    }

    return retornar;
}

struct cmdCAT R_CMD_CAT(vector<string> comandos,struct cmdCAT retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="file") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            if(VALIDAR_EXTENSION_PATH(eliminarComillas(comandos[1]))){
                retornar._file=eliminarComillas(comandos[1]);
            }
        }
    }else{
        imprimirln("=== ERROR: EL PARAMETRO USADO NO ES RECONOCIDO POR EL COMANDO CAT ===");
    }

    return retornar;
}

struct cmdLOSS R_CMD_LOSS(vector<string> comandos,struct cmdLOSS retornar){
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="id") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._id=obtener_texto_sin_espacios(comandos[1]);
        }
    }else{
        imprimirln("=== ERROR: ESTA USAANDO UN PARAMETRO NO RECONOCIDO  ===");
    }
    return retornar;
}

struct cmdEDIT R_CMD_EDIT(vector<string> comandos,struct cmdEDIT retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="path") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            if(VALIDAR_EXTENSION_PATH(comandos[1])){
                retornar._path=eliminarComillas(comandos[1]);
            }
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="cont") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._cont=comandos[1];
        }
    }else{
        imprimirln("=== ERROR: ESTA USANDO NO RECONOCIDO POR EDIT ===");
    }

    return retornar;
}

struct cmdRMGRP R_CMD_RMGRP(vector<string> comandos, struct cmdRMGRP retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="name") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._name=eliminarComillas(comandos[1]);
        }
    }else{
        imprimirln("=== ERROR: ESTA USANDO UN PARAMETRO NO RECONOCIDO POR RMGRP ===");
    }

    return retornar;
}

struct cmdRMUSR R_CMD_USR(vector<string> comandos,struct cmdRMUSR retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="usr") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._usr=eliminarComillas(comandos[1]);
        }
    }else{
        imprimirln("=== ERROR: EL PARAMETRO UTILIZADO NO ES RECONOCIDO POR RMUSR ===");
    }

    return retornar;
}

struct cmdCHGRP R_CMD_CHGRP(vector<string> comandos,struct cmdCHGRP retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="usr") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._usr=eliminarComillas(comandos[1]);
        }
    }else
    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="grp") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._grp=eliminarComillas(comandos[1]);
        }
    }else{
        imprimirln("=== ERROR: ESTA UTILIZANDO UN PARAMETRO NO RECONOCIDO POR CHGRP ===");
    }

    return retornar;
}

struct cmdSYNCRONICE R_CMD_SYNCRONICE(vector<string> comandos,struct cmdSYNCRONICE retornar){

    if((obtener_texto_sin_espacios(convertirMinusculla(comandos[0]))=="id") && (comandos.size()>1)){
        if(comandos[1]!=" "){
            retornar._id=obtener_texto_sin_espacios(convertirMinusculla(comandos[1]));
        }
    }else{
        imprimirln("=== ERROR: ESTA UTILIZANDO UN PARAMETRO NO RECONOCIDO POR SYNCRONICE ===");
    }

    return retornar;
}
//      ******      METODOS PARA LOS AJUSTES        ****

void APLICAR_AJUSTES_PARTITION_PYE(struct MBRD_DISK disco, struct cmdFDISK fdisk){
    //imprimirln("ingreso a primer ajuste");

    if(OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)<disco.mbr_tamano){//SI EL TAMAÑO ASIGNADO A LA PARTICION NO SOBREPASA EL DISCO SE EJECUTA ESTA PARTE SIGUIENTE
        int MatrizValores[5][3];//CREAMOS LA MATRIZ QUE NOS SIRVE COMO TABLA DE VALORES
        int MatrizEspacios[5][3];//CREAMOS LA MATRIZ DE ESPACIOS QUE NOS INDICARA LOS ESPACIOS DISPONIBLES
        int tamanioDisco=disco.mbr_tamano;
        int contadorMatrizEspacios=0;

        for(int i=0; i<5; i++){//INICIALIZA LA MATRIZ DE VALORES CON CEROS
            MatrizValores[i][0]=0;
            MatrizValores[i][1]=0;
            MatrizValores[i][2]=0;
            MatrizEspacios[i][0]=0;
            MatrizEspacios[i][1]=0;
            MatrizEspacios[i][2]=0;
        }

        int contador=0; //INICIAMOS EL CONTADOR PARA LLENAR CADA COLUMNA DE LA TABLA DE VALORES
        MatrizValores[contador][0]=0;       //ESTOS SON LOS VALORES DEL MBR LOS CUALES HAY QUE TENER EN CUENTA
        MatrizValores[contador][1]=(int)sizeof(MBRD_DISK);
        MatrizValores[contador][2]=(int)sizeof(MBRD_DISK);

        for(int i=0; i<4; i++){//OBTENEMOS LOS VALORES DE CADA PARTICION EXISTENTE
            if(disco.mbr_partition[i].part_status==49){//ENCONTRO UNA PARTICION ACTIVA
                //imprimirln("todo bien...");
                contador++;
                MatrizValores[contador][0]=disco.mbr_partition[i].part_start;
                MatrizValores[contador][1]=disco.mbr_partition[i].part_size;
                MatrizValores[contador][2]=((disco.mbr_partition[i].part_start)+(disco.mbr_partition[i].part_size));

            }
        }
          /*  contador++;//VALORES PARA PRUEBAS
            MatrizValores[contador][0]=19725;
            MatrizValores[contador][1]=1912115;
            MatrizValores[contador][2]=1931840;
            contador++;
            MatrizValores[contador][0]=18330;
            MatrizValores[contador][1]=100;
            MatrizValores[contador][2]=18430;
            contador++;
            MatrizValores[contador][0]=16325;
            MatrizValores[contador][1]=730;
            MatrizValores[contador][2]=17055;*/

   /*     imprimirln("mostrando primer for");
        //ESTE FOR ME IMPRIME ME MUESTRA LOS DATOS ANTES DE ORDENARLOS
         for(int j=0; (j<5) && (MatrizValores[j][2]!=0); j++){
            imprimirNumln(MatrizValores[j][0]);
            imprimirNumln(MatrizValores[j][1]);
            imprimirNumln(MatrizValores[j][2]);
        }*/

        //

        //FOR QUE VA A ORDENAR LOS DATOS
        for(int i=0; (i<5) && (MatrizValores[i][2]!=0); i++){

            for(int j=0; (j<5) && (MatrizValores[j][2]!=0); j++){

                if((MatrizValores[i][2])<(MatrizValores[j][2])){

                    int tmp1=MatrizValores[i][0];
                    int tmp2=MatrizValores[i][1];
                    int tmp3=MatrizValores[i][2];
                    //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                    MatrizValores[i][0]=MatrizValores[j][0];
                    MatrizValores[i][1]=MatrizValores[j][1];
                    MatrizValores[i][2]=MatrizValores[j][2];

                    MatrizValores[j][0]=tmp1;
                    MatrizValores[j][1]=tmp2;
                    MatrizValores[j][2]=tmp3;
                }

            }

        }

    /*    imprimirln("---mostrando datos ordenados---");
        //ME IMPRIME LOS DATOS ORDENDOS
        for(int j=0; (j<5) && (MatrizValores[j][2]!=0); j++){
            imprimirNumln(MatrizValores[j][0]);
            imprimirNumln(MatrizValores[j][1]);
            imprimirNumln(MatrizValores[j][2]);
        }
        imprimirln("************************");*/
        //CON ESTE FOR OBTENEMOS LOS ESPACIOS DISPONIBLES A OCUPAR EN EL DISCO
        for(int i=0; (i<5) && (MatrizValores[i][2]!=0); i++){

            if((MatrizValores[i+1][2]!=0) && (i+1<=4)){
                //imprimirNumln(MatrizValores[i+1][2]);
               /* imprimirNumln(MatrizValores[i+1][0]);
                imprimirNumln(MatrizValores[i][2]);*/
               // imprimirNumln(MatrizValores[i+1][1]-MatrizValores[i][2]);
                if(((MatrizValores[i+1][0])-(MatrizValores[i][2]))>5){

                    MatrizEspacios[contadorMatrizEspacios][0]=MatrizValores[i][2]+1;
                    MatrizEspacios[contadorMatrizEspacios][1]=MatrizValores[i+1][0]-MatrizValores[i][2]-2;
                    MatrizEspacios[contadorMatrizEspacios][2]=MatrizValores[i+1][0]-1;
                    contadorMatrizEspacios++;
                 /*   imprimirNumln(MatrizValores[i][2]+1);
                    imprimirNumln(MatrizValores[i+1][0]-MatrizValores[i][2]-2);
                    imprimirNumln(MatrizValores[i+1][0]-1);*/
                }

            }else{
                //imprimirln("error no entro que pts");
            }

        }
      /*  imprimirln("mostrando for de espacios 1");
        //FOR QUE MUESTRA LOS LUGARES CON ESPACIO
        for(int j=0; (j<5) && (MatrizEspacios[j][2]!=0); j++){
            imprimirNumln(MatrizEspacios[j][0]);
            imprimirNumln(MatrizEspacios[j][1]);
            imprimirNumln(MatrizEspacios[j][2]);
        }           */

        //...ESTOS VAN A TENER EL ESPACIO TOTAL DE PARTICIONES USADAS Y ESPACIO DISPONIBLE PARA SABER SI YA SE OCUPO TODO EL DISCO O AUN HAY ESPACIO NO REVISADO
        /*int sumador=0;

        for(int i=0; (i<5) && (MatrizValores[i][2]!=0); i++){
            sumador+=MatrizValores[i][1];
        }

        for(int i=0; (i<5) && (MatrizEspacios[i][2]!=0); i++){
            sumador+=MatrizEspacios[i][1];
        }
        imprimirln("tamanio total particiones dips y ocup");
        imprimirNumln(sumador);
        imprimirln("tamnio total diskco");
        imprimirNumln(disco.mbr_tamano);*/


        //FOR PARA OBTENER LOS DATOS DE LA ULTIMA PARTICION UTILIZADA EXISTENTE
        int bitInicialParticionFinal;
        int tamanioParticionFinal;
        int totalParticionFinal;
        for(int i=0; (i<5) && (MatrizValores[i][2]!=0);i++){
           // if((MatrizValores[i+1][2]==0) && (i+1<=4)){
                /*imprimirNumln(MatrizValores[i][0]);
                imprimirNumln(MatrizValores[i][1]);
                imprimirNumln(MatrizValores[i][2]);*/
                bitInicialParticionFinal=0;
                tamanioParticionFinal=0;
                totalParticionFinal=0;
                bitInicialParticionFinal=MatrizValores[i][0];
                tamanioParticionFinal=MatrizValores[i][1];
                totalParticionFinal=MatrizValores[i][2];
           // }
        }
     /*   imprimirln("-----");
        imprimirNumln(bitInicialParticionFinal);
        imprimirNumln(tamanioParticionFinal);
        imprimirNumln(totalParticionFinal);*/

        //COMPLETAMOS LA MATRIZ DE ESPACIOS DISPONIBLES CON LO ULTIMO QUE LE FALTABA DEL DISCO
        bool completado=false;
        for(int i=0; (i<5) && (completado==false); i++){
            if(MatrizEspacios[i][2]==0){
                completado=true;

                MatrizEspacios[i][0]=totalParticionFinal+1;
                MatrizEspacios[i][1]=tamanioDisco-totalParticionFinal-2;
                MatrizEspacios[i][2]=tamanioDisco-2;

            }
        }

 /*       imprimirln("mostrando for de espacios 2");
        //FOR QUE MUESTRA LOS LUGARES CON ESPACIO
        for(int j=0; (j<5) && (MatrizEspacios[j][2]!=0); j++){
            imprimirNumln(MatrizEspacios[j][0]);
            imprimirNumln(MatrizEspacios[j][1]);
            imprimirNumln(MatrizEspacios[j][2]);
        }*/

        //CON ESTO OBTENEMOS EL TOTAL DEL ESPACIO DISPONIBLE EN DISCO
        int sumador=0;

        for(int j=0; (j<5) &&(MatrizEspacios[j][2]!=0); j++){
            sumador+=MatrizEspacios[j][1];
        }
       // imprimirNumln(sumador);
       // imprimirNumln(OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit));

        if(sumador>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)){//SI ENTRA AQUI PODEMOS CREAR LA PARTICION CON SU RESPECTIVO AJUSTE
            //imprimirln("entro a prog");

            if(disco.mbr_fit==98){//APLICARA EL MEJOR AJUSTE

                if(EXISTE_PARTICION_EN_DISCO(disco,fdisk._name)){

                        //imprimirln("mejor ajuste");
                        int indiceUBICACION=0;
                        int TAMANIOVALIDACION=0;
                        bool validarTamanioINDESP=false;
                        for(int i=0; (i<4) && (validarTamanioINDESP==false); i++){
                            if(MatrizEspacios[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)){
                                indiceUBICACION=i;
                                TAMANIOVALIDACION=MatrizEspacios[i][1];
                                validarTamanioINDESP=true;
                            }
                        }

                        if(validarTamanioINDESP==true){//ENTRA AQUI SI EXISTE MAS DE ALGUN ESPACIO DISPONIBLE PARA LA PARTICION
                       /*     int indiceUBICACION=0;
                            int TAMANIOVALIDACION=MatrizEspacios[0][1];*/

                            for(int i=0; i<5; i++){//CON ESTO OBTENEMOS EL INDICE DEL ESPACIO MAS PEQUEÑO PERO MAS GRANDE PARA ALMACENAR PARTICION
                                if((TAMANIOVALIDACION>MatrizEspacios[i][1]) && (MatrizEspacios[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit))){
                                    indiceUBICACION=0;
                                    indiceUBICACION=i;
                                    TAMANIOVALIDACION=MatrizEspacios[i][1];
                                }
                            }
                            bool PARTICION_CREADA=false;
                            for(int k=0;(k<4) && (PARTICION_CREADA==false); k++){//-----------------------SE MODIFICO DE 5 A 4
                                if(disco.mbr_partition[k].part_status!=49){//SI ENCUENTRA UNA PARTICION QUE NO ESTA ACTIVA
        ///////////////////////////////////////////////////////////////////////////////////////
                                        PARTICION_CREADA=true;

                                        string status="1";
                                        disco.mbr_partition[k].part_status=status[0];           //ESCRIBIENDO LOS DATOS DE LA PRIMERA PARTICION
                                        disco.mbr_partition[k].part_type=fdisk._type[0];
                                        disco.mbr_partition[k].part_fit=fdisk._fit[0];
                                        disco.mbr_partition[k].part_start=MatrizEspacios[indiceUBICACION][0];
                                        disco.mbr_partition[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                       /* for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                            disco.mbr_partition[k].part_name[p]=fdisk._name[p];
                                        }*/

                                        for(int p=0; p<16; p++){

                                            if(p<fdisk._name.size()){
                                                disco.mbr_partition[k].part_name[p]=fdisk._name[p];
                                            }else{
                                                disco.mbr_partition[k].part_name[p]=00;
                                            }

                                        }
                                                    //imprimirln("permitido el acceso");
                                        GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                        GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                        imprimirln("+++++ LA PARTICION A SIDO CREADA EXITOSAMENTE (M.A:PE) ++++");

        /////////////////////////////////////////////////////////////////////////////////////////////////////
                                }
                            }


                        }else{
                            imprimirln("==== ERROR: NO EXISTE ESPACIO PARA EL TAMANIO DE ESTA PARTICION (M.A:PE)");
                        }

                }else{
                    imprimirln("=== ERROR: YA EXISTE LA PARTICION CON ESTE NOMBRE (M.A:PE) ====");
                }


            }else if(disco.mbr_fit==102){//APLICARA EL PRIMER AJUSTE
                //imprimirln("primer ajuse");

                bool validarTamanioINDESP=false;//CON ESTA INSTRUCCION VALIDAMOS QUE EXISTA AL MENOS UN ESPACIO DONDE QUEPA LA PARTICION
                for(int t=0;(t<5) && (validarTamanioINDESP==false); t++){
                    if(MatrizEspacios[t][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)){
                        validarTamanioINDESP=true;
                    }
                }

                if(validarTamanioINDESP==true){//VALIDA QUE EXISTA AL MENOS UN ESPACIO LIBRE PARA ESTA PARTICION
    ////////////////////////////////////////////

                    bool validoPrimerTamanio=false;

                    for(int i=0; (i<5) && (validoPrimerTamanio==false); i++){

                        if(MatrizEspacios[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)){//NO HACE FALTA COLOCAR MENSAJE DE ERROR YA QUE SI LLEGO AQUI SIGNIFICA QUE HAY UN ESPACIO PARA LA PARTICION
                                validoPrimerTamanio=true;

                                if(EXISTE_PARTICION_EN_DISCO(disco,fdisk._name)==true){
                                    //imprimirln("EL NOMBRE DE LA PARTICION NO EXISTE");
                                    bool particionCreada=false;

                                    for(int k=0; (k<4) && (particionCreada==false); k++){//---------------------SE CAMBIO DE 5 A 4

                                        if(disco.mbr_partition[k].part_status!=49){
                                            particionCreada=true;

                                           string status="1";

                                            disco.mbr_partition[k].part_status=status[0];           //ESCRIBIENDO LOS DATOS DE LA PRIMERA PARTICION
                                            disco.mbr_partition[k].part_type=fdisk._type[0];
                                            disco.mbr_partition[k].part_fit=fdisk._fit[0];
                                            disco.mbr_partition[k].part_start=MatrizEspacios[i][0];
                                            disco.mbr_partition[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                         /*   for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                                disco.mbr_partition[k].part_name[p]=fdisk._name[p];
                                            }*/


                                            ////
                                            for(int p=0; p<16; p++){

                                                if(p<fdisk._name.size()){
                                                    disco.mbr_partition[k].part_name[p]=fdisk._name[p];
                                                }else{
                                                    disco.mbr_partition[k].part_name[p]=00;
                                                }

                                            }

                                            ///
                                            //imprimirln("permitido el acceso");
                                          GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                          GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                          imprimirln("+++++ LA PARTICION A SIDO CREADA EXITOSAMENTE (PR.A:PE) ++++");
    //////////////////////////////////////////////////////////////

                                        }

                                    }

                                }else{
                                    imprimirln("=== ERROR: EL NOMBRE PARA LA PARTICION YA EXISTE (PR.A:PE)===");
                                }

                        }


                    }
///////////////////////////////////////////////////


                }else{
                    imprimirln("=== NO EXISTE ESPACIOS DISPOINBLES PARA EL TAMANIO INGRESADO (PR.A:PE)====");
                }
            }else if(disco.mbr_fit==119){//APLICARA EL PEOR AJUSTE
                //imprimirln("peor ajuste");
                if(EXISTE_PARTICION_EN_DISCO(disco,fdisk._name)){

                    bool VALIDARTAMANIO=false;
                    int INDICE_ESPACIO_DISP=0;
                    int PRIMER_VALOR=0;
                    for(int i=0; (i<5) && (VALIDARTAMANIO==false); i++){//ESTE FOR VALIDA QUE EXISTE ALGUN ESPACIO CON TAMAÑO SUFICIENTE PARA LA PARTCION
                        if(MatrizEspacios[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)){
                            INDICE_ESPACIO_DISP=i;
                            PRIMER_VALOR=MatrizEspacios[i][1];
                            VALIDARTAMANIO=true;
                        }
                    }

                    if(VALIDARTAMANIO==true){//SI EXISTE ALGUN ESPACIO SUFIENTE PARA LA PARTICION ENTRA AQUI
                      /*  int INDICE_ESPACIO_DISP=0;
                        int PRIMER_VALOR=MatrizEspacios[0][1];*/

                        for(int i=0; i<5; i++){//VERIFICA SI EL ESPACIO ACTUAL ES MENOR A OTRO ESPACIO MAS GRANDE Y SI ESE ESPACIO ES MAYOR AL TAMAÑO QUE NECESITA LA PARTICION
                            if((PRIMER_VALOR<MatrizEspacios[i][1]) && (MatrizEspacios[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit))){
                                INDICE_ESPACIO_DISP=0;
                                INDICE_ESPACIO_DISP=i;
                                PRIMER_VALOR=MatrizEspacios[i][1];
                            }
                        }

                        bool PARTITION_CREADA=false;
                        for(int k=0; (k<4) &&(PARTITION_CREADA==false); k++){//------------------ESTE NO SE MODIFICO

                            if(disco.mbr_partition[k].part_status!=49){
                                PARTITION_CREADA=true;
///////////////////////////////////////////////////////////////////////////
                                        string status="1";
                                        disco.mbr_partition[k].part_status=status[0];           //ESCRIBIENDO LOS DATOS DE LA PRIMERA PARTICION
                                        disco.mbr_partition[k].part_type=fdisk._type[0];
                                        disco.mbr_partition[k].part_fit=fdisk._fit[0];
                                        disco.mbr_partition[k].part_start=MatrizEspacios[INDICE_ESPACIO_DISP][0];
                                        disco.mbr_partition[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                      /*  for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                            disco.mbr_partition[k].part_name[p]=fdisk._name[p];
                                        }*/


                                        ///////
                                        for(int p=0; p<16; p++){

                                            if(p<fdisk._name.size()){
                                                disco.mbr_partition[k].part_name[p]=fdisk._name[p];
                                            }else{
                                                disco.mbr_partition[k].part_name[p]=00;
                                            }

                                        }

                                        //////

                                                    //imprimirln("permitido el acceso");
                                        GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                        GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                        imprimirln("+++++ LA PARTICION A SIDO CREADA EXITOSAMENTE (P.A:PE) ++++");
///////////////////////////////////////////////////////////////////////////
                            }
                        }


                    }else{
                        imprimirln("=== ERROR: NO EXISTE ESPACIO PARA EL TAMANIO DE ESTA PARTICION (P.A:PE)");
                    }

                }else{
                    imprimirln("=== ERROR: EL NOMBRE DE LA PARTICION YA EXISTE (P.A:PE) ===");
                }
            }

            MOSTRAR_INFORMACION_DISCO(disco);
        }else{
            imprimirln("==== ERROR: EL TAMANIO DISPOIBLE DEL DISCO NO ES SUFICIENTE ====");
        }

    }else{
        imprimirln("=== ERROR: EL TAMANIO ASIGNADO PARA PARTICION ES MAYOR AL TAMANIO DEL DISCO (P.A)");
    }
}


void APLICAR_AJUSTES_PARTITION_L(struct MBRD_DISK disco, struct cmdFDISK fdisk){
    //imprimirln("realizar particiones logicas");
    int INDICE_PARTICION_EXTENDIDA=OBTENER_INDICE_PARTITION_EXTENDIDA(disco);
    //strcmp(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_name,fdisk._name.c_str())==0
    //imprimirNumln(INDICE_PARTICION_EXTENDIDA);
    if(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_type==101){//VERIFICA QUE LA PARTICION SEA LA EXTENDIDA

        if(OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)<disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_size){//VERIFICA QUE EL TAMANIO DE LA PARTICION LOGICA NO SEA MAYOR AL DE LA PARTICION EXTENDIDA
            int MATRIZ_VALORES[20][3];
            int MATRIZ_ESPACIOS[20][3];

            for(int i=0; i<20; i++){//ESTE FOR INICIA LOS VALORES DE LAS MATRICES EN 0
                MATRIZ_VALORES[i][0]=0;
                MATRIZ_VALORES[i][1]=0;
                MATRIZ_VALORES[i][2]=0;
                MATRIZ_ESPACIOS[i][0]=0;
                MATRIZ_ESPACIOS[i][1]=0;
                MATRIZ_ESPACIOS[i][2]=0;
            }

            if(PARTICIONES_LOGICAS_VACIAS(disco)==true){//SI ESTA VACIA LAS PARTICIONES LOGICAS PODEMOS OCUPAR ESTE ESPACIO
               /* MATRIZ_VALORES[0][0]=disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_start+1;
                MATRIZ_VALORES[0][1]=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)-2;
                MATRIZ_VALORES[0][2]=disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_start+OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)-1;*/
////////////////////////////

                        bool PARTITION_CREADA=false;
                        for(int k=0; (k<20) &&(PARTITION_CREADA==false); k++){//------------------ESTE NO SE MODIFICO

                            if(disco.mbr_partitionEXT[k].part_status!=49){
                                PARTITION_CREADA=true;
///////////////////////////////////////////////////////////////////////////
                                        string status="1";
                                        disco.mbr_partitionEXT[k].part_status=status[0];
                                        disco.mbr_partitionEXT[k].part_fit=fdisk._fit[0];
                                        disco.mbr_partitionEXT[k].part_start=disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_start+1;
                                        disco.mbr_partitionEXT[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                        disco.mbr_partitionEXT[k].part_next=-1;
                                      /*  for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                            disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                        }*/


                                        /////
                                       for(int p=0; p<16; p++){

                                            if(p<fdisk._name.size()){
                                                disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                            }else{
                                                disco.mbr_partitionEXT[k].part_name[p]=00;
                                            }

                                        }


                                        /////
                                                    //imprimirln("permitido el acceso");
                                        GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                        GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                        imprimirln("+++++ LA PRIMERA PARTICION LOGICA A SIDO CREADA EXITOSAMENTE ++++");
///////////////////////////////////////////////////////////////////////////
                            }
                        }

////////////////////////////////////

       //         imprimirln("AGREGANDO PRIMERA PARTICION");
            }else{
                int contador=0;
                for(int i=0; i<20 ; i++){
                    if(disco.mbr_partitionEXT[i].part_status==49){
                        MATRIZ_VALORES[contador][0]=disco.mbr_partitionEXT[i].part_start;
                        MATRIZ_VALORES[contador][1]=disco.mbr_partitionEXT[i].part_size;
                        MATRIZ_VALORES[contador][2]=disco.mbr_partitionEXT[i].part_start+disco.mbr_partitionEXT[i].part_size;
                        contador++;
                    }
                }
///////////////////////////////////////////////////////////////////////////////////
                    //DATOS DE EJEMPLO
                 /*   MATRIZ_VALORES[1][0]=188565;
                    MATRIZ_VALORES[1][1]=17175;
                    MATRIZ_VALORES[1][2]=205740;*/

                /*    MATRIZ_VALORES[1][0]=193265;
                    MATRIZ_VALORES[1][1]=8000;
                    MATRIZ_VALORES[1][2]=201265;

                    MATRIZ_VALORES[2][0]=160120;
                    MATRIZ_VALORES[2][1]=5125;
                    MATRIZ_VALORES[2][2]=165245;

                    MATRIZ_VALORES[3][0]=173325;
                    MATRIZ_VALORES[3][1]=15238;
                    MATRIZ_VALORES[3][2]=188563;*/

                    //FIN DATOS EJEMPLO

                    //FOR QUE ORDENA LOS DATOS DE LA MATRIZ DE VALORES
                   for(int i=0; (i<20) && (MATRIZ_VALORES[i][2]!=0); i++){

                        for(int j=0; (j<20) && (MATRIZ_VALORES[j][2]!=0); j++){

                            if((MATRIZ_VALORES[i][2])<(MATRIZ_VALORES[j][2])){

                                int tmp1=MATRIZ_VALORES[i][0];
                                int tmp2=MATRIZ_VALORES[i][1];
                                int tmp3=MATRIZ_VALORES[i][2];
                                //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                                MATRIZ_VALORES[i][0]=MATRIZ_VALORES[j][0];
                                MATRIZ_VALORES[i][1]=MATRIZ_VALORES[j][1];
                                MATRIZ_VALORES[i][2]=MATRIZ_VALORES[j][2];

                                MATRIZ_VALORES[j][0]=tmp1;
                                MATRIZ_VALORES[j][1]=tmp2;
                                MATRIZ_VALORES[j][2]=tmp3;
                            }

                        }

                    }

                    /////

/*                    imprimirln("        mostando matriz de valores      ");

                    for(int i=0; i<20; i++){
                        imprimirNumln(MATRIZ_VALORES[i][0]);
                        imprimirNumln(MATRIZ_VALORES[i][1]);
                        imprimirNumln(MATRIZ_VALORES[i][2]);
                    }*/

                    //CON ESTE FOR OBTENEMOS LOS ESPACIOS DISPONIBLES EN LA PARTICION
                    for(int i=0; (i<20) && (MATRIZ_VALORES[i][2]!=0); i++){

                        if(MATRIZ_VALORES[i+1][2]!=0){


                            if((MATRIZ_VALORES[i+1][0]-MATRIZ_VALORES[i][2])>3){

                                MATRIZ_ESPACIOS[i][0]=MATRIZ_VALORES[i][2]+1;
                                MATRIZ_ESPACIOS[i][1]=MATRIZ_VALORES[i+1][0]-MATRIZ_VALORES[i][2]-2;
                                MATRIZ_ESPACIOS[i][2]=MATRIZ_VALORES[i+1][0]-1;

                            }

                        }

                    }

                    int PARTICION_FINAL_SIZE=0;
                    int PARTICION_FINAL_TAMANIO=0;
                    int PARTICION_FINAL_TOTAL=0;//CON ESTE FOR OBTENEMOS EL ULTIMO DATO DE LA TABLA DE VALORES QUE PERTENECE A LA ULTIMA PARTICION
                    for(int i=0; (i<20) && (MATRIZ_VALORES[i][2]!=0); i++){
                        PARTICION_FINAL_SIZE=0;
                        PARTICION_FINAL_TAMANIO=0;
                        PARTICION_FINAL_TOTAL=0;
                        PARTICION_FINAL_SIZE=MATRIZ_VALORES[i][0];
                        PARTICION_FINAL_TAMANIO=MATRIZ_VALORES[i][1];
                        PARTICION_FINAL_TOTAL=MATRIZ_VALORES[i][2];
                    }

                    bool DATO_AGREGADO=false;//CON ESTO COMPLETAMOS NUESTRA TABLA DE ESPACIOS EN BLANCO PARA PODER APLICAR AJUSTES A LA PARTICION EXTENDIDA
                    for(int i=0;(i<20) && (DATO_AGREGADO==false); i++){

                        if(MATRIZ_ESPACIOS[i][2]==0){
                            DATO_AGREGADO=true;
                            MATRIZ_ESPACIOS[i][0]=PARTICION_FINAL_TOTAL+1;
                            MATRIZ_ESPACIOS[i][1]=(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_size+disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_start)-PARTICION_FINAL_TOTAL-2;
                            MATRIZ_ESPACIOS[i][2]=(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_size+disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_start)-1;
                        }

                    }



                    imprimirln("        mostrando matriz de espacios    ");
                    for(int i=0; i<20; i++){
                        imprimirNumln(MATRIZ_ESPACIOS[i][0]);
                        imprimirNumln(MATRIZ_ESPACIOS[i][1]);
                        imprimirNumln(MATRIZ_ESPACIOS[i][2]);
                    }

                    int INDICE_ESPADIO_DISPONIBLE=0;
                    int VALOR_PIVOTE_ESPACIODISPONIBLE=0;
                    int INDICE_ESPACIODISPONIBLE=0;
                    int PIVOTE_VALORESPACIO=0;
                    bool TAMANIO_DISPONIBLE_PARTICION=false;//CON ESTO VALIDAMOS QUE UNO DE LOS ESPACIOS DISPONIBLES SEA GRANDE PARA LA PARTICION
                    for(int i=0; (i<20) && (TAMANIO_DISPONIBLE_PARTICION==false); i++){
                        if(MATRIZ_ESPACIOS[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit)){
                            TAMANIO_DISPONIBLE_PARTICION=true;
                            INDICE_ESPADIO_DISPONIBLE=i;
                            VALOR_PIVOTE_ESPACIODISPONIBLE=MATRIZ_ESPACIOS[i][1];
                            INDICE_ESPACIODISPONIBLE=i;
                            PIVOTE_VALORESPACIO=MATRIZ_ESPACIOS[i][1];
                        }
                    }

                    if(TAMANIO_DISPONIBLE_PARTICION==true){
                        //imprimirln("relaizar aqui ajustes");

                        if(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_fit==98){//MEJOR AJUSTE
           //                 imprimirln("realizarar mejor ajuste logica");
                           /* int INDICE_ESPADIO_DISPONIBLE=0;
                            int VALOR_PIVOTE_ESPACIODISPONIBLE=MATRIZ_ESPACIOS[0][1];*/

                            for(int i=0; i<20; i++){

                                if((VALOR_PIVOTE_ESPACIODISPONIBLE>MATRIZ_ESPACIOS[i][1]) && (MATRIZ_ESPACIOS[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit))){
                                    INDICE_ESPADIO_DISPONIBLE=0;
                                    INDICE_ESPADIO_DISPONIBLE=i;
                                    VALOR_PIVOTE_ESPACIODISPONIBLE=MATRIZ_ESPACIOS[i][1];
                                }
                            }

/////
                                bool PARTITION_CREADA=false;

                                for(int k=0; (k<20) && (PARTITION_CREADA==false); k++){

                                    if(disco.mbr_partitionEXT[k].part_status!=49){
                                        PARTITION_CREADA=true;
///////////////////////////////
                                        string status="1";
                                        disco.mbr_partitionEXT[k].part_status=status[0];
                                        disco.mbr_partitionEXT[k].part_fit=fdisk._fit[0];
                                        disco.mbr_partitionEXT[k].part_start=MATRIZ_ESPACIOS[INDICE_ESPADIO_DISPONIBLE][0];
                                        disco.mbr_partitionEXT[k].part_next=-1;
                                        disco.mbr_partitionEXT[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                        //disco.mbr_partitionEXT[k].part_next=-1;
                                       /* for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                            disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                        }*/

                                        ////
                                        for(int p=0; p<16; p++){
                                            if(p<fdisk._name.size()){
                                                disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                            }else{
                                                disco.mbr_partitionEXT[k].part_name[p]=00;
                                            }
                                        }
                                        /////

                                      /* if(k!=3){//CON ESTAS INSTRUCCIONES BUSCAMOS LA SIGUIENTE PARTICION LOGICA
                                            int valorseparado=k+1;
                                            bool siguiente_encontrada=false;
                                            for(valorseparado; (valorseparado<4) && (siguiente_encontrada==false); valorseparado++){//BUSCARA LA SIGUIENTE PARTICION QUE ESTE ACTIVA
                                                if(disco.mbr_partitionEXT[valorseparado].part_status==49){
                                                    siguiente_encontrada=true;
                                                    disco.mbr_partitionEXT[k].part_next=disco.mbr_partitionEXT[valorseparado].part_start;
                                                }
                                            }

                                            if(siguiente_encontrada==false){//SI EL VALOR SIGUE EN FALSE, SIGNIFICA QUE NO EXISTE NINGUNA OTRA PARTICION SIGUIENTE
                                                disco.mbr_partitionEXT[k].part_next=-1;
                                            }

                                        }else{
                                            disco.mbr_partitionEXT[k].part_next=-1;
                                        }*/
///////////////////////////////////

                                        //ACTUALIZAR DISCO CON SU CORRESPONDIENTE VALOR A NEXT DE LA PARTICION LOGICA
           /*                             for(int t=0; t<3; t++){
                                            if(disco.mbr_partitionEXT[t].part_status==49){
                                                int SEGUNDO_ELEMENTO=t+1;
                                                bool ACTUALIZADO=false;
                                                for(SEGUNDO_ELEMENTO; (SEGUNDO_ELEMENTO<4) && (ACTUALIZADO==false); SEGUNDO_ELEMENTO++){
                                                    if(disco.mbr_partitionEXT[SEGUNDO_ELEMENTO].part_status==49){
                                                        ACTUALIZADO=true;
                                                        disco.mbr_partitionEXT[t].part_next=0;
                                                        disco.mbr_partitionEXT[t].part_next=disco.mbr_partitionEXT[SEGUNDO_ELEMENTO].part_start;
                                                    }
                                                }
                                            }
                                        }*/

///////////////////////////////////////////////////////////////

                            int MATRIZVALORES2[20][3];

                            //FOR QUE INICIALIZA LA MATRIZ DE VALORES A CERO
                            for(int j=0; j<20; j++){
                                MATRIZVALORES2[j][0]=0;
                                MATRIZVALORES2[j][1]=0;
                                MATRIZVALORES2[j][2]=0;
                            }

                            //FOR QUE LE ASIGNA LOS VALORES A LA MATRIZ DE VALORES
                            int CONTADORINDICEVALORES2=0;
                            for(int v=0; v<20; v++){
                                if(disco.mbr_partitionEXT[v].part_status==49){//TOMA LOS VALORES DE LAS PARTICIONES LOGICAS QUE ESTE ACTIVA
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][0]=disco.mbr_partitionEXT[v].part_start;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][1]=disco.mbr_partitionEXT[v].part_size;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][2]=disco.mbr_partitionEXT[v].part_start+disco.mbr_partitionEXT[v].part_size;
                                    CONTADORINDICEVALORES2++;
                                }
                            }
                                //CON ESTE FOR ORDENAMOS LOS DATOS
                               for(int z=0; (z<20) && (MATRIZVALORES2[z][2]!=0); z++){

                                    for(int x=0; (x<20) && (MATRIZVALORES2[x][2]!=0); x++){

                                        if((MATRIZVALORES2[z][2])<(MATRIZVALORES2[x][2])){

                                            int tmp1=MATRIZVALORES2[z][0];
                                            int tmp2=MATRIZVALORES2[z][1];
                                            int tmp3=MATRIZVALORES2[z][2];
                                            //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                                            MATRIZVALORES2[z][0]=MATRIZVALORES2[x][0];
                                            MATRIZVALORES2[z][1]=MATRIZVALORES2[x][1];
                                            MATRIZVALORES2[z][2]=MATRIZVALORES2[x][2];

                                            MATRIZVALORES2[x][0]=tmp1;
                                            MATRIZVALORES2[x][1]=tmp2;
                                            MATRIZVALORES2[x][2]=tmp3;
                                        }

                                    }

                                }


                                for(int f=0; f<20; f++){
                                    if(MATRIZVALORES2[f][0]!=0){
                                        if((MATRIZVALORES2[f+1][0]!=0) && (f+1<20)){
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        encontrado=true;
                                                        disco.mbr_partitionEXT[n].part_next=MATRIZVALORES2[f+1][0];
                                                    }
                                                }
                                            }
                                        }else{
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        disco.mbr_partitionEXT[n].part_next=-1;
                                                        encontrado=true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

////////////////////////////////////////////////////////////////////

/////////////////////////////////////////


                                                    //imprimirln("permitido el acceso");
                                        GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                        GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                        imprimirln("+++++ PARTICION LOGICA CREADA EXITOSAMENTE (M.A:L) ++++");


///////////////////////////////////////
                                    }

                                }

///////
                        }else

                        if(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_fit==102){//PRIMER AJUSTE
                       //     imprimirln("realizarar primer ajuste logica");
                            int INDICE_ESPACIO_OCUPAR=0;
                            bool ESPACIO_ENCONTRADO=false;
                            for(int i=0; (i<20) && (ESPACIO_ENCONTRADO==false); i++){//SE OBTIENE EL PRIMER ESPACIO MAS GRANDE PARA LA PARTICION
                                if(MATRIZ_ESPACIOS[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size, fdisk._unit)){
                                    INDICE_ESPACIO_OCUPAR=0;
                                    INDICE_ESPACIO_OCUPAR=i;
                                    ESPACIO_ENCONTRADO=true;
                                }
                            }

                            bool PARTICION_CREADA=false;
                            for(int k=0; (k<20) && (PARTICION_CREADA==false); k++){

                                if(disco.mbr_partitionEXT[k].part_status!=49){
                                    PARTICION_CREADA=true;
/////////
                                        string status="1";
                                        disco.mbr_partitionEXT[k].part_status=status[0];
                                        disco.mbr_partitionEXT[k].part_fit=fdisk._fit[0];
                                        disco.mbr_partitionEXT[k].part_start=MATRIZ_ESPACIOS[INDICE_ESPACIO_OCUPAR][0];
                                        disco.mbr_partitionEXT[k].part_next=-1;
                                        disco.mbr_partitionEXT[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                        //disco.mbr_partitionEXT[k].part_next=-1;
                                        /*for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                            disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                        }*/

                                        ////
                                       for(int p=0; p<16; p++){
                                            if(p<fdisk._name.size()){
                                                disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                            }else{
                                                disco.mbr_partitionEXT[k].part_name[p]=00;
                                            }
                                        }
                                        ////
/*
                                        if(k!=3){//CON ESTAS INSTRUCCIONES BUSCAMOS LA SIGUIENTE PARTICION LOGICA
                                        imprimirln("--valor k--");
                                        imprimirNumln(k);
                                            int valorseparado=k+1;
                                            bool siguiente_encontrada=false;
                                            for(valorseparado; (valorseparado<4) && (siguiente_encontrada==false); valorseparado++){//BUSCARA LA SIGUIENTE PARTICION QUE ESTE ACTIVA

                                                imprimirln("--valor separado--");
                                                imprimirNumln(valorseparado);

                                                if(disco.mbr_partitionEXT[valorseparado].part_status==49){
                                                    imprimirln("-- valor validado ---");
                                                    imprimirNumln(k);
                                                    imprimirNumln(valorseparado);
                                                    cout<<disco.mbr_partitionEXT[valorseparado].part_status<<endl;
                                                    cout<<disco.mbr_partitionEXT[valorseparado].part_name<<endl;
                                                    siguiente_encontrada=true;
                                                    imprimirln("entro a spe1");
                                                    disco.mbr_partitionEXT[k].part_next=disco.mbr_partitionEXT[valorseparado].part_start;
                                                }
                                            }

                                            if(siguiente_encontrada==false){//SI EL VALOR SIGUE EN FALSE, SIGNIFICA QUE NO EXISTE NINGUNA OTRA PARTICION SIGUIENTE
                                                disco.mbr_partitionEXT[k].part_next=-1;
                                                imprimirln("entro a spe2");
                                            }

                                        }else{
                                            disco.mbr_partitionEXT[k].part_next=-1;
                                        }*/

                                        //ACTUALIZAR DISCO CON SU CORRESPONDIENTE VALOR A NEXT DE LA PARTICION LOGICA
                                    /*    for(int t=0; t<3; t++){
                                            if(disco.mbr_partitionEXT[t].part_status==49){
                                                int SEGUNDO_ELEMENTO=t+1;
                                                bool ACTUALIZADO=false;
                                                for(SEGUNDO_ELEMENTO; (SEGUNDO_ELEMENTO<4) && (ACTUALIZADO==false); SEGUNDO_ELEMENTO++){
                                                    if(disco.mbr_partitionEXT[SEGUNDO_ELEMENTO].part_status==49){
                                                        ACTUALIZADO=true;
                                                        disco.mbr_partitionEXT[t].part_next=0;
                                                        disco.mbr_partitionEXT[t].part_next=disco.mbr_partitionEXT[SEGUNDO_ELEMENTO].part_start;
                                                    }
                                                }
                                            }
                                        }*/
///////////////////////////////////////////////////////////////////////////////

                            int MATRIZVALORES2[20][3];

                            //FOR QUE INICIALIZA LA MATRIZ DE VALORES A CERO
                            for(int j=0; j<20; j++){
                                MATRIZVALORES2[j][0]=0;
                                MATRIZVALORES2[j][1]=0;
                                MATRIZVALORES2[j][2]=0;
                            }

                            //FOR QUE LE ASIGNA LOS VALORES A LA MATRIZ DE VALORES
                            int CONTADORINDICEVALORES2=0;
                            for(int v=0; v<20; v++){
                                if(disco.mbr_partitionEXT[v].part_status==49){//TOMA LOS VALORES DE LAS PARTICIONES LOGICAS QUE ESTE ACTIVA
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][0]=disco.mbr_partitionEXT[v].part_start;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][1]=disco.mbr_partitionEXT[v].part_size;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][2]=disco.mbr_partitionEXT[v].part_start+disco.mbr_partitionEXT[v].part_size;
                                    CONTADORINDICEVALORES2++;
                                }
                            }
                                //CON ESTE FOR ORDENAMOS LOS DATOS
                               for(int z=0; (z<20) && (MATRIZVALORES2[z][2]!=0); z++){

                                    for(int x=0; (x<20) && (MATRIZVALORES2[x][2]!=0); x++){

                                        if((MATRIZVALORES2[z][2])<(MATRIZVALORES2[x][2])){

                                            int tmp1=MATRIZVALORES2[z][0];
                                            int tmp2=MATRIZVALORES2[z][1];
                                            int tmp3=MATRIZVALORES2[z][2];
                                            //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                                            MATRIZVALORES2[z][0]=MATRIZVALORES2[x][0];
                                            MATRIZVALORES2[z][1]=MATRIZVALORES2[x][1];
                                            MATRIZVALORES2[z][2]=MATRIZVALORES2[x][2];

                                            MATRIZVALORES2[x][0]=tmp1;
                                            MATRIZVALORES2[x][1]=tmp2;
                                            MATRIZVALORES2[x][2]=tmp3;
                                        }

                                    }

                                }


                                for(int f=0; f<20; f++){
                                    if(MATRIZVALORES2[f][0]!=0){
                                        if((MATRIZVALORES2[f+1][0]!=0) && (f+1<20)){
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        encontrado=true;
                                                        disco.mbr_partitionEXT[n].part_next=MATRIZVALORES2[f+1][0];
                                                    }
                                                }
                                            }
                                        }else{
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        disco.mbr_partitionEXT[n].part_next=-1;
                                                        encontrado=true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

//////////////////////////////////////////////////////////////////////////////////

                                                    //imprimirln("permitido el acceso");
                                        GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                        GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                        imprimirln("+++++ PARTICION LOGICA CREADA EXITOSAMENTE (PR.A:L) ++++");

/////////////////////////
                                }
                            }


                        }else

                        if(disco.mbr_partition[INDICE_PARTICION_EXTENDIDA].part_fit==119){//PEOR AJUSTE
                      //      imprimirln("realizarar peor ajuste logica");
                            /*int INDICE_ESPACIODISPONIBLE=0;
                            int PIVOTE_VALORESPACIO=MATRIZ_ESPACIOS[0][1];*/

                            for(int i=0; i<20; i++){

                                if((PIVOTE_VALORESPACIO<MATRIZ_ESPACIOS[i][1]) && (MATRIZ_ESPACIOS[i][1]>OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit))){
                                    INDICE_ESPACIODISPONIBLE=0;
                                    INDICE_ESPACIODISPONIBLE=i;
                                    PIVOTE_VALORESPACIO=MATRIZ_ESPACIOS[i][1];
                                }
                            }

                            bool PARTICION_CREADA=false;
                            for(int k=0; (k<20) && (PARTICION_CREADA==false); k++){

                                if(disco.mbr_partitionEXT[k].part_status!=49){
                                    PARTICION_CREADA=true;
///////////////////////////////////////////////////////////////////
                                        string status="1";
                                        disco.mbr_partitionEXT[k].part_status=status[0];
                                        disco.mbr_partitionEXT[k].part_fit=fdisk._fit[0];
                                        disco.mbr_partitionEXT[k].part_start=MATRIZ_ESPACIOS[INDICE_ESPACIODISPONIBLE][0];
                                        disco.mbr_partitionEXT[k].part_next=-1;
                                        disco.mbr_partitionEXT[k].part_size=OBTENER_TAMANIO_PARTITION(fdisk._size,fdisk._unit);
                                        //disco.mbr_partitionEXT[k].part_next=-1;
                                     /*   for(int p=0; (p<16) && (p<fdisk._name.size()); p++){    //CON ESTE FOR LE COLOCAMOS SU NOMBRE A LA PARTICION
                                            disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                        }*/


                                        ////
                                       for(int p=0; p<16; p++){
                                            if(p<fdisk._name.size()){
                                                disco.mbr_partitionEXT[k].part_name[p]=fdisk._name[p];
                                            }else{
                                                disco.mbr_partitionEXT[k].part_name[p]=00;
                                            }
                                        }

                                        ////

                                        //ACTUALIZAR DISCO CON SU CORRESPONDIENTE VALOR A NEXT DE LA PARTICION LOGICA
/*                                        for(int t=0; t<3; t++){
                                            if(disco.mbr_partitionEXT[t].part_status==49){
                                                int SEGUNDO_ELEMENTO=t+1;
                                                bool ACTUALIZADO=false;
                                                for(SEGUNDO_ELEMENTO; (SEGUNDO_ELEMENTO<4) && (ACTUALIZADO==false); SEGUNDO_ELEMENTO++){
                                                    if(disco.mbr_partitionEXT[SEGUNDO_ELEMENTO].part_status==49){
                                                        ACTUALIZADO=true;
                                                        disco.mbr_partitionEXT[t].part_next=0;
                                                        disco.mbr_partitionEXT[t].part_next=disco.mbr_partitionEXT[SEGUNDO_ELEMENTO].part_start;
                                                    }
                                                }
                                            }
                                        }*/
///////////////////////////////////////////////////////////////////////////
                            int MATRIZVALORES2[20][3];

                            //FOR QUE INICIALIZA LA MATRIZ DE VALORES A CERO
                            for(int j=0; j<20; j++){
                                MATRIZVALORES2[j][0]=0;
                                MATRIZVALORES2[j][1]=0;
                                MATRIZVALORES2[j][2]=0;
                            }

                            //FOR QUE LE ASIGNA LOS VALORES A LA MATRIZ DE VALORES
                            int CONTADORINDICEVALORES2=0;
                            for(int v=0; v<20; v++){
                                if(disco.mbr_partitionEXT[v].part_status==49){//TOMA LOS VALORES DE LAS PARTICIONES LOGICAS QUE ESTE ACTIVA
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][0]=disco.mbr_partitionEXT[v].part_start;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][1]=disco.mbr_partitionEXT[v].part_size;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][2]=disco.mbr_partitionEXT[v].part_start+disco.mbr_partitionEXT[v].part_size;
                                    CONTADORINDICEVALORES2++;
                                }
                            }
                                //CON ESTE FOR ORDENAMOS LOS DATOS
                               for(int z=0; (z<20) && (MATRIZVALORES2[z][2]!=0); z++){

                                    for(int x=0; (x<20) && (MATRIZVALORES2[x][2]!=0); x++){

                                        if((MATRIZVALORES2[z][2])<(MATRIZVALORES2[x][2])){

                                            int tmp1=MATRIZVALORES2[z][0];
                                            int tmp2=MATRIZVALORES2[z][1];
                                            int tmp3=MATRIZVALORES2[z][2];
                                            //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                                            MATRIZVALORES2[z][0]=MATRIZVALORES2[x][0];
                                            MATRIZVALORES2[z][1]=MATRIZVALORES2[x][1];
                                            MATRIZVALORES2[z][2]=MATRIZVALORES2[x][2];

                                            MATRIZVALORES2[x][0]=tmp1;
                                            MATRIZVALORES2[x][1]=tmp2;
                                            MATRIZVALORES2[x][2]=tmp3;
                                        }

                                    }

                                }


                                for(int f=0; f<20; f++){
                                    if(MATRIZVALORES2[f][0]!=0){
                                        if((MATRIZVALORES2[f+1][0]!=0) && (f+1<20)){
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        encontrado=true;
                                                        disco.mbr_partitionEXT[n].part_next=MATRIZVALORES2[f+1][0];
                                                    }
                                                }
                                            }
                                        }else{
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        disco.mbr_partitionEXT[n].part_next=-1;
                                                        encontrado=true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


//////////////////////////////////////////////////////////////////////////////

                                                    //imprimirln("permitido el acceso");
                                        GUARDAR_MBR_EN_DISCO(fdisk._path,disco);//  ACTUALIZA LA ESTRUCTURA MBR EN EL ARCHIVO DISCO
                                        GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(fdisk._path),disco);//ACTUALIZA EL ARCHIVO RAID1 DEL DISCO
                                        imprimirln("+++++ PARTICION LOGICA CREADA EXITOSAMENTE (P.A:L) ++++");

///////////////////////////////////////////////////////////////////
                                }
                            }


                        }

                    }else{
                        imprimirln("=== ERROR: LA PARTICION EXTENDIDA NO CUENTA SUFICIENTE ESPACIO PARA LA NUEVA PARTICION LOGICA ====");
                    }

                 //   imprimirln("AGREGANDO MAS PARTICIONES");

//////////////////////////////////////////////////////////////////////////////////////////

            }

        }else{
            imprimirln("=== ERROR: EL TAMANIO PARA LA PARTICION LOGICA ES MAYOR A LA PARTICION EXTENDIDA ====");
        }

    }else{
        imprimirln("=== ERROR: NO SE PUDO OBTENER DE FORMA EXITOSA LA PARTICION EXTENDIDA ===");
    }

//    MOSTRAR_INFORMACION_DISCO(disco);

}

//*******************************************************

void EJECUCION_FDISK_DELETE(struct cmdFDISK disk){
  //  imprimirln("si entra a fdisk delete");
    if(EXISTE_DISCO_O_ARCHIVO(disk._path)){
        struct MBRD_DISK disco;
        disco=OBTENER_MBR_DEL_DISCO(disk._path);

        if((EXISTE_PARTICION_EN_DISCO(disco,disk._name)==false) || (EXISTE_PARTICION_EN_EXTENDIDA(disco,disk._name)==false)){
           // imprimirln("********************validado******************");
            bool ESTA_EN_PATICION_PYE=EXISTE_PARTICION_EN_DISCO(disco,disk._name);

          /*  if(disk._delete=="full"){
                imprimirln("eliminar full");

                if(ESTA_EN_PATICION_PYE==false){//SI ESTO ES FALSE SIGNIFICA QUE SI ES UNA PARTICION PRIMARIA O EXTENDIDA
                    int INDICE_PARTICION=OBTENER_INDICE_PARTICION_EXTENDIDAYPRIMARA(disco,disk._name);
                    imprimirln("primaria o EXTENDIDA");
                    imprimirNumln(INDICE_PARTICION);


                }else{//EN CASO CONTRARIO SIGNIFICA QUE ES UNA PARTICION LOGICA
                    int INDICE_PARTICION=OBTENER_INDICE_PARTICION_LOGICA(disco,disk._name);
                    imprimirln("logica");
                    imprimirNumln(INDICE_PARTICION);


                }


            }else*/
   //         if(disk._delete=="fast"){
      //          imprimirln("eliminar fast");
//////////////////////////////////////////////////////

                if(ESTA_EN_PATICION_PYE==false){//SI ESTO ES FALSE SIGNIFICA QUE SI ES UNA PARTICION PRIMARIA O EXTENDIDA
                    int INDICE_PARTICION=OBTENER_INDICE_PARTICION_EXTENDIDAYPRIMARA(disco,disk._name);
                    //imprimirNumln(INDICE_PARTICION);

                    if(strcmp(disco.mbr_partition[INDICE_PARTICION].part_name,disk._name.c_str())==0){//PARA RECONFIRMAR QUE ESTA SEA LA PARTICION

                        if(disco.mbr_partition[INDICE_PARTICION].part_type==112){//VERIFICA SI LA PARTICION ES UNA PARTICION PRIMARIA

                            char respuesta[100];
                            imprimirln("+++++ ¿SEGURO QUE DESEA ELIMINAR ESTA PARTICION (S)? ++++");
                            cin.getline(respuesta,100);

                            if(strcmp(respuesta,"s")==0){

                                if(disk._delete=="full"){
                                    ELIMINACION_FULL(disco.mbr_partition[INDICE_PARTICION].part_start,disco.mbr_partition[INDICE_PARTICION].part_size,disk._path);
                                    ELIMINACION_FULL(disco.mbr_partition[INDICE_PARTICION].part_start,disco.mbr_partition[INDICE_PARTICION].part_size,OBTENER_PATH_ARCHIVORAID(disk._path));
                                }
                            string status="0";
                            disco.mbr_partition[INDICE_PARTICION].part_status=status[0];
                            disco.mbr_partition[INDICE_PARTICION].part_start=0;
                            disco.mbr_partition[INDICE_PARTICION].part_size=0;
                            disco.mbr_partition[INDICE_PARTICION].part_fit=NULL;
                            disco.mbr_partition[INDICE_PARTICION].part_type=NULL;
                            for(int p=0; (p<16) ; p++){
                                disco.mbr_partition[INDICE_PARTICION].part_name[p]=NULL;
                            }

                            ///////     PARA ELIMINAR SI ESTA MONTADA LA PARTICION

                            if(EXISTE_PARTICION_MONTADA(disk._name)){
                                MONTAR_PARTICION=ELIMINAR_MONTAJE_ELIMINACION_PARTICION(disk._name);
                            }

                           ///////////////////////////////////


                            //cout<<disco.mbr_partition[INDICE_PARTICION].part_name<<endl;
                            GUARDAR_MBR_EN_DISCO(disk._path,disco);
                            GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(disk._path),disco);

                                imprimirln("++++ PARTICION ELIMINADA EXITOSAMENTE +++");
                            }else{
                               imprimirln("++++ ELIMINACION DE LA PARTICION CANCELADA +++");
                            }

                        }else{//DADO OTRO CASO ES UNA PARTICION EXTENDIDA
                           // imprimirln("extendida");

                            if(disco.mbr_partition[INDICE_PARTICION].part_type==101){
                                char respuesta[100];
                                imprimirln("+++++ ¿ESTA SEGURO QUE DESEA ELIMINAR ESTA PARTICION (S)? ++++++");
                                cin.getline(respuesta,100);

                                if(strcmp(respuesta,"s")==0){
                              //      imprimirln("eliminar particion extendidsa");

                                    if(disk._delete=="full"){
                                        ELIMINACION_FULL(disco.mbr_partition[INDICE_PARTICION].part_start,disco.mbr_partition[INDICE_PARTICION].part_size,disk._path);
                                        ELIMINACION_FULL(disco.mbr_partition[INDICE_PARTICION].part_start,disco.mbr_partition[INDICE_PARTICION].part_size,OBTENER_PATH_ARCHIVORAID(disk._path));
                                    }

                                    string status="0";
                                    disco.mbr_partition[INDICE_PARTICION].part_status=status[0];
                                    disco.mbr_partition[INDICE_PARTICION].part_size=0;
                                    disco.mbr_partition[INDICE_PARTICION].part_start=0;
                                    disco.mbr_partition[INDICE_PARTICION].part_type=NULL;
                                    disco.mbr_partition[INDICE_PARTICION].part_fit=NULL;
                                    for(int p=0; p<16; p++){
                                    disco.mbr_partition[INDICE_PARTICION].part_name[p]=NULL;
                                    }

                                    for(int l=0; l<20 ; l++){//CON ESTE FOR ELIMINAMOS TODAS LAS PARTICIONES LOGICAS YA QUE ESTAMOS ELIMINANDO LA PARTICION EXTENDIDA
                                        if(disco.mbr_partitionEXT[l].part_status==49){
                                            disco.mbr_partitionEXT[l].part_status=status[0];
                                            disco.mbr_partitionEXT[l].part_next=0;
                                            disco.mbr_partitionEXT[l].part_size=0;
                                            disco.mbr_partitionEXT[l].part_start=0;
                                            disco.mbr_partitionEXT[l].part_fit=NULL;
                                            for(int p=0; p<16; p++){
                                                disco.mbr_partitionEXT[l].part_name[p]=NULL;
                                            }
                                        }
                                    }

                                    ///////     PARA ELIMINAR SI ESTA MONTADA LA PARTICION

                                    if(EXISTE_PARTICION_MONTADA(disk._name)){
                                        MONTAR_PARTICION=ELIMINAR_MONTAJE_ELIMINACION_PARTICION(disk._name);
                                    }

                                    ///////////////////////////////////



                                    GUARDAR_MBR_EN_DISCO(disk._path,disco);
                                    GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(disk._path),disco);

                                    imprimirln("++++ PARTICION ELIMINADA EXITOSAMENTE ++++");

                                }else{
                                    imprimirln("+++++ ELIMINACION DE LA PARTICION CANCELADA +++++");
                                }


                            }
                        }


                    }else{
                        imprimirln("=== ERROR: NO SE PUDO ACCEDER A LA PARTICION PARA ELIMINARLA");
                    }


                }else{//EN CASO CONTRARIO SIGNIFICA QUE ES UNA PARTICION LOGICA
                    int INDICE_PARTICION=OBTENER_INDICE_PARTICION_LOGICA(disco,disk._name);
                    imprimirNumln(INDICE_PARTICION);
                    //imprimirln("logica");
                    if(strcmp(disco.mbr_partitionEXT[INDICE_PARTICION].part_name,disk._name.c_str())==0){//CON ESTO VALIDAMOS QUE SI SEA LA PARTICION LOGICA A ELIMINAR
                        char respuesta[100];
                        imprimirln("+++++ ¿ESTA SEGURO DE ELIMINAR LA PARTICION LOGICA (S)? +++++");
                        cin.getline(respuesta,100);

                        if(strcmp(respuesta,"s")==0){
                            string status="0";

                            if(disk._delete=="full"){
                                ELIMINACION_FULL(disco.mbr_partitionEXT[INDICE_PARTICION].part_start,disco.mbr_partitionEXT[INDICE_PARTICION].part_size,disk._path);
                                ELIMINACION_FULL(disco.mbr_partitionEXT[INDICE_PARTICION].part_start,disco.mbr_partitionEXT[INDICE_PARTICION].part_size,OBTENER_PATH_ARCHIVORAID(disk._path));
                            }


                            disco.mbr_partitionEXT[INDICE_PARTICION].part_status=status[0];
                            disco.mbr_partitionEXT[INDICE_PARTICION].part_fit=NULL;
                            disco.mbr_partitionEXT[INDICE_PARTICION].part_next=0;
                            disco.mbr_partitionEXT[INDICE_PARTICION].part_size=0;
                            disco.mbr_partitionEXT[INDICE_PARTICION].part_start=0;
                            for(int p=0; p<20; p++){
                                disco.mbr_partitionEXT[INDICE_PARTICION].part_name[p]=NULL;
                            }

                            int MATRIZVALORES2[20][3];

                            //FOR QUE INICIALIZA LA MATRIZ DE VALORES A CERO
                            for(int j=0; j<20; j++){
                                MATRIZVALORES2[j][0]=0;
                                MATRIZVALORES2[j][1]=0;
                                MATRIZVALORES2[j][2]=0;
                            }

                            //FOR QUE LE ASIGNA LOS VALORES A LA MATRIZ DE VALORES
                            int CONTADORINDICEVALORES2=0;
                            for(int v=0; v<20; v++){
                                if(disco.mbr_partitionEXT[v].part_status==49){//TOMA LOS VALORES DE LAS PARTICIONES LOGICAS QUE ESTE ACTIVA
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][0]=disco.mbr_partitionEXT[v].part_start;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][1]=disco.mbr_partitionEXT[v].part_size;
                                    MATRIZVALORES2[CONTADORINDICEVALORES2][2]=disco.mbr_partitionEXT[v].part_start+disco.mbr_partitionEXT[v].part_size;
                                    CONTADORINDICEVALORES2++;
                                }
                            }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                //CON ESTE FOR ORDENAMOS LOS DATOS
                               for(int z=0; (z<20) && (MATRIZVALORES2[z][2]!=0); z++){

                                    for(int x=0; (x<20) && (MATRIZVALORES2[x][2]!=0); x++){

                                        if((MATRIZVALORES2[z][2])<(MATRIZVALORES2[x][2])){

                                            int tmp1=MATRIZVALORES2[z][0];
                                            int tmp2=MATRIZVALORES2[z][1];
                                            int tmp3=MATRIZVALORES2[z][2];
                                            //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                                            MATRIZVALORES2[z][0]=MATRIZVALORES2[x][0];
                                            MATRIZVALORES2[z][1]=MATRIZVALORES2[x][1];
                                            MATRIZVALORES2[z][2]=MATRIZVALORES2[x][2];

                                            MATRIZVALORES2[x][0]=tmp1;
                                            MATRIZVALORES2[x][1]=tmp2;
                                            MATRIZVALORES2[x][2]=tmp3;
                                        }

                                    }

                                }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                                for(int f=0; f<20; f++){
                                    if(MATRIZVALORES2[f][0]!=0){
                                        if(MATRIZVALORES2[f+1][0]!=0){
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        encontrado=true;
                                                        disco.mbr_partitionEXT[n].part_next=MATRIZVALORES2[f+1][0];
                                                    }
                                                }
                                            }
                                        }else{
                                            bool encontrado=false;
                                            for(int n=0; (n<20) && (encontrado==false); n++){
                                                if(disco.mbr_partitionEXT[n].part_status==49){
                                                    if(MATRIZVALORES2[f][0]==disco.mbr_partitionEXT[n].part_start){
                                                        disco.mbr_partitionEXT[n].part_next=-1;
                                                        encontrado=true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                    ///////     PARA ELIMINAR SI ESTA MONTADA LA PARTICION

                                    if(EXISTE_PARTICION_MONTADA(disk._name)){
                                        MONTAR_PARTICION=ELIMINAR_MONTAJE_ELIMINACION_PARTICION(disk._name);
                                    }

                                    ///////////////////////////////////

                                GUARDAR_MBR_EN_DISCO(disk._path,disco);
                                GUARDAR_MBR_EN_DISCO(OBTENER_PATH_ARCHIVORAID(disk._path),disco);
                                imprimirln("=== PARTICION ELIMINADA EXITOSAMENTE ===");

                        }else{
                            imprimirln("++++ CANCELACION DE ELIMINACION ++++");
                        }

                    }else{
                        imprimirln("=== ERROR: NO SE PUDO ACCEDER A LA PARTICION LOGICA PARA ELIMINAR ====");
                    }

                }


//////////////////////////////////////////////////////

  //          }                 LLAVE DEL IF FULL


        }else{
            imprimirln("==== ERROR LA PARTICION INDICADA NO EXISTE ====");
        }

 //       MOSTRAR_INFORMACION_DISCO(disco);
    }else{
        imprimirln("=== ERROR: NO SE PUEDE ELIMINAR PARTICION YA QUE NO SE ENCONTRO EL ARCHIVO EN LA UBICACION INDICADA===");
    }


}

void EJECUCION_MBR_REP(struct cmdREP rep){
   // imprimirln("entro a mbr");
   struct REGISTRO DATOS_PARTICION_MONTADA;//STRUCT PARA OBTENER LOS DATOS DE LA PARTICION MONTADA
   DATOS_PARTICION_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);

   struct MBRD_DISK DISCO_REPORTE;//STRUCT PARA OBTENER LOS DATOS DEL DISCO
   DISCO_REPORTE=OBTENER_MBR_DEL_DISCO(DATOS_PARTICION_MONTADA._path);

   char DIRECTORIOS_CARPETAS[rep._path.size()+1];
   strcpy(DIRECTORIOS_CARPETAS,rep._path.c_str());
   vector <string> CARPETAS_A_CREAR=split(DIRECTORIOS_CARPETAS,"/");
   imprimirln("++++ CREANDO CARPETAS ++++");
   crearCarpetas(CARPETAS_A_CREAR);//ESTE METODO CREA LAS CARPETAS EN CASO DE QUE NO EXISTAN, SI EXISTEN NO HACE NADA

   ////************************************** CODIGO PARA EL REPORTE MBR DEL DISCO Y PARTICIONES  GRAPHVIZ ********************************
   ofstream archivo;

   archivo.open(OBTENER_PATH_DOT(rep._path),ios::out);//ABRE EL ARCHIVO EN CASO DE QUE NO EXISTA LO CREA Y EN CASO DE QUE EXISTA LO SOBREESCRIBE

    if(archivo.fail()){
        imprimirln("==== ERROR: EL ARCHIVO NO PUEDO CREARSE Y/O ABRIRSE ====");
    }else{
        //archivo<<<<endl;
        archivo<<"digraph{"<<endl;//                                INCIO DE GRAPHIZ EL MBR
        archivo<<"tbl [shape=plaintext label=< <table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;
        archivo<<"<tr><td> <b>tamanio</b> </td><td>"+to_string(DISCO_REPORTE.mbr_tamano)+"</td></tr>"<<endl;
        //archivo<<"<tr><td> fecha creacion </td><td>"+to_string(ctime(&DISCO_REPORTE.mbr_fecha_creacion))+"</td></tr>"<<endl;//TIPO FECHA NO ACEPTADO
        archivo<<"<tr><td> <b>signature</b> </td><td>"+to_string(DISCO_REPORTE.mbr_disk_signature)+"</td></tr>"<<endl;
        archivo<<"<tr><td> <b>Disk fit</b> </td><td>"+OBTENER_VALOR_REPORTE_FIT_DISK(DISCO_REPORTE.mbr_fit)+"</td></tr>"<<endl;

        for(int i=0; i<4; i++){//CON ESTE FOR SE REALIZA EL REPORTE DE LAS PARTICIONES PRIMARIAS Y EXTENDIDAS DEL MBR
            if(DISCO_REPORTE.mbr_partition[i].part_status==49){//si esta disponible
                archivo<<"<tr><td> <b>part status_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partition[i].part_status+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part type_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partition[i].part_type+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part fit_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partition[i].part_fit+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part start_"+to_string(i+1)+"</b> </td><td>"+to_string(DISCO_REPORTE.mbr_partition[i].part_start)+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part size_"+to_string(i+1)+"</b> </td><td>"+to_string(DISCO_REPORTE.mbr_partition[i].part_size)+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part name_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partition[i].part_name+"</td></tr>"<<endl;
            }
        }
        archivo<<"</table>"<<endl;
        archivo<<">];"<<endl;

        for(int i=0; i<20; i++){//CON ESTE FOR SE REALIZA EL REPORTE DE LAS PARTICIONES LOGICAS
            if(DISCO_REPORTE.mbr_partitionEXT[i].part_status==49){
                archivo<<"tbl"+to_string(i+1)+" [shape=plaintext label=< <table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;
                archivo<<"<tr><td> <b>part Nombre_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partitionEXT[i].part_name+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part status_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partitionEXT[i].part_status+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part fit_"+to_string(i+1)+"</b> </td><td>"+DISCO_REPORTE.mbr_partitionEXT[i].part_fit+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part start_"+to_string(i+1)+"</b> </td><td>"+to_string(DISCO_REPORTE.mbr_partitionEXT[i].part_start)+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part size_"+to_string(i+1)+"</b> </td><td>"+to_string(DISCO_REPORTE.mbr_partitionEXT[i].part_size)+"</td></tr>"<<endl;
                archivo<<"<tr><td> <b>part next_"+to_string(i+1)+"</b> </td><td>"+to_string(DISCO_REPORTE.mbr_partitionEXT[i].part_next)+"</td></tr>"<<endl;
                archivo<<"</table>"<<endl;
                archivo<<">];"<<endl;
            }
        }


        archivo<<"}"<<endl;
        archivo.close();
    }
   /////************************************************* FIN CODIGO REPORTE ***************************************************************


   if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
        string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
        system(ruta_compilar.c_str());
   }else
   if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
        string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
        system(ruta_compilar.c_str());
   }else{
    imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
   }
    imprimirln(DATOS_PARTICION_MONTADA._path);
    MOSTRAR_INFORMACION_DISCO(DISCO_REPORTE);

}

void EJECUCION_DISK_REP(struct cmdREP rep){
    //imprimirln("entro a disk");
    ////////////////////////////////////////////////////////
       // imprimirln("entro a mbr");
   struct REGISTRO DATOS_PARTICION_MONTADA;//STRUCT PARA OBTENER LOS DATOS DE LA PARTICION MONTADA
   DATOS_PARTICION_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);

   struct MBRD_DISK DISCO_REPORTE;//STRUCT PARA OBTENER LOS DATOS DEL DISCO
   DISCO_REPORTE=OBTENER_MBR_DEL_DISCO(DATOS_PARTICION_MONTADA._path);

   char DIRECTORIOS_CARPETAS[rep._path.size()+1];
   strcpy(DIRECTORIOS_CARPETAS,rep._path.c_str());
   vector <string> CARPETAS_A_CREAR=split(DIRECTORIOS_CARPETAS,"/");
   imprimirln("++++ CREANDO CARPETAS ++++");
   crearCarpetas(CARPETAS_A_CREAR);//ESTE METODO CREA LAS CARPETAS EN CASO DE QUE NO EXISTAN, SI EXISTEN NO HACE NADA

   ////************************************** CODIGO PARA EL REPORTE MBR DEL DISCO Y PARTICIONES  GRAPHVIZ ********************************
   ofstream archivo;

   archivo.open(OBTENER_PATH_DOT(rep._path),ios::out);//ABRE EL ARCHIVO EN CASO DE QUE NO EXISTA LO CREA Y EN CASO DE QUE EXISTA LO SOBREESCRIBE

    if(archivo.fail()){
        imprimirln("==== ERROR: EL ARCHIVO NO PUEDO CREARSE Y/O ABRIRSE ====");
    }else{
        //archivo<<<<endl;

         int MATRIZ_VALORES_REPORTE[25][4];
         int POSICIONFILA=0;
         for(int i=0; i<25; i++){//INICIALIZAMOS MATRIZ EN CERO
            MATRIZ_VALORES_REPORTE[i][0]=0;
            MATRIZ_VALORES_REPORTE[i][1]=0;
            MATRIZ_VALORES_REPORTE[i][2]=0;
            MATRIZ_VALORES_REPORTE[i][3]=0;
         }

            MATRIZ_VALORES_REPORTE[POSICIONFILA][0]=0;
            MATRIZ_VALORES_REPORTE[POSICIONFILA][1]=(int)sizeof(MBRD_DISK);
            MATRIZ_VALORES_REPORTE[POSICIONFILA][2]=(int)sizeof(MBRD_DISK);
            MATRIZ_VALORES_REPORTE[POSICIONFILA][3]=-1;
            POSICIONFILA++;
         for(int i=0; i<4; i++){//RECUPERAMOS TODOS LOS VALORES DE PRIMARIAS MENOS DE EXTENDIDAS, LA EXTENDIDA LA RECUPERAMOS CON LAS PARTICIONES LOGICAS
            if(DISCO_REPORTE.mbr_partition[i].part_status==49){
                if(DISCO_REPORTE.mbr_partition[i].part_type!=101){
                    MATRIZ_VALORES_REPORTE[POSICIONFILA][0]=DISCO_REPORTE.mbr_partition[i].part_start;
                    MATRIZ_VALORES_REPORTE[POSICIONFILA][1]=DISCO_REPORTE.mbr_partition[i].part_size;
                    MATRIZ_VALORES_REPORTE[POSICIONFILA][2]=(DISCO_REPORTE.mbr_partition[i].part_start+DISCO_REPORTE.mbr_partition[i].part_size);
                    MATRIZ_VALORES_REPORTE[POSICIONFILA][3]=-1;
                    POSICIONFILA++;
                }
            }
         }

         for(int i=0; i<20; i++){//RECUPERAMOS LAS PARTICIONES LOGICAS QUE FORMAN PARTE DE LAS EXTENDIDAS
            if(DISCO_REPORTE.mbr_partitionEXT[i].part_status==49){
                MATRIZ_VALORES_REPORTE[POSICIONFILA][0]=DISCO_REPORTE.mbr_partitionEXT[i].part_start;
                MATRIZ_VALORES_REPORTE[POSICIONFILA][1]=DISCO_REPORTE.mbr_partitionEXT[i].part_size;
                MATRIZ_VALORES_REPORTE[POSICIONFILA][2]=(DISCO_REPORTE.mbr_partitionEXT[i].part_start+DISCO_REPORTE.mbr_partitionEXT[i].part_size);
                MATRIZ_VALORES_REPORTE[POSICIONFILA][3]=-2;
                POSICIONFILA++;
            }
         }

         /////////  ORDENANDO DATOS

         for(int i=0; (i<25) && (MATRIZ_VALORES_REPORTE[i][2]!=0); i++){

            for(int j=0; (j<25) && (MATRIZ_VALORES_REPORTE[j][2]!=0); j++){

                if((MATRIZ_VALORES_REPORTE[i][2])<(MATRIZ_VALORES_REPORTE[j][2])){

                    int tmp1=MATRIZ_VALORES_REPORTE[i][0];
                    int tmp2=MATRIZ_VALORES_REPORTE[i][1];
                    int tmp3=MATRIZ_VALORES_REPORTE[i][2];
                    int tmp4=MATRIZ_VALORES_REPORTE[i][3];
                    //INTERCAMBIANDO LOS VALORES PARA QUE ESTEN EN ORDEN
                    MATRIZ_VALORES_REPORTE[i][0]=MATRIZ_VALORES_REPORTE[j][0];
                    MATRIZ_VALORES_REPORTE[i][1]=MATRIZ_VALORES_REPORTE[j][1];
                    MATRIZ_VALORES_REPORTE[i][2]=MATRIZ_VALORES_REPORTE[j][2];
                    MATRIZ_VALORES_REPORTE[i][3]=MATRIZ_VALORES_REPORTE[j][3];

                    MATRIZ_VALORES_REPORTE[j][0]=tmp1;
                    MATRIZ_VALORES_REPORTE[j][1]=tmp2;
                    MATRIZ_VALORES_REPORTE[j][2]=tmp3;
                    MATRIZ_VALORES_REPORTE[j][3]=tmp4;

                }

            }

        }

         int temporal1;
         int temporal2;
         int temporal3;
         for(int i=0; (i<25) && (MATRIZ_VALORES_REPORTE[i][2]!=0); i++){//CON ESTO RECUPERAMOS LO ULTIMO QUE NOS FALTA DE ESPACIO LIBRE SI ES QUE LO HUBIERA
             temporal1=0;
             temporal2=0;
             temporal3=0;
             temporal1=MATRIZ_VALORES_REPORTE[i][0];
             temporal2=MATRIZ_VALORES_REPORTE[i][1];
             temporal3=MATRIZ_VALORES_REPORTE[i][2];
         }

         MATRIZ_VALORES_REPORTE[POSICIONFILA][0]=temporal3+1;
         MATRIZ_VALORES_REPORTE[POSICIONFILA][1]=(DISCO_REPORTE.mbr_tamano-temporal3-2);
         MATRIZ_VALORES_REPORTE[POSICIONFILA][2]=DISCO_REPORTE.mbr_tamano-1;

                  /////////////////////////////
         /////      MOSTRANDO FOR ORDENADOS /////

         for(int i=0; i<25; i++){
            imprimirNumln(MATRIZ_VALORES_REPORTE[i][0]);
            imprimirNumln(MATRIZ_VALORES_REPORTE[i][1]);
            imprimirNumln(MATRIZ_VALORES_REPORTE[i][2]);
            imprimirNumln(MATRIZ_VALORES_REPORTE[i][3]);
            imprimirln("**************** separador ********************");
         }

         int INICIO_PE=0;
         int FINAL_PE=0;
         bool salir=false;
         for(int i=0; (i<4) && (salir==false); i++){//OBTENEMOS EL INICIO Y FINAL DE LA PARTICION EXTENDIDA
            if(DISCO_REPORTE.mbr_partition[i].part_status==49){
                if(DISCO_REPORTE.mbr_partition[i].part_type==101){
                    salir=true;
                    INICIO_PE=DISCO_REPORTE.mbr_partition[i].part_start;
                    FINAL_PE=(DISCO_REPORTE.mbr_partition[i].part_start+DISCO_REPORTE.mbr_partition[i].part_size);
                }
            }
         }

         archivo<<"digraph{"<<endl;
         archivo<<"tbl[shape=plaintext label=< <table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;
         archivo<<"<tr>"<<endl;
         archivo<<"<td> MBR: <br/>"+to_string((MATRIZ_VALORES_REPORTE[0][1]*100)/DISCO_REPORTE.mbr_tamano)+"</td>"<<endl;
         int x=DISCO_REPORTE.mbr_tamano;

         for(int i=1; (i<25) && (MATRIZ_VALORES_REPORTE[i][2]!=0);i++){

            if((MATRIZ_VALORES_REPORTE[i][0]>=INICIO_PE) && (MATRIZ_VALORES_REPORTE[i][2]<=(FINAL_PE+1))){//SI ESTO ES CIERTO LA PARTICION ES LOGICA
            ////////


                if((MATRIZ_VALORES_REPORTE[i][0]-MATRIZ_VALORES_REPORTE[i-1][2])>5){
                    int tmpResultado=MATRIZ_VALORES_REPORTE[i][0]-MATRIZ_VALORES_REPORTE[i-1][2];
                    archivo<<"<td bgcolor=\"yellow\"> Libre: <br/>"<<((MATRIZ_VALORES_REPORTE[i][0]-MATRIZ_VALORES_REPORTE[i-1][2])*100/x)<<"% </td>"<<endl;
                }

                archivo<<"<td bgcolor=\"yellow\"> Particion Logica: <br/>"<<((MATRIZ_VALORES_REPORTE[i][1]*100)/x)<<"% </td>"<<endl;

            ///////
            }else{//EN CASO CONTRARIO LA PARTICION ES PRIMARIA
                int resultado=((MATRIZ_VALORES_REPORTE[i][1]*100)/x);
                imprimirln("matriz valor resporte i de 1 ");
                imprimirNumln(MATRIZ_VALORES_REPORTE[i][1]);
                float calculo=(float)(MATRIZ_VALORES_REPORTE[i][1]/x);
                cout<<MATRIZ_VALORES_REPORTE[i][1]/x<<endl;
                imprimirNumln(x);
                cout<<(calculo*100)<<endl;
                if((MATRIZ_VALORES_REPORTE[i][0]-MATRIZ_VALORES_REPORTE[i-1][2])>5){
                    //int tmpResultado=MATRIZ_VALORES_REPORTE[i][0]-MATRIZ_VALORES_REPORTE[i-1][2];
                    archivo<<"<td> Libre: <br/>"<<((MATRIZ_VALORES_REPORTE[i][0]-MATRIZ_VALORES_REPORTE[i-1][2])*100/x)<<"% </td>"<<endl;
                }

                archivo<<"<td> Particion Primaria: <br/>"<<((MATRIZ_VALORES_REPORTE[i][1]*100)/x)<<"% </td>"<<endl;

            }

         }
        archivo<<"</tr>"<<endl;
        archivo<<"</table>"<<endl;
        archivo<<">];"<<endl;
        archivo<<"}"<<endl;



        archivo.close();
    }
   /////************************************************* FIN CODIGO REPORTE ***************************************************************


   if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
        string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
        system(ruta_compilar.c_str());
   }else
   if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
        string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
        system(ruta_compilar.c_str());
   }else{
    imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
   }
    imprimirln(DATOS_PARTICION_MONTADA._path);
    MOSTRAR_INFORMACION_DISCO(DISCO_REPORTE);

    ///////////////////////////////////////////////////////
}

void EJECION_INODE_REP(struct cmdREP rep){
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph g{"<<endl;
                        ARCHIVO_REPORTE<<"rankdir=LR;"<<endl;
                        ARCHIVO_REPORTE<<"node[shape=box]"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int INICIO_BITPMAP_INODOS=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }
                    //    imprimirln("va a entrar a for de bitmpa");
                    //    imprimirNumln(INICIO_BITPMAP_INODOS);
                        for(int i=INICIO_BITPMAP_INODOS; (i<INICIO_BITPMAP_INODOS+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){//SIGNIFICA QUE ENCONTRO UN BITMAP LIBRE
                       //         imprimirNumln(i);
                       //         imprimirln("if verificado");
                                int POSICION_INODO=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(IDENTIFICADOR*sizeof(Tabla_Inodos));
                                struct Tabla_Inodos INODO;
                       //         imprimirln("va a entrar a recuperar inodo");
                                INODO=RECUPERAR_INODO(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);

                                ARCHIVO_REPORTE<<"inode"<<IDENTIFICADOR<<"[label=\"inode"<<IDENTIFICADOR<<"\n";
                                ARCHIVO_REPORTE<<"i_uid:....."<<INODO.i_uid<<"\n";
                                ARCHIVO_REPORTE<<"i_gid:....."<<INODO.i_gid<<"\n";
                                ARCHIVO_REPORTE<<"i_size:....."<<INODO.i_size<<"\n";
                                ARCHIVO_REPORTE<<"i_atime:...."<<ctime(&INODO.i_atime)<<"\n";
                                ARCHIVO_REPORTE<<"i_ctime:...."<<ctime(&INODO.i_ctime)<<"\n";
                                ARCHIVO_REPORTE<<"i_mtime:...."<<ctime(&INODO.i_mtime)<<"\n";
                                for(int j=0; j<16; j++){
                                    ARCHIVO_REPORTE<<"i_block_"<<j<<":....."<<INODO.i_block[j]<<"\n";
                                }
                                ARCHIVO_REPORTE<<"i_type:...."<<INODO.i_type<<"\n";
                                ARCHIVO_REPORTE<<"i_perm:...."<<INODO.i_perm<<"\n";
                                ARCHIVO_REPORTE<<"\"]"<<endl;

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"inode"<<PIVOTE<<"->"<<"inode"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }
                            }
                            IDENTIFICADOR++;
                        }
                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE CREAR REPORTE INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
            imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE LOGIC INODE ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph g{"<<endl;
                        ARCHIVO_REPORTE<<"rankdir=LR;"<<endl;
                        ARCHIVO_REPORTE<<"node[shape=box]"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int INICIO_BITPMAP_INODOS=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO LOGIC INODE ===");
                        }
                        imprimirln("va a entrar a for de bitmpa");
                        imprimirNumln(INICIO_BITPMAP_INODOS);
                        for(int i=INICIO_BITPMAP_INODOS; (i<INICIO_BITPMAP_INODOS+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){//SIGNIFICA QUE ENCONTRO UN BITMAP LIBRE
                                imprimirNumln(i);
                                imprimirln("if verificado");
                                int POSICION_INODO=DATOS_PARTICION.part_start+sizeof(Super_Bloque)+(4*VALOR_N)+(IDENTIFICADOR*sizeof(Tabla_Inodos));
                                struct Tabla_Inodos INODO;
                                imprimirln("va a entrar a recuperar inodo");
                                INODO=RECUPERAR_INODO(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);

                                ARCHIVO_REPORTE<<"inode"<<IDENTIFICADOR<<"[label=\"inode"<<IDENTIFICADOR<<"\n";
                                ARCHIVO_REPORTE<<"i_uid:....."<<INODO.i_uid<<"\n";
                                ARCHIVO_REPORTE<<"i_gid:....."<<INODO.i_gid<<"\n";
                                ARCHIVO_REPORTE<<"i_size:....."<<INODO.i_size<<"\n";
                                ARCHIVO_REPORTE<<"i_atime:...."<<ctime(&INODO.i_atime)<<"\n";
                                ARCHIVO_REPORTE<<"i_ctime:...."<<ctime(&INODO.i_ctime)<<"\n";
                                ARCHIVO_REPORTE<<"i_mtime:...."<<ctime(&INODO.i_mtime)<<"\n";
                                for(int j=0; j<16; j++){
                                    ARCHIVO_REPORTE<<"i_block_"<<j<<":....."<<INODO.i_block[j]<<"\n";
                                }
                                ARCHIVO_REPORTE<<"i_type:...."<<INODO.i_type<<"\n";
                                ARCHIVO_REPORTE<<"i_perm:...."<<INODO.i_perm<<"\n";
                                ARCHIVO_REPORTE<<"\"]"<<endl;

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"inode"<<PIVOTE<<"->"<<"inode"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }
                            }
                            IDENTIFICADOR++;
                        }
                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTION PARA REP INODE ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE LA PARTICION MONTADA CON ESE ID ===");
    }
}

void EJECUCION_BLOCK_REP(struct cmdREP rep){
////
 if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
    //              imprimirln("reporte primaria");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);


                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE BLOCK ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph g{"<<endl;
                        ARCHIVO_REPORTE<<"rankdir=LR;"<<endl;
                        ARCHIVO_REPORTE<<"node[shape=box]"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        string path_extra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PARTICION.part_name+"/"+DATOS_PARTICION.part_name+".bin";
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                        int INICIO_BM_BC=0;
                        int INICIO_BM_BAP=VALOR_N;
                        int INICIO_BM_BAR=2*VALOR_N;

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(path_extra.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO ===");
                        }

                        //--------------------------------ESTE PRIMER FOR ES PARA LAS CARPETAS
                        for(int i=INICIO_BM_BC; i<(INICIO_BM_BC+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){
                                struct B_Carpetas CARPETA;
                                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);

                                ARCHIVO_REPORTE<<"carpeta"<<IDENTIFICADOR<<"[label=\"block_carpeta"<<IDENTIFICADOR<<"\n";
                                for(int j=0; j<4; j++){
                                    if(CARPETA.b_content[j].b_inodo!=-1){
                                        ARCHIVO_REPORTE<<CARPETA.b_content[j].b_name<<"    "<<CARPETA.b_content[j].b_inodo<<"\n";
                                    }else{
                                        ARCHIVO_REPORTE<<"NULL"<<"     "<<CARPETA.b_content[j].b_inodo<<"\n";
                                    }
                                }
                                ARCHIVO_REPORTE<<"\"]";

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"carpeta"<<PIVOTE<<"->"<<"carpeta"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }

                            }
                            IDENTIFICADOR++;
                        }
                        //--------------------------------ESTE SEGUNDO FOR ES PARA LOS APUNTDORES
                        IDENTIFICADOR=0;
                        PIVOTE=0;
                        for(int i=INICIO_BM_BAP; i<(INICIO_BM_BAP+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){
                                struct B_Apuntadores APUNTADOR;
                                APUNTADOR=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);
                                ARCHIVO_REPORTE<<"apuntador"<<IDENTIFICADOR<<"[label=\"block_indirecto"<<IDENTIFICADOR<<"\n";

                                for(int j=0; j<16; j++){
                                    ARCHIVO_REPORTE<<j<<"    "<<APUNTADOR.b_pointers[j]<<"\n";
                                }
                                ARCHIVO_REPORTE<<"\"]";

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"apuntador"<<PIVOTE<<"->"<<"apuntador"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }
                            }
                            IDENTIFICADOR++;
                        }
                        //---------------------------------ESTE TERCER FOR ES PARA LOS ARCHIVOS
                        IDENTIFICADOR=0;
                        PIVOTE=0;
                        for(int i=INICIO_BM_BAR;i<(INICIO_BM_BAR+VALOR_N);i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){
                                struct B_Archivos ARCHIVO;
                                ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);
                                ARCHIVO_REPORTE<<"archivo"<<IDENTIFICADOR<<"[label=\"block_archivo"<<IDENTIFICADOR<<"\n";

                                ARCHIVO_REPORTE<<ARCHIVO.b_contentA<<"\n";

                                ARCHIVO_REPORTE<<"\"]";

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"archivo"<<PIVOTE<<"->"<<"archivo"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }

                            }
                            IDENTIFICADOR++;
                        }
                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTICION PARA REP BLOCK, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
     //       imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
     //          imprimirln("repote logica");
                    vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);


                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE  BLOCK LOGIC===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph g{"<<endl;
                        ARCHIVO_REPORTE<<"rankdir=LR;"<<endl;
                        ARCHIVO_REPORTE<<"node[shape=box]"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        string path_extra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PARTICION.part_name+"/"+DATOS_PARTICION.part_name+".bin";
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                        int INICIO_BM_BC=0;
                        int INICIO_BM_BAP=VALOR_N;
                        int INICIO_BM_BAR=2*VALOR_N;

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(path_extra.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO ===");
                        }

                        //--------------------------------ESTE PRIMER FOR ES PARA LAS CARPETAS
                        for(int i=INICIO_BM_BC; i<(INICIO_BM_BC+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){
                                struct B_Carpetas CARPETA;
                                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);

                                ARCHIVO_REPORTE<<"carpeta"<<IDENTIFICADOR<<"[label=\"block_carpeta"<<IDENTIFICADOR<<"\n";
                                for(int j=0; j<4; j++){
                                    if(CARPETA.b_content[j].b_inodo!=-1){
                                        ARCHIVO_REPORTE<<CARPETA.b_content[j].b_name<<"    "<<CARPETA.b_content[j].b_inodo<<"\n";
                                    }else{
                                        ARCHIVO_REPORTE<<"NULL"<<"     "<<CARPETA.b_content[j].b_inodo<<"\n";
                                    }
                                }
                                ARCHIVO_REPORTE<<"\"]";

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"carpeta"<<PIVOTE<<"->"<<"carpeta"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }

                            }
                            IDENTIFICADOR++;
                        }
                        //--------------------------------ESTE SEGUNDO FOR ES PARA LOS APUNTDORES
                        IDENTIFICADOR=0;
                        PIVOTE=0;
                        for(int i=INICIO_BM_BAP; i<(INICIO_BM_BAP+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){
                                struct B_Apuntadores APUNTADOR;
                                APUNTADOR=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);
                                ARCHIVO_REPORTE<<"apuntador"<<IDENTIFICADOR<<"[label=\"block_indirecto"<<IDENTIFICADOR<<"\n";

                                for(int j=0; j<16; j++){
                                    ARCHIVO_REPORTE<<j<<"    "<<APUNTADOR.b_pointers[j]<<"\n";
                                }
                                ARCHIVO_REPORTE<<"\"]";

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"apuntador"<<PIVOTE<<"->"<<"apuntador"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }
                            }
                            IDENTIFICADOR++;
                        }
                        //---------------------------------ESTE TERCER FOR ES PARA LOS ARCHIVOS
                        IDENTIFICADOR=0;
                        PIVOTE=0;
                        for(int i=INICIO_BM_BAR;i<(INICIO_BM_BAR+VALOR_N);i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);
                            if(verificar==49){
                                struct B_Archivos ARCHIVO;
                                ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(DATOS_PARTICION.part_start,IDENTIFICADOR,VALOR_N,DATOS_PART_MONTADA._path);
                                ARCHIVO_REPORTE<<"archivo"<<IDENTIFICADOR<<"[label=\"block_archivo"<<IDENTIFICADOR<<"\n";

                                ARCHIVO_REPORTE<<ARCHIVO.b_contentA<<"\n";

                                ARCHIVO_REPORTE<<"\"]";

                                if(IDENTIFICADOR!=0){
                                    ARCHIVO_REPORTE<<"archivo"<<PIVOTE<<"->"<<"archivo"<<IDENTIFICADOR<<endl;
                                    PIVOTE=IDENTIFICADOR;
                                }

                            }
                            IDENTIFICADOR++;
                        }
                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: LA PARTICION NO SE PUEDE REALIZAR PARA REP BLOCK LOGIC ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE LA PARTICION MONTADA CON ESE ID ===");
    }
////
}

void EJECUCION_BM_INODE_REP(struct cmdREP rep){
////
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(rep._path,ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE BM INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int INICIO_BITPMAP_INODOS=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO ===");
                        }

                        for(int i=INICIO_BITPMAP_INODOS; (i<INICIO_BITPMAP_INODOS+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<19){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }
                            IDENTIFICADOR++;

                        }
                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE.close();
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTICION PARA BM INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
  //          imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
  //              imprimirln("logica parte logica");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(rep._path,ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE BM INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int INICIO_BITPMAP_INODOS=DATOS_PARTICION.part_start+sizeof(Super_Bloque);
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO BM INODE ===");
                        }

                        for(int i=INICIO_BITPMAP_INODOS; (i<INICIO_BITPMAP_INODOS+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<19){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }
                            IDENTIFICADOR++;

                        }
                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE.close();
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE USAR BM INODE BLOCK LOGIC ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
////
}

void EJECUCION_BM_BLOCK_REP(struct cmdREP rep){
////
 if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
             //     imprimirln("reporte primaria");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(rep._path,ios::out);


                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE BM BLOCK ===");
                   }else{

                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        string path_extra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PARTICION.part_name+"/"+DATOS_PARTICION.part_name+".bin";
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                        int INICIO_BM_BC=0;
                        int INICIO_BM_BAP=VALOR_N;
                        int INICIO_BM_BAR=2*VALOR_N;

                        int IDENTIFICADOR=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(path_extra.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO BM BLOCK ===");
                        }
                        ARCHIVO_REPORTE<<"---------------------BM CARPETAS -------------------------------------------------"<<endl;
                        //--------------------------------ESTE PRIMER FOR ES PARA LAS CARPETAS
                        for(int i=INICIO_BM_BC; i<(INICIO_BM_BC+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<20){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }

                            IDENTIFICADOR++;
                        }
                        //--------------------------------ESTE SEGUNDO FOR ES PARA LOS APUNTDORES
                        ARCHIVO_REPORTE<<endl;
                        ARCHIVO_REPORTE<<"---------------------BM APUNTADORES -------------------------------------------------"<<endl;
                        IDENTIFICADOR=0;
                        for(int i=INICIO_BM_BAP; i<(INICIO_BM_BAP+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<20){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }

                            IDENTIFICADOR++;
                        }
                        //---------------------------------ESTE TERCER FOR ES PARA LOS ARCHIVOS
                        ARCHIVO_REPORTE<<endl;
                        ARCHIVO_REPORTE<<"---------------------BM ARCHIVOS -------------------------------------------------"<<endl;
                        IDENTIFICADOR=0;
                        for(int i=INICIO_BM_BAR;i<(INICIO_BM_BAR+VALOR_N);i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<20){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }

                            IDENTIFICADOR++;
                        }

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }

                   ARCHIVO_REPORTE.close();

            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTICON PARA BM BLOCK, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
    //        imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(rep._path,ios::out);


                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE BM BLOCK ===");
                   }else{

                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        string path_extra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PARTICION.part_name+"/"+DATOS_PARTICION.part_name+".bin";
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);
                        int INICIO_BM_BC=0;
                        int INICIO_BM_BAP=VALOR_N;
                        int INICIO_BM_BAR=2*VALOR_N;

                        int IDENTIFICADOR=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(path_extra.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO BM BLOCK ===");
                        }
                        ARCHIVO_REPORTE<<"---------------------BM CARPETAS -------------------------------------------------"<<endl;
                        //--------------------------------ESTE PRIMER FOR ES PARA LAS CARPETAS
                        for(int i=INICIO_BM_BC; i<(INICIO_BM_BC+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<20){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }

                            IDENTIFICADOR++;
                        }
                        //--------------------------------ESTE SEGUNDO FOR ES PARA LOS APUNTDORES
                        ARCHIVO_REPORTE<<endl;
                        ARCHIVO_REPORTE<<"---------------------BM APUNTADORES -------------------------------------------------"<<endl;
                        IDENTIFICADOR=0;
                        for(int i=INICIO_BM_BAP; i<(INICIO_BM_BAP+VALOR_N); i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<20){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }

                            IDENTIFICADOR++;
                        }
                        //---------------------------------ESTE TERCER FOR ES PARA LOS ARCHIVOS
                        ARCHIVO_REPORTE<<endl;
                        ARCHIVO_REPORTE<<"---------------------BM ARCHIVOS -------------------------------------------------"<<endl;
                        IDENTIFICADOR=0;
                        for(int i=INICIO_BM_BAR;i<(INICIO_BM_BAR+VALOR_N);i++){
                            verificar=00;
                            fseek(ARCHIVO_PIVOTE,i,SEEK_SET);
                            fread(&verificar,1,1,ARCHIVO_PIVOTE);

                            if(IDENTIFICADOR<20){
                                ARCHIVO_REPORTE<<verificar;
                            }else{
                                ARCHIVO_REPORTE<<verificar<<endl;
                                IDENTIFICADOR=0;
                            }

                            IDENTIFICADOR++;
                        }

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }

                   ARCHIVO_REPORTE.close();
               ////
            }else{
                imprimirln("=== ERROR: NO SE PUEDE USAR LA PARTICION PARA BM BLOCK LOGIC ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
////
}

void EJECUCION_SB_REP(struct cmdREP rep){
////
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph{"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }

                        struct Super_Bloque SUPERBLOQUE;
                        fseek(ARCHIVO_PIVOTE,DATOS_PARTICION.part_start,SEEK_SET);
                        fread(&SUPERBLOQUE,sizeof(Super_Bloque),1,ARCHIVO_PIVOTE);

                        ARCHIVO_REPORTE<<"tbl[ shape=plaintext label=< <table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;

                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_inodes_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_inodes_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_blocks_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_blocks_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_free_blocks_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_free_blocks_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_free_inodes_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_free_inodes_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_mtime</td><td bgcolor=\"blue\">"<<ctime(&SUPERBLOQUE.s_mtime)<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_umtime</td><td bgcolor=\"blue\">"<<ctime(&SUPERBLOQUE.s_umtime)<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_mnt_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_mnt_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_magic</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_magic<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_inode_size</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_inode_size<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_block_size</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_block_size<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_first_ino</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_first_ino<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_first_block</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_first_blo<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_bm_inode_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_bm_inode_start<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_bm_block_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_bm_block_start<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_inode_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_inode_start<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_block_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_block_start<<"</td></tr>"<<endl;

                        ARCHIVO_REPORTE<<"</table>  >];"<<endl;

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE CREAR REPORTE INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
            imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph{"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }

                        struct Super_Bloque SUPERBLOQUE;
                        fseek(ARCHIVO_PIVOTE,DATOS_PARTICION.part_start,SEEK_SET);
                        fread(&SUPERBLOQUE,sizeof(Super_Bloque),1,ARCHIVO_PIVOTE);

                        ARCHIVO_REPORTE<<"tbl[ shape=plaintext label=< <table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;

                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_inodes_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_inodes_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_blocks_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_blocks_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_free_blocks_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_free_blocks_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_free_inodes_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_free_inodes_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_mtime</td><td bgcolor=\"blue\">"<<ctime(&SUPERBLOQUE.s_mtime)<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_umtime</td><td bgcolor=\"blue\">"<<ctime(&SUPERBLOQUE.s_umtime)<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_mnt_count</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_mnt_count<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_magic</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_magic<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_inode_size</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_inode_size<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_block_size</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_block_size<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_first_ino</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_first_ino<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_first_block</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_first_blo<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_bm_inode_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_bm_inode_start<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_bm_block_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_bm_block_start<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_inode_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_inode_start<<"</td></tr>"<<endl;
                        ARCHIVO_REPORTE<<"<tr><td bgcolor=\"blue\">s_block_start</td><td bgcolor=\"blue\">"<<SUPERBLOQUE.s_block_start<<"</td></tr>"<<endl;

                        ARCHIVO_REPORTE<<"</table>  >];"<<endl;

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTION PARA REP INODE ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
////
}

void EJECUCION_FILE_REP(struct cmdREP rep){
////
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }

                        struct DATOS_PUBLICOS TEMPORAL_DATOS;
                        struct DATOS_PUBLICOS NUEVO;
                        NUEVO.g_id=1;
                        NUEVO.u_id=1;
                        NUEVO.path_disco=DATOS_PART_MONTADA._path;
                        NUEVO.valor_N=VALOR_N;
                        NUEVO.inicio_particion=DATOS_PARTICION.part_start;
                        NUEVO.path_raid=OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path);
                        string nombre_estra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        string nombre_journaling=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+"jour"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        NUEVO.path_archivoExtra=nombre_estra;
                        NUEVO.path_journaling=nombre_journaling;
                        bool CON_DATOS=false;

                        if(DATOS_PIVOTE.size()!=0){//ENTRA SI ES DIFERENTE DE CERO
                            TEMPORAL_DATOS=DATOS_PIVOTE[0];
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                            CON_DATOS=true;
                        }else{//ENTRA SI ES IGUAL A CERO
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                        }

                        ARCHIVO_REPORTE<<"digraph g{ rankdir=LR; node[shape=box]"<<endl;

                        if(rep._ruta!=""){
                            if(VALIDAR_EXTENSION_PATH(rep._ruta)){
                                vector<string> DIRECCION=OBTENER_DIRECCION(rep._ruta,"/");
                                string DATOS=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);
                                vector<string> TEXTOSEPARADO=TEXTO_BLOQUES_ARCHIVODISCO(DATOS);

                                ARCHIVO_REPORTE<<"a_texto[label=\""<<endl;
                                ARCHIVO_REPORTE<<OBTENER_NOMBRE_DISCO(rep._ruta)<<endl;
                                for(int j=0; j<TEXTOSEPARADO.size(); j++){
                                    ARCHIVO_REPORTE<<TEXTOSEPARADO[j]<<endl;
                                }
                                ARCHIVO_REPORTE<<"\"]"<<endl;
                            }
                        }else{
                            imprimirln("=== ERROR: NO INDICO UNA RUTA PARA OBTENER TEXTO ===");
                        }


                        DATOS_PIVOTE.clear();
                        if(CON_DATOS==true){
                            DATOS_PIVOTE.push_back(TEMPORAL_DATOS);
                        }

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE CREAR REPORTE INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
        //    imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        int IDENTIFICADOR=0;
                        int PIVOTE=0;
                        char verificar;
                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }

                        struct DATOS_PUBLICOS TEMPORAL_DATOS;
                        struct DATOS_PUBLICOS NUEVO;
                        NUEVO.g_id=1;
                        NUEVO.u_id=1;
                        NUEVO.path_disco=DATOS_PART_MONTADA._path;
                        NUEVO.valor_N=VALOR_N;
                        NUEVO.inicio_particion=DATOS_PARTICION.part_start;
                        NUEVO.path_raid=OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path);
                        string nombre_estra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        string nombre_journaling=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+"jour"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        NUEVO.path_archivoExtra=nombre_estra;
                        NUEVO.path_journaling=nombre_journaling;
                        bool CON_DATOS=false;

                        if(DATOS_PIVOTE.size()!=0){//ENTRA SI ES DIFERENTE DE CERO
                            TEMPORAL_DATOS=DATOS_PIVOTE[0];
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                            CON_DATOS=true;
                        }else{//ENTRA SI ES IGUAL A CERO
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                        }

                        ARCHIVO_REPORTE<<"digraph g{ rankdir=LR; node[shape=box]"<<endl;

                        if(rep._ruta!=""){
                            if(VALIDAR_EXTENSION_PATH(rep._ruta)){
                                vector<string> DIRECCION=OBTENER_DIRECCION(rep._ruta,"/");
                                string DATOS=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(DIRECCION,0,0);
                                vector<string> TEXTOSEPARADO=TEXTO_BLOQUES_ARCHIVODISCO(DATOS);

                                ARCHIVO_REPORTE<<"a_texto[label=\""<<endl;
                                ARCHIVO_REPORTE<<OBTENER_NOMBRE_DISCO(rep._ruta)<<endl;
                                for(int j=0; j<TEXTOSEPARADO.size(); j++){
                                    ARCHIVO_REPORTE<<TEXTOSEPARADO[j]<<endl;
                                }
                                ARCHIVO_REPORTE<<"\"]"<<endl;
                            }
                        }else{
                            imprimirln("=== ERROR: NO INDICO UNA RUTA PARA OBTENER TEXTO ===");
                        }


                        DATOS_PIVOTE.clear();
                        if(CON_DATOS==true){
                            DATOS_PIVOTE.push_back(TEMPORAL_DATOS);
                        }

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTION PARA REP INODE ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
////
}

void EJECUCION_JOURNALING_REP(struct cmdREP rep){
////
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);
                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE

                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }

                        ARCHIVO_REPORTE<<"digraph{ tbl[ shape=plaintext label=<"<<endl;
                        ARCHIVO_REPORTE<<"<table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;
                        string path_journaling=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+"jour_"+DATOS_PART_MONTADA._nombreParticion+".bin";

                        int CONTADOR_J=RECUPERAR_CONTADOR_JOURNALING(path_journaling);
                        for(int j=0; j<CONTADOR_J; j++){
                            struct JOURNALING TMP_JOUR;
                            int posicion_recuperar=sizeof(CONTADOR_JOURNALING)+(j*sizeof(JOURNALING));
                            TMP_JOUR=RECUPERAR_JOURNALING(path_journaling,posicion_recuperar);
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">JOUT #</td><td>"<<j<<"</td></tr>"<<endl;
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">RUTA</td><td>"<<TMP_JOUR.RUTA<<"</td></tr>"<<endl;
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">CONTENIDO</td><td>"<<TMP_JOUR.CONTENIDO<<"</td></tr>"<<endl;
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">FECHA/HORA</td><td>"<<ctime(&TMP_JOUR.FECHA_CREACION)<<"</td></tr>"<<endl;

                        }
                        ARCHIVO_REPORTE<<"</table> >];"<<endl;

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE CREAR REPORTE INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
 //           imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);
                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE

                        if(ARCHIVO_PIVOTE==NULL){
                            ARCHIVO_PIVOTE=fopen(DATOS_PART_MONTADA._path.c_str(),"rb+");
                        }else{
                            imprimirln("=== ERROR: ARCHIVO ABIERTO REP INODE ===");
                        }

                        ARCHIVO_REPORTE<<"digraph{ tbl[ shape=plaintext label=<"<<endl;
                        ARCHIVO_REPORTE<<"<table border='0' cellborder='1' color='black' cellspacing='0'>"<<endl;
                        string path_journaling=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+"jour_"+DATOS_PART_MONTADA._nombreParticion+".bin";

                        int CONTADOR_J=RECUPERAR_CONTADOR_JOURNALING(path_journaling);
                        for(int j=0; j<CONTADOR_J; j++){
                            struct JOURNALING TMP_JOUR;
                            int posicion_recuperar=sizeof(CONTADOR_JOURNALING)+(j*sizeof(JOURNALING));
                            TMP_JOUR=RECUPERAR_JOURNALING(path_journaling,posicion_recuperar);
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">JOUT #</td><td>"<<j<<"</td></tr>"<<endl;
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">RUTA</td><td>"<<TMP_JOUR.RUTA<<"</td></tr>"<<endl;
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">CONTENIDO</td><td>"<<TMP_JOUR.CONTENIDO<<"</td></tr>"<<endl;
                            ARCHIVO_REPORTE<<"<tr><td bgcolor=\"yellow\">FECHA/HORA</td><td>"<<ctime(&TMP_JOUR.FECHA_CREACION)<<"</td></tr>"<<endl;

                        }
                        ARCHIVO_REPORTE<<"</table> >];"<<endl;

                        fflush(ARCHIVO_PIVOTE);
                        fclose(ARCHIVO_PIVOTE);
                        ARCHIVO_PIVOTE=NULL;
                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTION PARA REP INODE ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
////
}

void EJECUCION_TREE_REP(struct cmdREP rep){
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);
                   int valorN=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph structs{"<<endl;
                        ARCHIVO_REPORTE<<"rankdir=LR;"<<endl;
                        ARCHIVO_REPORTE<<"node[shape=record];"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        struct PARAMETRO_REP_TREE param;
                        param.path_disco=DATOS_PART_MONTADA._path;
                        param.valor_n=valorN;
                        param.inicio_particion=DATOS_PARTICION.part_start;

                        INICIO_REPORTE_INODOS(param,0,-1,-1);

                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE CREAR REPORTE INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
         //   imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);
                   int valorN=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        ARCHIVO_REPORTE<<"digraph structs{"<<endl;
                        ARCHIVO_REPORTE<<"rankdir=LR;"<<endl;
                        ARCHIVO_REPORTE<<"node[shape=record];"<<endl;
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        struct PARAMETRO_REP_TREE param;
                        param.path_disco=DATOS_PART_MONTADA._path;
                        param.valor_n=valorN;
                        param.inicio_particion=DATOS_PARTICION.part_start;

                        INICIO_REPORTE_INODOS(param,0,-1,-1);

                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTION PARA REP INODE ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
}

void EJECUCION_LS_REP(struct cmdREP rep){
////
    if(EXISTE_LLAVE_REGISTRO(rep._id)){//SE REALIZARA SOBRE UNA PARTICION MONTADA
        ////CODIGO EXTRAIDO DE LOSS
        struct REGISTRO DATOS_PART_MONTADA;
        DATOS_PART_MONTADA=OBTENER_DATOS_REGISTRO_PMONTADA(rep._id);
        //DATOS DEL DISCO
        struct MBRD_DISK DISCO;
        DISCO=OBTENER_MBR_DEL_DISCO(DATOS_PART_MONTADA._path);

        if(EXISTE_PARTICION_EN_DISCO(DISCO,DATOS_PART_MONTADA._nombreParticion)==false){//SI ENTRA AQUI VA A USAR UNA PARTIICON PRIMARIA O EXTENDIDA
            struct PARTITION_MBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_PRIMARIA(DISCO,DATOS_PART_MONTADA._nombreParticion);

            //VALIDANDO QUE SEA UNA PARTICION PRIMARIA
            if(DATOS_PARTICION.part_type==112){
                   // imprimirln("se puede realizar reporte particion primaria ");
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        struct DATOS_PUBLICOS TEMPORAL_DATOS;
                        struct DATOS_PUBLICOS NUEVO;
                        NUEVO.g_id=1;
                        NUEVO.u_id=1;
                        NUEVO.path_disco=DATOS_PART_MONTADA._path;
                        NUEVO.valor_N=VALOR_N;
                        NUEVO.inicio_particion=DATOS_PARTICION.part_start;
                        NUEVO.path_raid=OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path);
                        string nombre_estra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        string nombre_journaling=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+"jour"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        NUEVO.path_archivoExtra=nombre_estra;
                        NUEVO.path_journaling=nombre_journaling;
                        bool CON_DATOS=false;

                        if(DATOS_PIVOTE.size()!=0){//ENTRA SI ES DIFERENTE DE CERO
                            TEMPORAL_DATOS=DATOS_PIVOTE[0];
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                            CON_DATOS=true;
                        }else{//ENTRA SI ES IGUAL A CERO
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                        }

                        ARCHIVO_REPORTE<<"digraph {"<<endl;


                            if(rep._ruta!=""){
                                vector<string> DIRECCION=OBTENER_DIRECCION(rep._ruta,"/");

                                ARCHIVO_REPORTE<<"tbl[ shape=plaintext label=<"<<endl;
                                ARCHIVO_REPORTE<<"<table border='0' cellborder='1' color='black' cellspacing='0'>\n";
                                ARCHIVO_REPORTE<<"<tr><td>Permisos</td><td>Owner</td><td>Grupo</td><td>Size</td><td>Fecha/Hora</td><td>Tipo</td><td>Name</td></tr>"<<endl;
                                    INICIO_REPORTE_LS(DIRECCION,0,0);
                                ARCHIVO_REPORTE<<"</table> >];"<<endl;
                            }else{
                                imprimirln("=== ERROR: NO ESPECIFICO NINGUNA RUTA ===");
                            }


                        DATOS_PIVOTE.clear();
                        if(CON_DATOS==true){
                            DATOS_PIVOTE.push_back(TEMPORAL_DATOS);
                        }

                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                   //
            }else{
                imprimirln("=== ERROR: NO SE PUEDE CREAR REPORTE INODE, PUEDE QUE SEA EXTENDIDA ===");
            }
        }else{//EN CASO CONTRARIO VA USAR UNA PARTICION LOGICA
            struct PARTITION_EBR_DISK DATOS_PARTICION;
            DATOS_PARTICION=OBTENER_DATOS_PARTICION_LOGICA(DISCO,DATOS_PART_MONTADA._nombreParticion);
            imprimirln("REPORTE DE LOGICAS");
            if(strcmp(DATOS_PARTICION.part_name,DATOS_PART_MONTADA._nombreParticion.c_str())==0){
                ////-----------------------------CODIGO DE PRIMARIA
                   vector<string> CREAR_CARPETAS=OBTENER_DIRECCION(rep._path,"/");
                   crearCarpetas(CREAR_CARPETAS);
                   ARCHIVO_REPORTE.open(OBTENER_PATH_DOT(rep._path),ios::out);

                   if(ARCHIVO_REPORTE.fail()){
                    imprimirln("=== ERROR: OCURRIO UN ERROR AL TRATAR DE ABRIR/CREAR ARCHIVO REPORTE INODE ===");
                   }else{
                        //AQUI COMIENZA EL CODIGO PARA EL MANEJO DE LOS INODOS RECUPERARLOS Y ESCRIBIR LOS DATOS EN EL REPORTE
                        int VALOR_N=OBTENER_CANTIDAD_N_EXT2(DATOS_PARTICION.part_size);

                        struct DATOS_PUBLICOS TEMPORAL_DATOS;
                        struct DATOS_PUBLICOS NUEVO;
                        NUEVO.g_id=1;
                        NUEVO.u_id=1;
                        NUEVO.path_disco=DATOS_PART_MONTADA._path;
                        NUEVO.valor_N=VALOR_N;
                        NUEVO.inicio_particion=DATOS_PARTICION.part_start;
                        NUEVO.path_raid=OBTENER_PATH_ARCHIVORAID(DATOS_PART_MONTADA._path);
                        string nombre_estra=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        string nombre_journaling=OBTENER_NOMBRE_DISCO(DATOS_PART_MONTADA._path)+"_"+DATOS_PART_MONTADA._nombreParticion+"/"+"jour"+DATOS_PART_MONTADA._nombreParticion+".bin";
                        NUEVO.path_archivoExtra=nombre_estra;
                        NUEVO.path_journaling=nombre_journaling;
                        bool CON_DATOS=false;

                        if(DATOS_PIVOTE.size()!=0){//ENTRA SI ES DIFERENTE DE CERO
                            TEMPORAL_DATOS=DATOS_PIVOTE[0];
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                            CON_DATOS=true;
                        }else{//ENTRA SI ES IGUAL A CERO
                            DATOS_PIVOTE.clear();
                            DATOS_PIVOTE.push_back(NUEVO);
                        }

                        ARCHIVO_REPORTE<<"digraph {"<<endl;


                            if(rep._ruta!=""){
                                vector<string> DIRECCION=OBTENER_DIRECCION(rep._ruta,"/");

                                ARCHIVO_REPORTE<<"tbl[ shape=plaintext label=<"<<endl;
                                ARCHIVO_REPORTE<<"<table border='0' cellborder='1' color='black' cellspacing='0'>\n";
                                ARCHIVO_REPORTE<<"<tr><td>Permisos</td><td>Owner</td><td>Grupo</td><td>Size</td><td>Fecha/Hora</td><td>Tipo</td><td>Name</td></tr>"<<endl;
                                    INICIO_REPORTE_LS(DIRECCION,0,0);
                                ARCHIVO_REPORTE<<"</table> >];"<<endl;
                            }else{
                                imprimirln("=== ERROR: NO ESPECIFICO NINGUNA RUTA ===");
                            }


                        DATOS_PIVOTE.clear();
                        if(CON_DATOS==true){
                            DATOS_PIVOTE.push_back(TEMPORAL_DATOS);
                        }

                   }
                   ARCHIVO_REPORTE<<"}"<<endl;
                   ARCHIVO_REPORTE.close();
                   //
                      if(VALIDA_TIPO_GRAPHIZ(rep._path)=="png"){
                            string ruta_compilar="dot -Tpng "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else
                       if(VALIDA_TIPO_GRAPHIZ(rep._path)=="pdf"){
                            string ruta_compilar="dot -Tpdf "+OBTENER_PATH_DOT(rep._path)+" -o "+rep._path;
                            system(ruta_compilar.c_str());
                       }else{
                        imprimirln("==== ERROR: NO SE RECONOCE LA EXTENSION PARA EL REPORTE SOLO SE PERMITE .PDF Y .PNG ====");
                       }
                ////---------------------------------------------------
            }else{
                imprimirln("=== ERROR: NO SE PUEDE UTILIZAR LA PARTION PARA REP INODE ====");
            }

        }
        ////FIN CODIGO
    }else{//SE REALIZARA SOBRE EL DISCO ENTERO
        imprimirln("=== ERROR: NO EXISTE PARTICION MONTADA CON ESE ID ===");
    }
////
}

#endif // VALIDACIONESCOMANDOS_H_INCLUDED
