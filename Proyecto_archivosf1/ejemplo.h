#ifndef EJEMPLO_H_INCLUDED
#define EJEMPLO_H_INCLUDED

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <math.h>
#include <fstream>
#include <ctime>
#include <cstring>
#include <vector>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
using namespace std;


vector<string> split(char letra1[],char* separado);
void imprimirln(std::string str);
void imprimirNumln(int str);
void imprimirNum(int str);
void imprimir(std::string str);
string convertirMinusculla(string palabra);
bool validarNumero(string entrada);
string eliminarComillas(string entrada);
string DATOSARCHIVO(string datos);
string obtener_texto_sin_espacios(string palabra);
bool validar_fit_mkdisk(string valor);
bool validar_unit_mkdisk(string palabra);
void crearCarpetas(vector<string> directorios);
string OBTENER_PATH_ARCHIVORAID(string path);
bool VALIDAR_EXTENSION_PATH(string path);
bool VALIDAR_UNIT_FDISK(string palabra);
bool VALIDAR_NUMEROS_NEGYPOST(string entrada);
string OBTENER_PATH_DOT(string path);
string VALIDA_TIPO_GRAPHIZ(string path);
bool VALIDAR_SINEXTENSION_PATH(string path);
bool VALIDAR_EXTENSION_SINMENSAJE(string path);
//string character_tostring(char letra[]);

void imprimirln(std::string str){
    std::cout<<str<<std::endl;
}

void imprimirNumln(int str){
    cout<<str<<endl;
}

void imprimirNum(int str){
    cout<<str;
}


void imprimir(std::string str){
    std::cout<<str;
}

vector<string> split(char letra1[],char* separado){
    vector<string> retorno;
    char* point;
    point =strtok(letra1,separado);

    while(point!=NULL){
        //valores[contador]=(string)point;
        retorno.push_back((string)point);
        point=strtok(NULL,separado);
    }

    return retorno;
}

string convertirMinusculla(string palabra){

    string respuesta="";

    for(size_t i=0; i<palabra.size(); i++){

       // if(palabra[i]!=""){
            char caracter=tolower(palabra[i]);
            respuesta+=caracter;
      //  }

    }

    return respuesta;
}

string obtener_texto_sin_espacios(string palabra){
    string respuesta;

    for(int i=0; i<palabra.size(); i++){
        if(palabra[i]!=32){
            respuesta+=palabra[i];
        }
    }
    return respuesta;
}

string eliminarComillas(string entrada){
    string concatena="";
    int contador=0;

 /*   for(int i=0; i<entrada.size(); i++){

        if(entrada[entrada.size()-1]==32){//VERIFICA SI EL ULTIMO ELEMENTO ES ESPACIO VACIO

            if(i!=entrada.size()-1){
                if(entrada[i]!=34 && contador!=2){
                    concatena+=entrada[i];
                }else{
                    contador++;
                }
            }

        }else{

           if(entrada[i]!=34 && contador!=2){
                concatena+=entrada[i];
            }else{
                contador++;
            }

        }
    }*/

    for(int i=0; i<entrada.size(); i++){

        if(entrada[i]==34){
            contador++;
        }

        if((contador>0) && (contador<2)){

            if(entrada[i]!=34){

                concatena+=entrada[i];

            }

        }else{

            if(entrada[i]==32){
               //entra aqui si encontro espacios
            }else if(entrada[i]!=34){//entra si lo que viene no es comilla
                concatena+=entrada[i];
            }

        }

    }


    return concatena;
}

bool validarNumero(string entrada){
    bool validar=true;

    for(int i=0; i<entrada.size(); i++){
        if(entrada[i]<=47 || entrada[i]>=58){
            validar=false;
            break;
        }
    }
    return validar;
}

bool validar_fit_mkdisk(string palabra){
    string valor=convertirMinusculla(obtener_texto_sin_espacios(palabra));
    bool retorno=false;

    if((valor=="bf") || (valor=="ff") || (valor=="wf") ){
        retorno=true;
    }

    return retorno;
}

bool validar_unit_mkdisk(string palabra){
    string valor=convertirMinusculla(obtener_texto_sin_espacios(palabra));
    bool retorno=false;

    if((valor=="k") || (valor=="m")){
        retorno=true;
    }
    return retorno;
}

/*
string character_tostring(char letra[]){
    int i=0;
    string cadena;

    while(letra[i]!=NULL){
        cadena+=letra[i];
        i++;
    }
    return cadena;
}*/

string DATOSARCHIVO(string datos){
    string concatenar="";

    for(int i=0; i<datos.size(); i++){

        if(datos[i]!=10){
            concatenar+=datos[i];
        }else{
            concatenar+="\n";
        }
    }

    return concatenar;
}

void crearCarpetas(vector<string> directorios){
    string separador="/";
  /*  if(opendir("/home")){
    imprimirln("la carpeta existe");
    }else{
    imprimirln("la carpeta no existe");
    }*/
    //imprimirNumln(mkdir("/home/EJEMPLOS2",0777));
    for(int i=0; i<directorios.size(); i++){//      ESTE FOR VALIDA LA RUTA Y EN CASO DE QUE NO EXISTAN LAS CARPETAS LA CREA
        //imprimirln(directorios[i]);
        if(i!=directorios.size()-1){
            separador+=directorios[i];  //VA RRECORIENDO CADA CARPETA PARA CREARLA EN CASO DE QUE NO EXISTA
            if(!opendir(separador.c_str())){    //VERFICAR SI LA CARPETA NO EXISTE
               if(mkdir(separador.c_str(),ACCESSPERMS)==0){// CREA LA CARPETA
                    imprimirln("++++CARPETA CREADA EXITOSAMENTE++++");
                }else{
                    imprimirln("===ERROR: CARPETA NO CREADA EXITOSAMENTE===");
                }

            }
            //imprimirln(separador);//DE PRUEBA PARA VER SI ESTA OBTENIENDO BIEN LAS RUTAS POR SEPARADO
        }
        separador+="/";
    }
}

string OBTENER_PATH_ARCHIVORAID(string path){
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> directorio=split(temporal,"/");

    char temporalPunto[directorio[directorio.size()-1].size()];
    strcpy(temporalPunto,directorio[directorio.size()-1].c_str());
    vector<string> directorioRaid=split(temporalPunto,".");

    string nuevoNombre;
    string concatena="/";
    for(int i=0; i<directorio.size(); i++){
        //imprimirln(directorio[i]);
        if(i!=directorio.size()-1){
            concatena+=directorio[i];
            concatena+="/";
        }else{
            nuevoNombre=concatena+directorioRaid[0]+"_raid."+directorioRaid[1];
        }
        //imprimirln(concatena);
    }
    //imprimirln(nuevoNombre);

    return nuevoNombre;
}


bool VALIDAR_EXTENSION_PATH(string path){
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> extension=split(temporal,".");
    bool respuesta=false;

    if(extension.size()>1){
        respuesta=true;
    }else{
        imprimirln("===ERROR: LA RUTA EXPECIFICADA NO CUENTA CON UN ARHIVO CON EXTENSION===");
    }
    return respuesta;
}

bool VALIDAR_SINEXTENSION_PATH(string path){
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> extension=split(temporal,".");
    bool respuesta=false;

    if(extension.size()>1){
        respuesta=true;
        imprimirln("==== ERROR: NO DEBEN VENIR ARCHIVOS EN LA RUTA DE CREACION DE DIRECTORIOS ====");
    }
    return respuesta;
}

bool VALIDAR_EXTENSION_SINMENSAJE(string path){
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> extension=split(temporal,".");
    bool respuesta=false;

    if(extension.size()>1){
        respuesta=true;
    }
    return respuesta;
}

bool VALIDAR_UNIT_FDISK(string palabra){
    string valor=obtener_texto_sin_espacios(convertirMinusculla(palabra));
    bool retornar=false;

    if((valor=="b") || (valor=="k") || (valor=="m")){
        retornar=true;
    }
    return retornar;
}

bool VALIDAR_TYPE_FDISK(string palabra){
    string valor=convertirMinusculla(obtener_texto_sin_espacios(palabra));
    bool respuesta=false;

    if((valor=="p") || (valor=="e")  || (valor=="l")){
        respuesta=true;
    }
    return respuesta;
}

bool VALIDAR_DELETE_FDISK(string palabra){
    string valor=obtener_texto_sin_espacios(convertirMinusculla(palabra));
    bool respuesta=false;

    if((valor=="fast") || (valor=="full")){
        respuesta=true;
    }

    return respuesta;
}

bool VALIDAR_NUMEROS_NEGYPOST(string entrada){
    bool validar=true;

    for(int i=0; i<entrada.size(); i++){

        if(entrada[i]==45){

        }else
        if(entrada[i]<=47 || entrada[i]>=58){
            validar=false;
            break;
        }
    }
    return validar;
}

string OBTENER_PATH_DOT(string path){
/////
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> directorio=split(temporal,"/");

    char temporalPunto[directorio[directorio.size()-1].size()];
    strcpy(temporalPunto,directorio[directorio.size()-1].c_str());
    vector<string> NUEVODIRECTORIO=split(temporalPunto,".");

    string nuevoNombre;
    string concatena="/";
    for(int i=0; i<directorio.size(); i++){
        //imprimirln(directorio[i]);
        if(i!=directorio.size()-1){
            concatena+=directorio[i];
            concatena+="/";
        }else{
            nuevoNombre=concatena+NUEVODIRECTORIO[0]+".dot";
        }
        //imprimirln(concatena);
    }
    //imprimirln(nuevoNombre);

    return nuevoNombre;
/////
}

string VALIDA_TIPO_GRAPHIZ(string path){
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> directorio=split(temporal,"/");

    char temporalPunto[directorio[directorio.size()-1].size()];
    strcpy(temporalPunto,directorio[directorio.size()-1].c_str());
    vector<string> VALOR_FINAL=split(temporalPunto,".");

    return VALOR_FINAL[1];
}


#endif // EJEMPLO_H_INCLUDED
