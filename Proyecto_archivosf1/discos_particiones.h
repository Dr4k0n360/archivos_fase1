#ifndef DISCOS_PARTICIONES_H_INCLUDED
#define DISCOS_PARTICIONES_H_INCLUDED

#include "ejemplo.h"
#include "bloques_archivos.h"
#include <time.h>

using namespace std;


//ESTRUCTURAS PARA EL DISCO Y LAS PARTICIONES
struct PARTITION_MBR_DISK{
    char part_status;
    char part_type;
    char part_fit;
    int part_start;
    int part_size;
    char part_name[16];
};

struct PARTITION_EBR_DISK{
    char part_status;
    char part_fit;
    int part_start;
    int part_size;
    int part_next;
    char part_name[16];
};

struct PARAMETROS_EXTRAS{

};

struct MBRD_DISK{
    int mbr_tamano;
    time_t mbr_fecha_creacion;
    int mbr_disk_signature;
    char mbr_fit;
    PARTITION_MBR_DISK mbr_partition[4];             //esto es para las particiones primarias o Extendidas
    PARTITION_EBR_DISK mbr_partitionEXT[20];          //esto es para las particiones extendidas indican donde estan las particiones logicas
};

struct CLAVE{
    string _path;
    string _llave;
};

struct REGISTRO{
    string _path;
    string _llave;
    string _nombreParticion;
};

vector<REGISTRO> MONTAR_PARTICION;//EN ESTE SE MONTARA LA PARTICION
vector<CLAVE> VALORES_CLAVE;
string VALORES[26]={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};

void CREAR_ARCHIVO_DISCO(string ruta, int tamano);
void GUARDAR_MBR_EN_DISCO(string path,struct MBRD_DISK mbr);
bool EXISTE_DISCO_O_ARCHIVO(string path);
struct MBRD_DISK OBTENER_MBR_DEL_DISCO(string path);
bool DISCO_EN_BLANCO_PARTICIONES_PYE(struct MBRD_DISK disco);
int OBTENER_TAMANIO_PARTITION(int _size, string unit);
bool EXISTEN_PARTCIONES_EXTENDIDAS(struct MBRD_DISK disk);
int CANTIDAD_PARTICIONES_PRIMARIAS(struct MBRD_DISK dir);
void MOSTRAR_INFORMACION_DISCO(struct MBRD_DISK disco);
bool EXISTE_PARTICION_EN_DISCO(struct MBRD_DISK disco,string nombre);
int OBTENER_INDICE_PARTITION_EXTENDIDA(struct MBRD_DISK disco);
bool PARTICIONES_LOGICAS_VACIAS(struct MBRD_DISK disco);
int CANTIDAD_PARTICIONES_LOGICAS(struct MBRD_DISK disco);
bool EXISTE_PARTICION_EN_EXTENDIDA(struct MBRD_DISK disco, string nombre);
int OBTENER_INDICE_PARTICION_LOGICA(struct MBRD_DISK disco,string nombre);
int OBTENER_INDICE_PARTICION_EXTENDIDAYPRIMARA(struct MBRD_DISK disco, string nombre);
void ELIMINACION_FULL(int inicio, int tamanio, string ruta);
bool EXISTE_LLAVE_PATH(string path);
bool EXISTE_LLAVE(string llave);
bool EXISTE_LLAVE_REGISTRO(string llaveCompleta);
string RETORNAR_LLAVE_PATH(string path);
void MOSTRAR_INF_MONTAJES();
bool EXISTE_PARTICION_MONTADA(string nombre);
vector<REGISTRO> ELIMINAR_MONTAJE(string id);
vector<REGISTRO> ELIMINAR_MONTAJE_ELIMINACION_PARTICION(string nombre);
bool VALIDAR_NAME_REP(string campo);
struct REGISTRO OBTENER_DATOS_REGISTRO_PMONTADA(string id);
string OBTENER_VALOR_REPORTE_FIT_DISK(char valor);
string RECUPERAR_TEXT_ARCHIVOCOMPU(string path);
string RECUPERAR_TEXT_SIZE(string cant);

void CREAR_ARCHIVO_DISCO(string ruta, int tamano){

    if(ruta!=""){
        FILE *archivo=fopen(ruta.c_str(),"w");

        char buffer[1024];

        for(int i=0; i<1024; i++){
            buffer[i]=(char)*"0";
        }

        for(int i=0; i<tamano; i++){
            fwrite(&buffer,1024,1,archivo);
        }
        fclose(archivo);

        imprimirln("++++ARCHIVO CREADO/MODIFICADO EXITOSAMENTE++++");
    }

}

void ELIMINACION_FULL(int inicio, int tamanio, string ruta){

    FILE *archivo=fopen(ruta.c_str(),"rb+");

    if(tamanio>1024){//SI EL TAMAÑO ES MAYOR A 1024 (1K)

        int REPETICIONES=(tamanio/1024);
        char buffer[1024];
        for(int i=0; i<1024; i++){
            buffer[i]=(char)*"0";
        }
        fseek(archivo,inicio,SEEK_SET);//nos ubicamos en la posicion donde inicia la particion contando a partir del inicio del archivo
        for(int i=0; i<REPETICIONES; i++){
           fwrite(&buffer,1024,1,archivo);
        }

    }else{//ENTRARIA AQUI SI TAMANIO ES MENOR A (1K) POR LO TANTOS SON BYTES

        char buffer[tamanio];
        for(int i=0; i<1024; i++){
            buffer[i]=(char)*"0";
        }
        fseek(archivo,inicio,SEEK_SET);
        fwrite(&buffer,tamanio,1,archivo);

    }
    fclose(archivo);
    imprimirln("++++++ ELIMINACION FULL REALIZADA CORRECTAMENTE +++++");

}

void GUARDAR_MBR_EN_DISCO(string path,struct MBRD_DISK mbr){

    FILE *archivo=fopen(path.c_str(),"rb+");//ABRE EL ARCHIVO EN MODO BINARIO PARA ACTUALIZACION LECTURA/ESCRITURA
    fseek(archivo,0,SEEK_SET);//SE SITUA EN LA POSICION 0 DEL ARCHIVO
    fwrite(&mbr, sizeof(MBRD_DISK),1,archivo);
    fclose(archivo);
}

bool EXISTE_DISCO_O_ARCHIVO(string path){
    bool valor=false;
    ifstream temporal(path.c_str(),ios::in);

    if(temporal.fail()){
        temporal.close();
        imprimirln("===ERROR: EL ARCHIVO NO EXISTE===");
    }else{
        temporal.close();
        valor=true;
    }

    return valor;
}

struct MBRD_DISK OBTENER_MBR_DEL_DISCO(string path){
    struct MBRD_DISK mbr;
  /*  FILE *archivo=fopen(path.c_str(),"rb+");
    fseek(archivo,0,SEEK_SET);
    fread(&mbr,sizeof(MBRD_DISK),1,archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
    }else{
        imprimirln("2626626 archivo abierto ");
    }

    fseek(ARCHIVO_GLOBAL,0,SEEK_SET);
    fread(&mbr,sizeof(MBRD_DISK),1,ARCHIVO_GLOBAL);
    fflush(ARCHIVO_GLOBAL);
    fclose(ARCHIVO_GLOBAL);
    ARCHIVO_GLOBAL=NULL;
    return mbr;
}

bool DISCO_EN_BLANCO_PARTICIONES_PYE(struct MBRD_DISK disco){
    bool respuesta=true;

    for(int i=0; (i<4) && (respuesta==true); i++){
        if(disco.mbr_partition[i].part_status==49){
            respuesta=false;
        }
    }

    return respuesta;
}

int OBTENER_TAMANIO_PARTITION(int _size, string unit){
    int respuesta=0;

    if(unit=="b"){
        respuesta=_size;
    }else if(unit=="k"){
        respuesta=_size*1024;
    }else if(unit=="m"){
        respuesta=_size*1024*1024;
    }
    return respuesta;
}

bool EXISTEN_PARTCIONES_EXTENDIDAS(struct MBRD_DISK disk){
    bool respuesta=false;
    //char* comparador=static_cast<char *>(malloc(5));
    //comparador=
    for(int i=0; (i<4) && (respuesta==false) ; i++){

        if(disk.mbr_partition[i].part_status!=0){
           // imprimirNumln(i);

            if(disk.mbr_partition[i].part_type==101){   //SIRVE PARA VALIDAR SI PART_TYPE==E DE EXTENDIDA
                //imprimirln("soy mula");
                respuesta=true;
            }

        }
    }


    return respuesta;
}

int CANTIDAD_PARTICIONES_PRIMARIAS(struct MBRD_DISK dir){
    int contador=0;

    /*for(int i=0; (i<4) && (dir.mbr_partition[i].part_status!=0); i++){//CUENTA UNICMANETE LAS PARTICIONES PRIMARIAS

        if(dir.mbr_partition[i].part_type==112){
            //imprimirln("entro al contador primarias");
            contador++;
        }

    }*/

    for(int i=0; i<4; i++){

        if(dir.mbr_partition[i].part_status==49){

            if(dir.mbr_partition[i].part_type==112){
                contador++;
            }

        }

    }
    return contador;
}

void MOSTRAR_INFORMACION_DISCO(struct MBRD_DISK disco){
    imprimirln("******MOSTRANDO INFORMACION DISCO******");
    cout<<ctime(&disco.mbr_fecha_creacion)<<endl;
    cout<<disco.mbr_fit<<endl;
    cout<<disco.mbr_disk_signature<<endl;
    imprimirNumln(disco.mbr_tamano);

    imprimirln("******MOSTRANDO INFORMACION DE PARTICIONES PRIM Y EXT****");

    for(int i=0; i<4; i++){

        if(disco.mbr_partition[i].part_status==49){

            imprimir("---------------PARTICION: ");
            imprimirNumln(i);

            cout<<disco.mbr_partition[i].part_name<<endl;
            cout<<disco.mbr_partition[i].part_fit<<endl;
            cout<<disco.mbr_partition[i].part_status<<endl;
            cout<<disco.mbr_partition[i].part_start<<endl;
            cout<<disco.mbr_partition[i].part_size<<endl;
            cout<<disco.mbr_partition[i].part_type<<endl;

        }

    }

     imprimirln("******MOSTRANDO INFORMACION DE PARTICIONES LOG****");

    for(int i=0; i<20; i++){

        if(disco.mbr_partitionEXT[i].part_status==49){

            imprimir("---------------PARTICION: ");
            imprimirNumln(i);

            cout<<disco.mbr_partitionEXT[i].part_name<<endl;
            cout<<disco.mbr_partitionEXT[i].part_fit<<endl;
            cout<<disco.mbr_partitionEXT[i].part_status<<endl;
            cout<<disco.mbr_partitionEXT[i].part_start<<endl;
            cout<<disco.mbr_partitionEXT[i].part_size<<endl;
            cout<<disco.mbr_partitionEXT[i].part_next<<endl;

        }

    }

}

bool EXISTE_PARTICION_EN_DISCO(struct MBRD_DISK disco,string nombre){
    bool respuesta=true;

    for(int k=0; (k<4) && (respuesta==true); k++){
        if(disco.mbr_partition[k].part_status==49){
            if(strcmp(disco.mbr_partition[k].part_name,nombre.c_str())==0){
                respuesta=false;
               // imprimirln("comprobo");
            }
        }
    }

    return respuesta;
}

int OBTENER_INDICE_PARTITION_EXTENDIDA(struct MBRD_DISK disco){
////
    int INDICE_PARTICION_EXTENDIDA=0;
    bool PARTICION_EXTENDIDA_ENCONTRADA=false;

    for(int i=0; (i<4) && (PARTICION_EXTENDIDA_ENCONTRADA==false); i++){//CON ESTE
        if(disco.mbr_partition[i].part_status==49){
            if(disco.mbr_partition[i].part_type==101){
                PARTICION_EXTENDIDA_ENCONTRADA=true;
                INDICE_PARTICION_EXTENDIDA=i;
                //imprimirln("particion extendida encontrada");
            }
        }
    }
    return INDICE_PARTICION_EXTENDIDA;
////
}

bool PARTICIONES_LOGICAS_VACIAS(struct MBRD_DISK disco){
    bool respuesta=true;

    for(int i=0;(i<20) && (respuesta==true); i++){

        if(disco.mbr_partitionEXT[i].part_status==49){
            respuesta=false;
            //imprimirln("comprobo una particion usada");
        }

    }

    return respuesta;
}

int CANTIDAD_PARTICIONES_LOGICAS(struct MBRD_DISK disco){
    int contador=0;
    for(int i=0; i<20; i++){

        if(disco.mbr_partitionEXT[i].part_status==49){
            contador++;
        }

    }
    return contador;
}

bool EXISTE_PARTICION_EN_EXTENDIDA(struct MBRD_DISK disco, string nombre){
    bool existe=true;

    for(int i=0; (i<20) && (existe==true); i++){
        if(disco.mbr_partitionEXT[i].part_status==49){
            if(strcmp(disco.mbr_partitionEXT[i].part_name,nombre.c_str())==0){
                existe=false;
            }
        }
    }
    return existe;
}

int OBTENER_INDICE_PARTICION_LOGICA(struct MBRD_DISK disco,string nombre){
    int INDICE_PARTICION=0;
    bool PARTICION_ENCONTRADA=false;

    for(int i=0; (i<20) && (PARTICION_ENCONTRADA==false);i++){

        if(disco.mbr_partitionEXT[i].part_status==49){

            if(strcmp(disco.mbr_partitionEXT[i].part_name,nombre.c_str())==0){
                PARTICION_ENCONTRADA=true;
                INDICE_PARTICION=i;
            }
        }
    }
    return INDICE_PARTICION;
}

int OBTENER_INDICE_PARTICION_EXTENDIDAYPRIMARA(struct MBRD_DISK disco, string nombre){
    int INDICE_PARTICION=0;
    bool PARTICION_ENCONTRADA=false;

    for(int i=0; (i<4) && (PARTICION_ENCONTRADA==false); i++){

        if(disco.mbr_partition[i].part_status==49){

            if(strcmp(disco.mbr_partition[i].part_name,nombre.c_str())==0){
                PARTICION_ENCONTRADA=true;
                INDICE_PARTICION=i;
            }
        }
    }
    return INDICE_PARTICION;
}

bool EXISTE_LLAVE_PATH(string path){
    bool respuesta=false;

    for(int i=0; (i<VALORES_CLAVE.size()) && (respuesta==false);i++){

        if(VALORES_CLAVE[i]._path==path){
            respuesta=true;
        }

    }
    return respuesta;
}

bool EXISTE_LLAVE(string llave){
    bool respuesta=false;

    for(int i=0; (i<VALORES_CLAVE.size()) && (respuesta==false);i++){
        if(VALORES_CLAVE[i]._llave==llave){
            respuesta=true;
        }
    }
    return respuesta;
}

bool EXISTE_LLAVE_REGISTRO(string llaveCompleta){
    bool respuesta=false;

    for(int i=0; (i<MONTAR_PARTICION.size()) && (respuesta==false);i++){
        if(MONTAR_PARTICION[i]._llave==llaveCompleta){
            respuesta=true;
        }
    }
    return respuesta;
}

string RETORNAR_LLAVE_PATH(string path){
    string respuesta="";
    for(int i=0; i<VALORES_CLAVE.size();i++){

        if(VALORES_CLAVE[i]._path==path){
            respuesta=VALORES_CLAVE[i]._llave;
        }

    }
    return respuesta;
}

bool EXISTE_PARTICION_MONTADA(string nombre){
    bool respuesta=false;

    for(int i=0; (i<MONTAR_PARTICION.size()) && (respuesta==false); i++){
        if(MONTAR_PARTICION[i]._nombreParticion==nombre){
            respuesta=true;
        }
    }
    return respuesta;
}

vector<REGISTRO> ELIMINAR_MONTAJE_ELIMINACION_PARTICION(string nombre){
    vector<REGISTRO> TEMPORAL;

    for(int i=0; i<MONTAR_PARTICION.size(); i++){
        if(MONTAR_PARTICION[i]._nombreParticion!=nombre){
            TEMPORAL.push_back(MONTAR_PARTICION[i]);
        }
    }
    return TEMPORAL;
}

vector<REGISTRO> ELIMINAR_MONTAJE(string id){
    vector<REGISTRO> TEMPORAL;

    for(int i=0; i<MONTAR_PARTICION.size(); i++){
        if(MONTAR_PARTICION[i]._llave!=id){
            TEMPORAL.push_back(MONTAR_PARTICION[i]);
        }
    }
    return TEMPORAL;
}

void MOSTRAR_INF_MONTAJES(){
    imprimirln("-------------------------------------- CLAVES ----------------------------------");

    for(int i=0; i<VALORES_CLAVE.size(); i++){
        imprimirln(VALORES_CLAVE[i]._path);
        imprimirln(VALORES_CLAVE[i]._llave);
        imprimirln("*******");
    }
    imprimirln("-------------------------------------- REGISTROS ----------------------------------");

    for(int i=0; i<MONTAR_PARTICION.size(); i++){
        imprimirln(MONTAR_PARTICION[i]._path);
        imprimirln(MONTAR_PARTICION[i]._llave);
        imprimirln(MONTAR_PARTICION[i]._nombreParticion);
        imprimirln("*******");
    }
}

bool VALIDAR_NAME_REP(string campo){
    bool respuesta=false;

    if((campo=="inode") || (campo=="journaling") || (campo=="block") || (campo=="bm_inode") || (campo=="bm_block") || (campo=="tree") || (campo=="sb") || (campo=="file") || (campo=="ls")){
        respuesta=true;
    }
    return respuesta;
}

struct REGISTRO OBTENER_DATOS_REGISTRO_PMONTADA(string id){
    struct REGISTRO temporal;
    bool encontrada=false;
    for(int i=0; (i<MONTAR_PARTICION.size()) && (encontrada==false); i++){
        if(MONTAR_PARTICION[i]._llave==id){
            temporal=MONTAR_PARTICION[i];
            encontrada=true;
        }
    }
    return temporal;
}


string OBTENER_VALOR_REPORTE_FIT_DISK(char valor){
    string respuesta;

    if(valor==98){
        respuesta="b";
    }else
    if(valor==102){
        respuesta="f";
    }else
    if(valor==119){
        respuesta="w";
    }
    return respuesta;
}

string RECUPERAR_TEXT_ARCHIVOCOMPU(string path){
    ifstream archivotext;
    string temporaltexto;
    string texto="";

    archivotext.open(path.c_str(),ios::in);

    if(archivotext.fail()){
        imprimirln("===ERROR: NO SE PUEDO ABRIR EL ARCHIVO ESPECIFICADO POR CONT===");
    }else{
        while(!archivotext.eof()){
            getline(archivotext,temporaltexto);
            texto+=temporaltexto;
        }
    }
    return texto;
}

string RECUPERAR_TEXT_SIZE(string cant){
    int cantidad=atoi(cant.c_str());
    int temporal=0;
    string texto="";
    for(int i=0; i<cantidad; i++){
        if(temporal!=10){
            texto+=to_string(temporal);
            temporal++;
        }else{
            temporal=0;
            texto+=to_string(temporal);
            temporal++;
        }
    }
    return texto;
}



#endif // DISCOS_PARTICIONES_H_INCLUDED
