#ifndef VALIDACIONES_COMANDOSBLOQUES_H_INCLUDED
#define VALIDACIONES_COMANDOSBLOQUES_H_INCLUDED

#include "ejemplo.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <dirent.h>
#include <time.h>
#include "discos_particiones.h"
#include "bloques_archivos.h"

bool VALIDACION_TYPE_MKFS(string parametro);
bool VALIDACION_FS_MKFS(string parametro);
int OBTENER_CANTIDAD_N_EXT2(int tamanioPartition);
struct PARTITION_MBR_DISK OBTENER_DATOS_PARTICION_PRIMARIA(struct MBRD_DISK disco,string nomparticion);
string OBTENER_NOMBRE_DISCO(string path);
void CREAR_ARCHIVO_AUXILIAR_BLOQUES(string nombreCarpeta,string nombreArchivo,int tamanioBloques);
void CREAR_ARCHIVO_JOURNALING(string nombreCarpeta,string nombreArchivo);
void ACTUALIZAR_CONTADOR_JOURNALING(int contador,string nombre);
struct PARTITION_EBR_DISK OBTENER_DATOS_PARTICION_LOGICA(struct MBRD_DISK disco, string nombreparticion);

void ESCRIBIR_SUPERBLOQUE(struct Super_Bloque SB,int inicio);
void ESCRIBIR_INODO(struct Tabla_Inodos INODO,int inicio_bitmap_inodos,int inicio_inodos,string path_disco,int cantidad_inodos);
void GUARDAR_INODO_EN_DISCO(struct Tabla_Inodos INODO,int posision_inicio,string path_disco);
struct Tabla_Inodos INODO_CARPETA_RAIZ(int inicio_tabla_inodo,string path);
void FORMATEO_FAST_FS(string path,int inicio_bitmap_inodos,int inicio_bitmap_bloques,int N);
void FORMATE_FULL_FS(string path, int inicio_inodos, int inicio_bloques, int N);
bool EXISTE_EN_BLOQUE(vector<string> N_C_A,int P_N_C_A,int T_A,int I_B,int I_P,int T_I, int N_B_A_I,int V_N,string path_disco);
bool EXISTE_EN_INODO(vector<string> N_C_A,int P_N_C_A,int I_I,int I_P,int V_N,string path_disco);
struct Tabla_Inodos RECUPERAR_INODO(int P_I_P,int I_I,int V_N, string path_disco);
bool EXISTE_CARPETA_ARCHIVO_EN_INODO(struct Tabla_Inodos INODO,string NOMBRE_C_A,int I_P, int I_I_P,int V_N, string path_disco);
struct B_Carpetas RECUPERAR_BLOQUE_CARPETA(int P_I_P, int I_B, int V_N, string path_disco);
struct B_Apuntadores RECUPERAR_BLOQUE_INDIRECTO(int P_I_P, int I_B, int V_N, string path_disco);
bool EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(string NOMBRE_C_A,int TIPO_APUNTADOR_INDIRECTO,int NIVEL_APUNTADOR,int I_P,int V_N,int I_B,string path_disco);
void INICIO_CREACION_CARPETAS_ARCHIVOS(vector<string> RUTA_PATH,int I_I, int I_P,int P_R_P,int P_I_P,int V_N,string path_disco,int U_ID,int G_ID);
void ACTUALIZAR_BITMAP_BLQUES(string path_disco,int POSICION_INICIO_BITMAP_BLQOUES,int FIN_BITMAP_BLOQUES);
void ACTUALIZAR_BITPMAP_ARCHIVO_EXTRA(string NOMBRE_ARCHIVO_RUTA,int POSICION);
int CREAR_INODO_ARCHIVO_USERSR(int I_P,int V_N,string path_disco,string path_archivo_extra,string CONTENIDO_ARCHIVO);
void GUARDAR_BLOQUE_ARCHIVOS(string path_disco,int POSICION,struct B_Archivos BLOQUE_ARCHIVO);
void GUARDAR_BLOQUE_CARPETAS(string path_disco, int posicion,struct B_Carpetas BLOQUE_CARPETA);
void GUARDAR_SUPERBLOQUE_DISCO(string path_disco, int posicion, struct Super_Bloque SUPERBLOQUE);
struct Super_Bloque RECUPERAR_SUPERBLOQUE_DISCO(string path_disco, int posicion);
void GUARDAR_STRUCT_JOURNALING(string path_jour,struct JOURNALING JOUR);
void GUARDAR_CONTADOR_ACTUALIZADO(string path_jour,int CONTADOR_act);
int RECUPERAR_CONTADOR_JOURNALING(string path_jounaling);
struct B_Archivos RECUPERAR_BLOQUE_ARCHIVO(int P_I_P, int I_A, int V_N, string path_disco);
void GUARDAR_BLOQUE_INDIRECTO(string path_disco,int posicion,struct B_Apuntadores BLOQUE_INDIRECTO);
void MOSTRAR_INFORMACION_JOURNALING(string path_journalingn);

bool VALIDACION_TYPE_MKFS(string parametro){
    bool validado=false;

    if((parametro=="fast") || (parametro=="full")){
        validado=true;
    }else{
        imprimirln("=== ERROR: INGRESO UN VALOR INCORRECTO PARA TYPE ===");
    }

    return validado;
}

bool VALIDACION_FS_MKFS(string parametro){
    bool validado=false;

    if((parametro=="2fs") || (parametro=="3fs")){
        validado=true;
    }else{
        imprimirln("=== ERROR: INGRESO UN VALOR INCORRECTO PARA FS ===");
    }
    return validado;
}

int OBTENER_CANTIDAD_N_EXT2(int tamanioPartition){

    int N=((tamanioPartition-sizeof(Super_Bloque))/((4)+(sizeof(Tabla_Inodos))+(3*(sizeof(B_Archivos)))));

    return N;
}

struct PARTITION_MBR_DISK OBTENER_DATOS_PARTICION_PRIMARIA(struct MBRD_DISK disco,string nomparticion){
    struct PARTITION_MBR_DISK temporal;
    bool encontrado=false;

    for(int i=0; (i<4) && (encontrado==false); i++){
        if(disco.mbr_partition[i].part_status==49){
            if(strcmp(disco.mbr_partition[i].part_name,nomparticion.c_str())==0){
                encontrado=true;
                temporal=disco.mbr_partition[i];
            }
        }
    }

    return temporal;
}

struct PARTITION_EBR_DISK OBTENER_DATOS_PARTICION_LOGICA(struct MBRD_DISK disco, string nombreparticion){
    struct PARTITION_EBR_DISK temporal;
    bool encontrado=false;

    for(int i=0; (i<20) && (encontrado==false); i++){
        if(disco.mbr_partitionEXT[i].part_status==49){
            if(strcmp(disco.mbr_partitionEXT[i].part_name,nombreparticion.c_str())==0){
                encontrado=true;
                temporal=disco.mbr_partitionEXT[i];
            }
        }
    }
    return temporal;
}

string OBTENER_NOMBRE_DISCO(string path){
    //OBTENEMOS CADA NOMBRE DE LAS CARPETAS AL SEPARARLAS POR /
    char temporal[path.size()+1];
    strcpy(temporal,path.c_str());
    vector<string> carpetas=split(temporal,"/");
    //OBTENEMOS EL NOMBRE DEL DISCO AL SEPARARLA CON EL .
    char temporal2[carpetas[carpetas.size()-1].size()+1];
    strcpy(temporal2,carpetas[carpetas.size()-1].c_str());
    vector<string> nombre_disco=split(temporal2,".");

    return nombre_disco[0];
}

void CREAR_ARCHIVO_AUXILIAR_BLOQUES(string nombreCarpeta,string nombreArchivo,int tamanioBloques){
    //OBTENEMOS LA CANTIDAD DE VECES QUE SE ESCRIBIRA EN EL ARCHIVO
    int cantidad=((tamanioBloques/1024)+1);
    char buffer[1024];
    for(int i=0; i<1024; i++){
        buffer[i]=(char)*"0";
    }
    string nombre=nombreCarpeta+"/"+nombreArchivo+".bin";
   // imprimirln(nombre);
    if(mkdir(nombreCarpeta.c_str(),ACCESSPERMS)==0){
       // FILE *archivo=fopen(nombre.c_str(),"w");
       if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(nombre.c_str(),"w");
       }else{
        imprimirln("548464 archivo abierto");
       }
                for(int i=0; i<cantidad; i++){
                   // fwrite(&buffer,1024,1,archivo);
                   fwrite(&buffer,1024,1,ARCHIVO_GLOBAL);
                }
        /*fflush(archivo);
        fclose(archivo);*/
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }
}

void CREAR_ARCHIVO_JOURNALING(string nombreCarpeta,string nombreArchivo){

    char buffer[1024];
    for(int i=0; i<1024; i++){
        buffer[i]=(char)*"0";
    }

    string nombre=nombreCarpeta+"/"+"jour_"+nombreArchivo+".bin";
    //imprimirln("entrara a escribri file");
  //  FILE *archivo=fopen(nombre.c_str(),"w");
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(nombre.c_str(),"w");
    }else{
        imprimirln("46484 error archivo abierto");
    }
    for(int i=0; i<(20*1024); i++){
        //fwrite(&buffer,1024,1,archivo);
        fwrite(&buffer,1024,1,ARCHIVO_GLOBAL);
    }
   /* fflush(archivo);
    fclose(archivo);*/
    fflush(ARCHIVO_GLOBAL);
    fclose(ARCHIVO_GLOBAL);
    ARCHIVO_GLOBAL=NULL;
    ACTUALIZAR_CONTADOR_JOURNALING(0,nombre);
}

void ACTUALIZAR_CONTADOR_JOURNALING(int cont,string nombre){
////
//AQUI INICIAMOS EL CONTADOR DEL JOURNALING EN 0
    struct CONTADOR_JOURNALING contador;
    contador.CONTADOR=cont;
  /*  FILE *jou=fopen(nombre.c_str(),"rb+");
    fseek(jou,0,SEEK_SET);
    fwrite(&contador,sizeof(CONTADOR_JOURNALING),1,jou);
    fflush(jou);
    fclose(jou);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(nombre.c_str(),"rb+");
    }else{
        imprimirln("8484 error archivo abierto");
    }
    fseek(ARCHIVO_GLOBAL,0,SEEK_SET);
    fwrite(&contador,sizeof(CONTADOR_JOURNALING),1,ARCHIVO_GLOBAL);
    fflush(ARCHIVO_GLOBAL);
    fclose(ARCHIVO_GLOBAL);
    ARCHIVO_GLOBAL=NULL;
////
}

void ESCRIBIR_INODO(struct Tabla_Inodos INODO,int inicio_bitmap_inodos,int inicio_inodos,string path_disco,int cantidad_inodos){
  //  imprimirln("entro a metodo ESCRIBIR_INODO");
    int ubicacion_tabla_inodo=inicio_inodos;
    bool posicionEncontrada=false;
 //   imprimirln(path_disco);
    FILE *archivo=fopen(path_disco.c_str(),"rb+");
    for(int i=inicio_bitmap_inodos; (i<=(inicio_bitmap_inodos+cantidad_inodos)) && (posicionEncontrada==false); i++){

        char temporal;
        fseek(archivo,i,SEEK_SET);
        fread(&temporal,1,1,archivo);
  //      imprimir("buscando posicion i: ");
  //      imprimirNumln(i);
        if(temporal==48){
            posicionEncontrada=true;
            char nuevo=49;
            fseek(archivo,i,SEEK_SET);
            fwrite(&nuevo,1,1,archivo);
            fflush(archivo);
            fclose(archivo);
            //SE GUARDA EN EL ARCHIVO NORMAL Y EN EL RAID
            GUARDAR_INODO_EN_DISCO(INODO,ubicacion_tabla_inodo,path_disco);
            GUARDAR_INODO_EN_DISCO(INODO,ubicacion_tabla_inodo,OBTENER_PATH_ARCHIVORAID(path_disco));
   //         imprimirln("posicion encontrada bitmap");
   //         imprimirNumln(i);
   //         imprimirNumln(ubicacion_tabla_inodo);
        }else{
            ubicacion_tabla_inodo+=sizeof(Tabla_Inodos);
        }
    }
    fflush(archivo);
    fclose(archivo);
}

void GUARDAR_INODO_EN_DISCO(struct Tabla_Inodos INODO,int posision_inicio,string path_disco){
    /*FILE *archivo=fopen(path_disco.c_str(),"rb+");

    if(archivo!=NULL){
        imprimirln("OBTUVO UN VALOR AL ARCHIVO PARA GUARDAR");
    }else{
        imprimirln("OBTUVO UN VALOR NULO AL ARCHIVO PARA GUARDAR");
    }
    imprimirln("ESTE COMO PRUEBA DEL ERROR");
    imprimirNumln(ftell(archivo));
    int c=fseek(archivo,posision_inicio,SEEK_SET);
    imprimirNumln(c);
    if(c==-1){
        imprimirln("OCURRIO ERROR CON FSEEK");
    }

    imprimirln("VA A ESCRIBIR EL INODO..");
    fwrite(&INODO,sizeof(Tabla_Inodos),1,archivo);
    fflush(archivo);
    imprimirln("VA A CERRAR EL INODO");
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
   //     imprimirln("ba a vuscar su posicin POR SPUEST QUE OBTUVO UN VALOR PARA GUARDAR");
        fseek(ARCHIVO_GLOBAL,posision_inicio,SEEK_SET);
        fwrite(&INODO,sizeof(Tabla_Inodos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;

    }else{
        imprimirln("3333333333333333333333333333  error el archivo se encuentra abierto 3333333333333333333333");
    }
}

struct Tabla_Inodos INODO_CARPETA_RAIZ(int inicio_tabla_inodo,string path){
    struct Tabla_Inodos temporal;
    FILE *archivo=fopen(path.c_str(),"rb+");
    fseek(archivo,inicio_tabla_inodo,SEEK_SET);
    fread(&temporal,sizeof(Tabla_Inodos),1,archivo);
    fflush(archivo);
    fclose(archivo);
    return temporal;
}

void FORMATEO_FAST_FS(string path,int inicio_bitmap_inodos,int inicio_bitmap_bloques,int N){
    char valorEscribir=48;
    //PARA EL FORMATEO DEL BITMAP INODOS
    /*FILE *archivo=fopen(path.c_str(),"rb+");
    fseek(archivo,inicio_bitmap_inodos,SEEK_SET);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,inicio_bitmap_inodos,SEEK_SET);
    }else{
        imprimirln("9999999 archivo abierrto 9999");
    }
    for(int i=inicio_bitmap_inodos; i<(inicio_bitmap_inodos+N);i++){
       // fwrite(&valorEscribir,1,1,archivo);
       fwrite(&valorEscribir,1,1,ARCHIVO_GLOBAL);
    }
    //PARA EL FORMATEO DEL BITMAP BLOQUES
   // fseek(archivo,inicio_bitmap_bloques,SEEK_SET);
    fseek(ARCHIVO_GLOBAL,inicio_bitmap_bloques,SEEK_SET);
    for(int i=inicio_bitmap_bloques; i<(inicio_bitmap_bloques+(3*N));i++){
       // fwrite(&valorEscribir,1,1,archivo);
       fwrite(&valorEscribir,1,1,ARCHIVO_GLOBAL);
    }
    /*fflush(archivo);
    fclose(archivo);*/
    fflush(ARCHIVO_GLOBAL);
    fclose(ARCHIVO_GLOBAL);
    ARCHIVO_GLOBAL=NULL;
    //imprimirln("++++ FORMATEO FAST REALIZADO EXITOSAMENTE ++++");
}

void FORMATE_FULL_FS(string path, int inicio_inodos, int inicio_bloques, int N){
    char escribirInodos[112];
    char escribirBloques[64];

    for(int i=0; i<112; i++){
        escribirInodos[i]=48;
    }
    for(int i=0; i<(64); i++){
        escribirBloques[i]=48;
    }

 //   FILE *archivo=fopen(path.c_str(),"rb+");
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
    }else{
        imprimirln("9999 archivo abierto 999");
    }
    //PARA LOS INODOS
    //fseek(archivo,inicio_inodos,SEEK_SET);
    fseek(ARCHIVO_GLOBAL,inicio_inodos,SEEK_SET);
    for(int i=inicio_inodos; i<(inicio_bloques); i+=sizeof(Tabla_Inodos)){
    //    fwrite(&escribirInodos,sizeof(Tabla_Inodos),1,archivo);
        fwrite(&escribirInodos,sizeof(Tabla_Inodos),1,ARCHIVO_GLOBAL);
    }
    //PARA LOS BLOQUES
//    fseek(archivo,inicio_bloques,SEEK_SET);
        fseek(ARCHIVO_GLOBAL,inicio_bloques,SEEK_SET);
    for(int i=inicio_bloques; i<((inicio_bloques)+(3*N*sizeof(B_Archivos)));i+=(sizeof(B_Archivos))){
       // fwrite(&escribirBloques,sizeof(B_Archivos),1,archivo);
       fwrite(&escribirBloques,sizeof(B_Archivos),1,ARCHIVO_GLOBAL);
    }
   /* fflush(archivo);
    fclose(archivo);*/
    fflush(ARCHIVO_GLOBAL);
    fclose(ARCHIVO_GLOBAL);
    ARCHIVO_GLOBAL=NULL;
}
//============================== INICIO CREACION METODO ============================
//ESTE METODO INCIA LA CREACION DE LAS CARPETAS Y/O ARCHIVOS
//I_B=IDENTIFICADOR INODO
//I_P=INODO PADRES
//P_R_P=POSICION RUTA PATH
//P_I_P=POSICION INICIAL PARTICION
//I_I=IDENTIFICADOR INODO;
//U_ID=ID DEL USUARIO QUE QUIERE CREAR EL ARCHIVO/CARPETA
//G_ID=ID DEL GRUPO AL QUE PERTENECE EL USUARIO
void INICIO_CREACION_CARPETAS_ARCHIVOS(vector<string> RUTA_PATH,int I_I, int I_P,int P_R_P,int P_I_P,int V_N,string path_disco,int U_ID,int G_ID){
    //RECUPERAR EL INODO
    struct Tabla_Inodos TEMPORAL_BUSCAR_INODO;
    TEMPORAL_BUSCAR_INODO=RECUPERAR_INODO(P_I_P,I_I,V_N,path_disco);
    //CON ESTO VALIDAMOS QUE EL INODO RECUPERADO NO SEA UN INODO QUE NO HALLA SIDO CREADO
    if((TEMPORAL_BUSCAR_INODO.i_type==0) || (TEMPORAL_BUSCAR_INODO.i_type==1)){//
        //VERIFICA SI LA CARPETA/ARCHIVO EXISTE EN DICHO INODO
        if(EXISTE_CARPETA_ARCHIVO_EN_INODO(TEMPORAL_BUSCAR_INODO,RUTA_PATH[P_R_P],P_I_P,0,V_N,path_disco)==true){
            //AQUI SE DEBE LLAMAR RECURSIVAMENTE A ESTE MISMO METODO
    //        imprimirln("lo encontro");

        }else{
            //PARA CREAR UNA CARPETA/ARCHIVO EN EL INODO DEVE VERIFICARSE LOS PERMISOS
            //Y QUE USUARIO INTENTA ACCEDER A LA CREACION DEL ARCHIVO O/CARPETA
            //POR AHORA NO SE VALIDARAN PERMISOS SOLO SE VERIFICARA QUE SE PUEDAN CREAR ARCHIVOS
      //      imprimirln("algo no salio bien");

        }

    }else{
        imprimirln("=== ERROR: NO SE PUDO RECUPERAR INODO PARA CREACION/VERIFICACION DE CARPETAS/ARCHIVO ===");
    }


}

//========================CREA UN BLOQUE DE ARCHIVOS, APUNTADORES, O DE CARPETAS ================
//I_P=INICIO PARTICION
//I_I_P=
bool EXISTE_CARPETA_ARCHIVO_EN_INODO(struct Tabla_Inodos INODO,string NOMBRE_C_A,int I_P, int I_I_P,int V_N, string path_disco){
//    imprimirln("verificar si existe archivo en inodo");
    bool existe=false;
    //RECORREMOS EL INODO PARA BUSCAR SI EXISTE CARPETA/ARCHIVO
    for(int i=0; (i<16) && (existe==false); i++){
        //SI EL APUNTADOR ES DIFERENTE A -1
        if(INODO.i_block[i]!=-1){
            //SI SON APUNTADORES INDIRECTOS
            if(i==13){
                existe=EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(NOMBRE_C_A,i,1,I_P,V_N,INODO.i_block[i],path_disco);
            }else
            if(i==14){
                existe=EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(NOMBRE_C_A,i,2,I_P,V_N,INODO.i_block[i],path_disco);
            }else
            if(i==15){
                existe=EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(NOMBRE_C_A,i,3,I_P,V_N,INODO.i_block[i],path_disco);
            }else{//SI ES APUNTADOR DIRECTO
          //      cout<<"entro a recuperar bloque carpeta "<<INODO.i_block[i]<<endl;
                struct B_Carpetas BLOQUE_CARPETA;
                BLOQUE_CARPETA=RECUPERAR_BLOQUE_CARPETA(I_P,INODO.i_block[i],V_N,path_disco);
                //VAMOS A RECORRER EL BLQOUE DE CARPETAS Y VERIFICAR SI EXISTE
                for(int j=0; (j<4) && (existe==false); j++){
                    if(BLOQUE_CARPETA.b_content[j].b_inodo!=-1){
            //            cout<<BLOQUE_CARPETA.b_content[j].b_name<<endl;
           //             cout<<NOMBRE_C_A<<endl;
                        //VERIFICA QUE EL NOMBRE DE ESTE APUNTADOR SEA IGUAL AL DE LA CARPETA
                        if(strcmp(BLOQUE_CARPETA.b_content[j].b_name,NOMBRE_C_A.c_str())==0){
                            existe=true;
                        }
                    }
                }
            }
        }
    }

    return existe;
}

//I_P=INICIO PARTICION
//I_V=VALOR DE N
//I_B_I=IDENTIFICADOR BLOQUE
bool EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(string NOMBRE_C_A,int TIPO_APUNTADOR_INDIRECTO,int NIVEL_APUNTADOR,int I_P,int V_N,int I_B,string path_disco){
    bool existe=false;
//    imprimirln("verficara en apuntador indirecto");
    struct B_Apuntadores BLOQUE_INDIRECTO;
    BLOQUE_INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(I_P,I_B,V_N,path_disco);
    NIVEL_APUNTADOR--;
   // imprimirln("ENTRO A BUSCAR EN APUNTADOR INDIRECTO DE METODO APUNTADORES_INDIRECTOS");
    if((TIPO_APUNTADOR_INDIRECTO==13) && (NIVEL_APUNTADOR==0)){
        //  RECORRO TODOS LOS APUNTADORES DEL BLQOUE APUNTADOR INDIRECTO
    //    imprimirln("VA A RECORRER EL APUNTADOR INDIRECTO RECUPERADO");
        for(int i=0; (i<13) && (existe==false); i++){
    //        imprimirln("VALOR DE APUNTADOR INDIRECTO: ");
    //        imprimirNumln(BLOQUE_INDIRECTO.b_pointers[i]);
    //        imprimirNumln(i);
            if(BLOQUE_INDIRECTO.b_pointers[i]!=-1){
                    //RECUPERO EL BLOQUE CARPETA AL QUE APUNTA ESTE APUNTADOR INDIRECTO
                        struct B_Carpetas BLOQUE_CARPETA;
                        BLOQUE_CARPETA=RECUPERAR_BLOQUE_CARPETA(I_P,BLOQUE_INDIRECTO.b_pointers[i],V_N,path_disco);
                        //RECORRO TODOS LOS APUNTADORES DEL BLOQUE CARPETA
          //              imprimirln("VA A RECORRER EL BLQOUE CARPETA DEL APUNTADOR INDIRECTO");
                        for(int j=0; (j<4) && (existe==false); j++){
                            if(BLOQUE_CARPETA.b_content[j].b_inodo!=-1){
                         //       cout<<BLOQUE_CARPETA.b_content[j].b_name<<endl;
                         //       imprimirln(NOMBRE_C_A);
                                if(strcmp(BLOQUE_CARPETA.b_content[j].b_name,NOMBRE_C_A.c_str())==0){
                                    existe=true;
                                }
                            }
                        }

                    ////
               // }
            }
        }
    }else
    if((TIPO_APUNTADOR_INDIRECTO==14) && (NIVEL_APUNTADOR==1)){
        if(BLOQUE_INDIRECTO.b_pointers[13]!=-1){
            existe=EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(NOMBRE_C_A,13,1,I_P,V_N,BLOQUE_INDIRECTO.b_pointers[13],path_disco);
        }
    }else
    if((TIPO_APUNTADOR_INDIRECTO==15) && (NIVEL_APUNTADOR==2)){
        if(BLOQUE_INDIRECTO.b_pointers[13]!=-1){
            existe=EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(NOMBRE_C_A,13,2,I_P,V_N,BLOQUE_INDIRECTO.b_pointers[13],path_disco);
        }
    }else
    if((TIPO_APUNTADOR_INDIRECTO==13) && (NIVEL_APUNTADOR!=0)){
        if(BLOQUE_INDIRECTO.b_pointers[13]!=-1){
            existe=EXISTE_CARPETA_ARCHIVO_APUNTADORES_INDIRECTOS(NOMBRE_C_A,13,1,I_P,V_N,BLOQUE_INDIRECTO.b_pointers[13],path_disco);
        }
    }
    return existe;
}

struct Tabla_Inodos RECUPERAR_INODO(int P_I_P,int I_I,int V_N, string path_disco){
    ////
    struct Tabla_Inodos TEMPORAL_BUSCAR_INODO;
    int POSICION_INICIAL_INODO=P_I_P+sizeof(Super_Bloque)+(4*V_N)+(I_I*sizeof(Tabla_Inodos));
    /*FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,POSICION_INICIAL_INODO,SEEK_SET);
    fread(&TEMPORAL_BUSCAR_INODO,sizeof(Tabla_Inodos),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,POSICION_INICIAL_INODO,SEEK_SET);
        fread(&TEMPORAL_BUSCAR_INODO,sizeof(Tabla_Inodos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("3333333333333333333333333333  errror archivo abiero 333333333333333333333333333");
    }

    return TEMPORAL_BUSCAR_INODO;
    ////
}
//P_I_P=POSICION INICIAL PARTICION
//I_B=IDENTIFICADOR LOQUE
struct B_Carpetas RECUPERAR_BLOQUE_CARPETA(int P_I_P, int I_B, int V_N, string path_disco){
    struct B_Carpetas TEMPORAL_BLOQUE_CARPETA;
    int POSICION_INICIAL_BLOQUE=P_I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(I_B*sizeof(B_Carpetas));
  //  cout<<"posicion a recuperar: "<<POSICION_INICIAL_BLOQUE<<endl;
    //cout<<"path a recuperar: "<<path_disco<<endl;
    /*FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,POSICION_INICIAL_BLOQUE,SEEK_SET);
    fread(&TEMPORAL_BLOQUE_CARPETA,sizeof(B_Carpetas),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,POSICION_INICIAL_BLOQUE,SEEK_SET);
        fread(&TEMPORAL_BLOQUE_CARPETA,sizeof(B_Carpetas),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("e¿33333333333333333333333333333 error archivo abierto 333333333333333333333333");
    }
   // cout<<"nombre recuperado metodo interno: "<<TEMPORAL_BLOQUE_CARPETA.b_content[2].b_name<<endl;
    return TEMPORAL_BLOQUE_CARPETA;
}

struct B_Apuntadores RECUPERAR_BLOQUE_INDIRECTO(int P_I_P, int I_B, int V_N, string path_disco){
    struct B_Apuntadores TEMPORAL_APUNTADORES;
    int POSICION_INICIAL_APUNTADOR_INDIRECTO=P_I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(V_N*sizeof(B_Carpetas))+(I_B*sizeof(B_Apuntadores));
    /*FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,POSICION_INICIAL_APUNTADOR_INDIRECTO,SEEK_SET);
    fread(&TEMPORAL_APUNTADORES,sizeof(B_Apuntadores),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,POSICION_INICIAL_APUNTADOR_INDIRECTO,SEEK_SET);
        fread(&TEMPORAL_APUNTADORES,sizeof(B_Apuntadores),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("33333333333333333333333 error archivo abierto 33333333333333333333333");
    }

    return TEMPORAL_APUNTADORES;
}

struct B_Archivos RECUPERAR_BLOQUE_ARCHIVO(int P_I_P, int I_A, int V_N, string path_disco){
    struct B_Archivos TEMPORAL_ARCHIVO;
    int POSICION_RECUPERAR_BLOQUE_ARCHIVO=P_I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(2*V_N*sizeof(B_Carpetas))+(I_A*sizeof(B_Archivos));
  //  imprimirln("posicion a recuperar bloque archivo:");
  //  imprimirNumln(POSICION_RECUPERAR_BLOQUE_ARCHIVO);
    /*FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,POSICION_RECUPERAR_BLOQUE_ARCHIVO,SEEK_SET);
    fread(&TEMPORAL_ARCHIVO,sizeof(B_Archivos),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,POSICION_RECUPERAR_BLOQUE_ARCHIVO,SEEK_SET);
        fread(&TEMPORAL_ARCHIVO,sizeof(B_Archivos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("33333333333333333333333333333333 ERRO ARCHIVO abierto333333333333333333333333333333333");
    }
    return TEMPORAL_ARCHIVO;
}

////==================================== METODOS DE LA CREACION DEL PRIMER INODO Y PRIMER BLOQUE =======================
//R_A=RUTA ARCHIVO
//I_P=INICIO PARTICION
//P_R_A=POSICION RUTA ARCHIVO
int CREAR_BLOQUE_ARCHIVO_USERS(vector<string> R_A,int P_R_A,struct Tabla_Inodos INODO_PRINCIPAL,string RUTA_ARCHIVO_EXTRA,int LIMITE_BLOQUES,int I_P, int V_N,string path_disco,string CONTENIDO_ARCHIVO,string path_archivoExtra){
    int IDENTIFICADOR_BLOQUE_ARCHIVO=0;
//    imprimirln(RUTA_ARCHIVO_EXTRA);
   // FILE *archivo=fopen(RUTA_ARCHIVO_EXTRA.c_str(),"rb+");
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(RUTA_ARCHIVO_EXTRA.c_str(),"rb+");
    }else{
        imprimirln("84684 archivo abierto");
    }
    bool creado=false;
    char BITMAP_ARCHIVO_EXTRA;
    for(int i=0; (i<V_N) && (creado==false); i++){
        BITMAP_ARCHIVO_EXTRA=00;
        /*fseek(archivo,i,SEEK_SET);
        fread(&BITMAP_ARCHIVO_EXTRA,1,1,archivo);*/
        fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
        fread(&BITMAP_ARCHIVO_EXTRA,1,1,ARCHIVO_GLOBAL);
        //SI ENTRA AQUI ENCONTRO UN BITMAP LIBRE PARA CREAR EL BLOQUE CARPETA
        if(BITMAP_ARCHIVO_EXTRA==48){
            //imprimirln("entro a bimap extra");
            IDENTIFICADOR_BLOQUE_ARCHIVO=i;
            creado=true;
           /* fflush(archivo);
            fclose(archivo);*/
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
            //ACTUALIZO EL BITMAP DE ARCHIVO_EXTRA
            ACTUALIZAR_BITPMAP_ARCHIVO_EXTRA(path_archivoExtra,i);
            ACTUALIZAR_BITMAP_BLQUES(path_disco,(I_P+sizeof(Super_Bloque)+V_N),(I_P+sizeof(Super_Bloque)+(4*V_N)));
            //CREAMOS EL BLOQUE
            struct B_Carpetas PRIMER_BLOQUE;

            for(int k=0; k<4; k++){
                PRIMER_BLOQUE.b_content[k].b_inodo=-1;
            }

            string actual=".";
            string padre="..";
            //llenamos el actual
            PRIMER_BLOQUE.b_content[0].b_inodo=0;
            strcpy(PRIMER_BLOQUE.b_content[0].b_name,actual.c_str());
            //lenamos el padre
            PRIMER_BLOQUE.b_content[1].b_inodo=1;
            strcpy(PRIMER_BLOQUE.b_content[1].b_name,padre.c_str());
            //LLENAMOS EL BLOQUE CON LOS DATOS DEL INODO ARCHIVO
            strcpy(PRIMER_BLOQUE.b_content[2].b_name,R_A[P_R_A].c_str());
            //LLAMA AL METODO CREAR INODO USERS YA QUE EL BLQUE CARPETAS/ARCHIVOS CREA UN NUEVO INODO
            PRIMER_BLOQUE.b_content[2].b_inodo=CREAR_INODO_ARCHIVO_USERSR(I_P,V_N,path_disco,path_archivoExtra,CONTENIDO_ARCHIVO);
            //GUARDAR BLOQUE????
      //       imprimirNumln(i);
           // cout<<"posicion a guardar: "<<(I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(i*sizeof(B_Archivos)))<<endl;
            GUARDAR_BLOQUE_CARPETAS(path_disco,(I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(i*sizeof(B_Archivos))),PRIMER_BLOQUE);
            GUARDAR_BLOQUE_CARPETAS(OBTENER_PATH_ARCHIVORAID(path_disco),(I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(i*sizeof(B_Archivos))),PRIMER_BLOQUE);
          //-  imprimirln("salio de bitmap extra");
        }

    }

    if(ARCHIVO_GLOBAL!=NULL){
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }

   return IDENTIFICADOR_BLOQUE_ARCHIVO;
}
//I_P=INICIO DE LA PARTICION
//V_N=VALOR DE N
//
int CREAR_INODO_ARCHIVO_USERSR(int I_P,int V_N,string path_disco,string path_archivo_extra,string CONTENIDO_ARCHIVO){
    int POSICION_II=0;
    FILE *archivo=fopen(path_disco.c_str(),"rb+");
    bool creado=false;
    int contador_temporal=0;
    int inicio=I_P+sizeof(Super_Bloque);
    //ESTE FOR RECORRE CADA BITMAP DE INODOS PARA SABER DONDE CREAR EL  NUEVO INODO
    for(int i=inicio; (i<(I_P+sizeof(Super_Bloque)+V_N)) && (creado==false); i++){
        char BITMAP_INODO;
        fseek(archivo,i,SEEK_SET);
        fread(&BITMAP_INODO,1,1,archivo);
        //SIGNIFICA QUE ENCONTRO UN BIT DISPONIBLE EN EL BITMAP DE INODOS
        if(BITMAP_INODO==48){
            char ACTUALIZAR_BM_INODO=49;
            POSICION_II=contador_temporal;
            creado=true;
            //CON ESTO ACTUALIZAMOS EL BITMAP DE INODOS EN EL DISCO
            fseek(archivo,i,SEEK_SET);
            fwrite(&ACTUALIZAR_BM_INODO,1,1,archivo);
            fflush(archivo);
            fclose(archivo);
            //LLENAMOS EL NUEVO INODO ARCHIVO CON LOS DATOS
            srand(time(NULL));
            struct Tabla_Inodos NUEVO_INODO;
            NUEVO_INODO.i_uid=1;
            NUEVO_INODO.i_gid=1;
            NUEVO_INODO.i_size=CONTENIDO_ARCHIVO.size();
            NUEVO_INODO.i_atime=time(NULL);
            NUEVO_INODO.i_ctime=time(NULL);
            NUEVO_INODO.i_mtime=time(NULL);
            for(int k=0; k<16; k++){
                NUEVO_INODO.i_block[k]=-1;
            }
            NUEVO_INODO.i_type=1;
            NUEVO_INODO.i_perm=664;
            // AQUI VAMOSA COLOCAR EL CODIGO DONDE SE CREA EL BLOQUE ARCHIVO EL CUAL VA A TENER LA INFORMACION DEL ARCHIVO
            NUEVO_INODO.i_block[0]=0;//APUNTA A 0 PORQUE ES EL PRIMER BLOQUE ARCHIVOS QUE SE CREA
            //ESTE CODIGO PARA EL ARCHIVO PARA LA CREACION DE ARCHIVOS TIENE QUE IR EN OTRO METODO CON OTRAS INSTRUCCIONES
            //ESTE SE COLOCA AQUI YA UNICAMENTE ES EL PRIMERO POR DEFECTO Y NO VA HABER NINGUN CAMBIO
            //POR ESO SE PASA PARAMETRO AL BITMAP DE ARCHIVO EXTRA 2*V_N YA QUE UTILIZA EL PRIMER BIT DEL BITMAP DEL ARCHIVO EXTRA
            struct B_Archivos ARCHIVO_TEMPORAL;
            ACTUALIZAR_BITPMAP_ARCHIVO_EXTRA(path_archivo_extra,(2*V_N));
            ACTUALIZAR_BITMAP_BLQUES(path_disco,(I_P+sizeof(Super_Bloque)+V_N),(I_P+sizeof(Super_Bloque)+(4*V_N)));
            strcpy(ARCHIVO_TEMPORAL.b_contentA,CONTENIDO_ARCHIVO.c_str());
            //GUARDAMOS LA CARPETA EN EL LOS BLOQUES EN AL POSICION INDICADA
            GUARDAR_BLOQUE_ARCHIVOS(path_disco,(I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(2*V_N*sizeof(B_Archivos))+(0*sizeof(B_Archivos))),ARCHIVO_TEMPORAL);
            GUARDAR_BLOQUE_ARCHIVOS(OBTENER_PATH_ARCHIVORAID(path_disco),(I_P+sizeof(Super_Bloque)+(4*V_N)+(V_N*sizeof(Tabla_Inodos))+(2*V_N*sizeof(B_Archivos))+(0*sizeof(B_Archivos))),ARCHIVO_TEMPORAL);
            //POR ULTIMO GUARDAMOS EL INODO DEL ARCHIVO CREADO
    //        imprimirln("contador temporal");
    //        imprimirNumln(contador_temporal);
            GUARDAR_INODO_EN_DISCO(NUEVO_INODO,(I_P+sizeof(Super_Bloque)+(4*V_N)+(contador_temporal*sizeof(Tabla_Inodos))),path_disco);
            GUARDAR_INODO_EN_DISCO(NUEVO_INODO,(I_P+sizeof(Super_Bloque)+(4*V_N)+(contador_temporal*sizeof(Tabla_Inodos))),OBTENER_PATH_ARCHIVORAID(path_disco));

        }
        contador_temporal++;
    }
    fflush(archivo);
    fclose(archivo);

    return POSICION_II;
}
//ESTE METODO NOS SIRVE PARA ACTUALIZAR EL BITMAP DE BLOQUE CARPETAS/APUNTADORES/ARCHIVOS EN EL ARCHIVO EXTRA
void ACTUALIZAR_BITPMAP_ARCHIVO_EXTRA(string NOMBRE_ARCHIVO_RUTA,int POSICION){
    FILE *archivo=fopen(NOMBRE_ARCHIVO_RUTA.c_str(),"rb+");
    char NUEVO_VALOR_BITMAP=49;
    fseek(archivo,POSICION,SEEK_SET);
    fwrite(&NUEVO_VALOR_BITMAP,1,1,archivo);
    fflush(archivo);
    fclose(archivo);
}
//ESTO ES PARA LOS BITMAPS DE LOS DEMAS BLOQUES GENERALES DEL DISCO Y DEL RAID
void ACTUALIZAR_BITMAP_BLQUES(string path_disco,int POSICION_INICIO_BITMAP_BLQOUES,int FIN_BITMAP_BLOQUES){
    FILE *archivo=fopen(path_disco.c_str(),"rb+");
    char NUEVO_VALOR_BITMAP=49;
    bool actualizado=false;
    for(int i=POSICION_INICIO_BITMAP_BLQOUES;(i<FIN_BITMAP_BLOQUES) && (actualizado==false); i++){
        char BITMAP_LIBRE;
        fseek(archivo,i,SEEK_SET);
        fread(&BITMAP_LIBRE,1,1,archivo);
        if(BITMAP_LIBRE==48){
            actualizado=true;
            fseek(archivo,i,SEEK_SET);
            fwrite(&NUEVO_VALOR_BITMAP,1,1,archivo);
            fflush(archivo);
            fclose(archivo);
        }
    }
    fflush(archivo);
    fclose(archivo);
}
//ESTO NOS SIRVE PARA GUARDAR EL BLOQUE DE ARCHIVOS EN EL DISCO Y EN EL RAID
void GUARDAR_BLOQUE_ARCHIVOS(string path_disco,int POSICION,struct B_Archivos BLOQUE_ARCHIVO){
   /* FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,POSICION,SEEK_SET);
    fwrite(&BLOQUE_ARCHIVO,sizeof(B_Archivos),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,POSICION,SEEK_SET);
        fwrite(&BLOQUE_ARCHIVO,sizeof(B_Archivos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("3333333333333333333333333333333333EL ARCHIVO SE ENCUENTRA ABIERTO33333333333333333333333333");
    }
}
//ESTO NOS SIRVE PARA GUARDAR EL BLOUE DE CARPETAS EN EL DISCO Y EN EL RAID
void GUARDAR_BLOQUE_CARPETAS(string path_disco, int posicion,struct B_Carpetas BLOQUE_CARPETA){
   /* FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,posicion,SEEK_SET);
    cout<<"posicoin a guardar metodo interno: "<<posicion<<endl;
    fwrite(&BLOQUE_CARPETA,sizeof(B_Carpetas),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion,SEEK_SET);
        fwrite(&BLOQUE_CARPETA,sizeof(B_Carpetas),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("3333333333333333333333333333 EL ARCHIVO SE ENCUENTRA ABIERTO 333333333333333333333");
    }
}

void GUARDAR_BLOQUE_INDIRECTO(string path_disco,int posicion,struct B_Apuntadores BLOQUE_INDIRECTO){
   /* FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,posicion,SEEK_SET);
    imprimirln("entro a guardar blqoue indirecto file");
    fwrite(&BLOQUE_INDIRECTO,sizeof(B_Apuntadores),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion,SEEK_SET);
   //     imprimirln("entro a guardar bloque indirecto file");
        fwrite(&BLOQUE_INDIRECTO,sizeof(B_Apuntadores),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("33333333333333333333333333333333333EL ARCHIVO SE ENCUENTRA ABIERTO333333333333333333333333333");
    }
}

void GUARDAR_SUPERBLOQUE_DISCO(string path_disco, int posicion, struct Super_Bloque SUPERBLOQUE){
    /*FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,posicion,SEEK_SET);
    fwrite(&SUPERBLOQUE,sizeof(Super_Bloque),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
    }else{
        imprimirln("26888 ERRO ARCHIVO ABIERTO ");
    }
    fseek(ARCHIVO_GLOBAL,posicion,SEEK_SET);
    fwrite(&SUPERBLOQUE,sizeof(Super_Bloque),1,ARCHIVO_GLOBAL);
    fflush(ARCHIVO_GLOBAL);
    fclose(ARCHIVO_GLOBAL);
    ARCHIVO_GLOBAL=NULL;
}

struct Super_Bloque RECUPERAR_SUPERBLOQUE_DISCO(string path_disco, int posicion){
    struct Super_Bloque TEMPORALSUPERB;
    FILE *archivo=fopen(path_disco.c_str(),"rb+");
    fseek(archivo,posicion,SEEK_SET);
    fread(&TEMPORALSUPERB,sizeof(Super_Bloque),1,archivo);
    fflush(archivo);
    fclose(archivo);
    return TEMPORALSUPERB;
}

void GUARDAR_STRUCT_JOURNALING(string path_jour,struct JOURNALING JOUR){
    int contadoractual=RECUPERAR_CONTADOR_JOURNALING(path_jour);
    int posicion_journaling=sizeof(CONTADOR_JOURNALING)+(contadoractual*sizeof(JOURNALING));
   /* FILE *archivo=fopen(path_jour.c_str(),"rb+");
    fseek(archivo,posicion_journaling,SEEK_SET);
    fwrite(&JOUR,sizeof(JOURNALING),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_jour.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_journaling,SEEK_SET);
        fwrite(&JOUR,sizeof(JOURNALING),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("2222222222222222222222222222222 ARCHIVO ABIERTO 2222222222222222");
    }
    GUARDAR_CONTADOR_ACTUALIZADO(path_jour,contadoractual+1);
}

void GUARDAR_CONTADOR_ACTUALIZADO(string path_jour,int CONTADOR_act){
    struct CONTADOR_JOURNALING JOUR_ACTUALIZADO;
    JOUR_ACTUALIZADO.CONTADOR=CONTADOR_act;
   /* FILE *archivo=fopen(path_jour.c_str(),"rb+");
    fseek(archivo,0,SEEK_SET);
    fwrite(&JOUR_ACTUALIZADO,sizeof(CONTADOR_JOURNALING),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_jour.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,0,SEEK_SET);
        fwrite(&JOUR_ACTUALIZADO,sizeof(CONTADOR_JOURNALING),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("2222222222222222222222222 ARCHIVO ABIERTO 22222222");
    }
}

int RECUPERAR_CONTADOR_JOURNALING(string path_jounaling){
    struct CONTADOR_JOURNALING TEMPCONTADOR;
  /*  FILE *archivo=fopen(path_jounaling.c_str(),"rb+");
    fseek(archivo,0,SEEK_SET);
    fread(&TEMPCONTADOR,sizeof(CONTADOR_JOURNALING),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_jounaling.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,0,SEEK_SET);
        fread(&TEMPCONTADOR,sizeof(CONTADOR_JOURNALING),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("2222222222222222222 ERROR AL ABRIR ARCHIVO 222222222222222222");
    }
    return TEMPCONTADOR.CONTADOR;
}

struct JOURNALING RECUPERAR_JOURNALING(string path_jour,int ubicacion){
    struct JOURNALING TEMPORAL;
    /*FILE *archivo=fopen(path_jour.c_str(),"rb+");
    fseek(archivo,ubicacion,SEEK_SET);
    fread(&TEMPORAL,sizeof(JOURNALING),1,archivo);
    fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_jour.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,ubicacion,SEEK_SET);
        fread(&TEMPORAL,sizeof(JOURNALING),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("222222222222 EL ARCHIVO ESTA ABIERTO 222222222222222222");
    }
    return TEMPORAL;
}

void MOSTRAR_INFORMACION_JOURNALING(string path_journalingn){
    int CONTADOR=RECUPERAR_CONTADOR_JOURNALING(path_journalingn);

    for(int i=0; i<CONTADOR; i++){
        struct JOURNALING TMP;
        int posicion_recuperar=sizeof(CONTADOR_JOURNALING)+(i*sizeof(JOURNALING));
        TMP=RECUPERAR_JOURNALING(path_journalingn,posicion_recuperar);
        cout<<"INFORMACION JOUR :"<<i<<endl;
        cout<<TMP.RUTA<<endl;
        cout<<TMP.CONTENIDO<<endl;
        cout<<TMP.CP<<endl;
        cout<<ctime(&TMP.FECHA_CREACION)<<endl;

    }


}

/////-------------------------------------------------------------------------------------------------------------------


#endif // VALIDACIONES_COMANDOSBLOQUES_H_INCLUDED
